<?php
/*
 *
 * */


define('ROOT_DIR',__DIR__);
require_once ROOT_DIR.'/core/selwyn.php';
error_reporting(E_ALL);
if($_SERVER['REQUEST_METHOD'] == 'OPTIONS' || $_SERVER['REQUEST_URI'] == '/favicon.ico'){
    echo 'success';exit;
}

try{
    $app = App::inst();
    $app->run();
}catch (Exception $e){
    echo 'ENVIRONMENT : '.ENVIRONMENT;
    var_dump($e);
}

/*
 * Index File Should only Initialize Base class App;
 * Do not Add any more Code here in this File;
 * -Thanks for Choosing LifePHP Framework -By Selwyn Infotech INC. INDIA
 **/

