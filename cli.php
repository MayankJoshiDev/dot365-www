<?php
/**
 * Created by PhpStorm.
 * User: CPU011
 * Date: 4/25/2018
 * Time: 10:25 PM
 */

//require_once 'index.php';
define('ROOT_DIR',__DIR__);
$_SERVER['ENVIRONMENT'] = 'local';
require_once ROOT_DIR.'/core/selwyn.php';

$app = App::inst();
$app->run();

$argument = $_SERVER['argv'];


unset($argument[0]); //unset file name so that is not included in variable list

foreach ($argument as $key => $value) {
    $variable[] = explode(':', $value);
}
$className = $variable[0][0];
${'fxName'} = $variable[0][1];

$class = new $className();
if(isset($variable[1][0])){
    $ans = $class->{$fxName}($variable[1][0]);
}else{
    $ans = $class->{$fxName}();
}
