<!-- footer section start -->
<footer>
    <div class="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12">
                    <div class="footer-end">
                        <p><i class="fa fa-copyright" aria-hidden="true"></i>  2020-21 Dot365</p>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12">
                    <div class="payment-card-bottom">
                        <ul>
                            <li class="terms-li">
                                <a class="text-uppercase" href="/termandconditions" style="color: #fff;font-weight: bold" target="_blank">Terms and Conditions</a>
                            </li>
                            <li>
                                <a href="fb.me/Dot365official" target="_blank">
                                    <img src="<?php echo THEME_URL ?>/images/fb.png" style="height: 25px;width: 25px">
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/dot365official/" target="_blank">
                                    <img src="<?php echo THEME_URL ?>/images/instagram.png" style="height: 25px;width: 25px">
                                </a>
                            </li>
                            <li>
                                <a href="#" onclick="openWhatsapp()" target="_blank">
                                    <img src="<?php echo THEME_URL ?>/images/whatsapp.png" style="height: 28px;width: 28px">
                                </a>
                            </li>
                            <li>
                                <a href="#" onclick="openCall()" target="_blank">
                                    <img src="<?php echo THEME_URL ?>/images/call.png" style="height: 28px;width: 28px">
                                </a>
                            </li>
                            <li>
                                <a href="mailto:info@dot365.in" target="_blank">
                                    <img src="<?php echo THEME_URL ?>/images/gmail.png" style="height: 25px;width: 25px">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- footer section end -->

<!-- Add to cart bar -->
<div id="customer_support" class="add_to_cart right">
    <a href="javascript:void(0)" class="overlay" onclick="closeCustomerSupport()"></a>
    <div class="cart-inner">
        <div class="cart_top">
            <h3>Customer Support</h3>
            <div class="close-cart">
                <a href="javascript:void(0)" onclick="closeCustomerSupport()">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <div class="cart_media" style="padding: 0 10px">
            <div id="chatListHolder">

            </div>
            <ul class="cart_total">
                <li>
                    <div class="buttons">
                        <a href="#" onclick="openNewChat()" class="btn btn-solid btn-block btn-solid-sm view-cart">Start New Chat</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Add to cart bar end-->

<!-- My account bar -->
<div id="new_chat" class="add_to_cart right">
    <a href="javascript:void(0)" class="overlay" onclick="closeNewChat()"></a>
    <div class="cart-inner">
        <div class="cart_top">
            <h3>Start New Chat</h3>
            <div class="close-cart">
                <a href="javascript:void(0)" onclick="closeNewChat()">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <form class="theme-form">
            <div class="form-group">
                <label for="topic">Topic Name</label>
                <input type="text" class="form-control" id="Newtopic" placeholder="Enter Topic Name" required="">
            </div>
            <a href="#" onclick="createChat(<?php  if(isset($chat_user)) { echo $chat_user['uid']; }; ?>)" class="btn btn-solid btn-solid-sm btn-block ">Start Chat</a>
        </form>
    </div>
</div>
<!-- Add to wishlist bar end-->

<!-- My account bar -->
<div id="chat_detail" class="add_to_cart right" >
    <a href="javascript:void(0)" class="overlay" onclick="closeChatDetail()"></a>
    <div class="cart-inner">
        <div class="cart_top" style="margin-bottom: 0">
            <h3>Chat Detail</h3>
            <div class="close-cart">
                <a href="javascript:void(0)" onclick="closeChatDetail()">
                    <i class="fa fa-times" aria-hidden="true"></i>
                </a>
            </div>
        </div>
        <div class="showChat_inner">
            <div class="media chat-inner-header">
                <a class="back_chatBox">
                    <i class="fa fa-chevron-left"></i> <?php if(isset($chat_user)) { echo $chat_user['Name']; }; ?>
                </a>
            </div>
            <input type="hidden" id="chatid">
            <input type="hidden" class="lastmsg">
            <input type="hidden" id="cuserid" value="<?php  if(isset($chat_user)) { echo $chat_user['uid']; }; ?>">
            <div id="chatdeails"></div>

            <div class="chat-reply-box p-b-20">
                <div class="right-icon-control">
                    <input type="text" class="form-control search-text" id="chatreply" placeholder="Share Your Thoughts">
                    <div class="form-icon" onclick="ChatReply(<?php  if(isset($chat_user)) { echo $chat_user['uid']; }; ?>)">
                        <i class="fa fa-paper-plane" ></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Add to wishlist bar end-->

<!-- tap to top -->
<!--<div class="tap-top">
    <div>
        <i class="fa fa-angle-double-up"></i>
    </div>
</div>-->

<div id="snackbar"></div>
<script>
    function toastFunction(msg) {
        // Get the snackbar DIV
        var x = document.getElementById("snackbar");

        x.innerHTML = msg;
        // Add the "show" class to DIV
        x.className = "show";

        // After 3 seconds, remove the show class from DIV
        setTimeout(function(){ x.className = x.className.replace("show", ""); }, 3000);
    }
</script>
<!-- tap to top End -->
<!-- latest jquery-->
<script src="<?php echo THEME_URL ?>/js/jquery-3.3.1.min.js" ></script>

<!-- menu js-->
<script src="<?php echo THEME_URL ?>/js/menu.js"></script>

<!-- timer js-->
<script src="<?php echo THEME_URL ?>/js/flipclock.js"></script>

<!-- popper js-->
<script src="<?php echo THEME_URL ?>/js/popper.min.js" ></script>

<!-- slick js-->
<script  src="<?php echo THEME_URL ?>/js/slick.js"></script>

<!-- Bootstrap js-->
<script src="<?php echo THEME_URL ?>/js/bootstrap.js" ></script>


<!-- Bootstrap Notification js-->
<script src="<?php echo THEME_URL ?>/js/bootstrap-notify.min.js"></script>

<!-- Theme js-->
<script src="<?php echo THEME_URL ?>/js/script.js?date=<?php echo date("Y/m/d h:i:s"); ?>" ></script>

<script>
    $(window).on('load', function() {
        $('#exampleModal').modal('show');
    });
</script>

<!-- modal js-->
<script src="<?php echo THEME_URL ?>/assets/js/modal.js" ></script>
<script>
   
    getcartcount();
    getfavcount();
   
</script>
</body>

</html>