<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="bigboost">
    <meta name="keywords" content="bigboost">
    <meta name="author" content="bigboost">
    <link rel="icon" href="<?php echo THEME_URL ?>/assets/images/favicon/11.png" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo THEME_URL ?>/assets/images/favicon/11.png" type="image/x-icon"/>
    <title>DOT365.in</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/style.css">
    <!-- Icons -->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/fontawesome.css">

    <!-- Icons -->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/flipclock.css">

    <!--Slick slider css-->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/slick-theme.css">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/bootstrap.css">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/themify-icons.css">

    <!-- Theme css -->
    <link rel="stylesheet" id="color" type="text/css" href="<?php echo THEME_URL ?>/css/color11.css">
	<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window,document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
 fbq('init', '250169089973755'); 
fbq('track', 'PageView');
</script>
<noscript>
 <img height="1" width="1" 
src="https://www.facebook.com/tr?id=250169089973755&ev=PageView
&noscript=1"/>
</noscript>
<!-- End Facebook Pixel Code -->
</head>
<body>

<!-- loader start -->
<div class="loader-wrapper">
    <div class=" bar">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>
<!-- loader end -->

<!-- header part start -->
<header class="header-2" style="background: #fff;">
    <div class="mobile-fix-header">
    </div>
    <div class="container" style="background: #fff !important;">
        <div class="row header-content">
            <div class="col-12">
                <div class="content-header">
                    <div class="left-section" style="width: 100%; padding-right: 45px">
                        <div class="header-top">
                            <div class="navbar mobile-icon">
                                <a href="javascript:void(0)" onclick="openNav()">
                                    <div class="bar-style"><i class="fa fa-bars sidebar-bar" aria-hidden="true"></i>
                                    </div>
                                </a>
                                <div id="mySidenav" class="sidenav">
                                    <a href="javascript:void(0)" class="sidebar-overlay" onclick="closeNav()"></a>
                                    <nav class="">
                                        <div onclick="closeNav()">
                                            <div class="sidebar-back text-left"><i class="fa fa-angle-left pr-2"
                                                                                   aria-hidden="true"></i> Back
                                            </div>
                                        </div>
                                        <ul id="sub-menu" class="sm pixelstrap sm-vertical ">
                                            <li>
                                                <a href="<?php echo SITE_DOMAIN . str::HOME_ROUTE ?>"><i
                                                            class="mr-3 fa fa-home"></i>Home</a>
                                            </li>
											 <li>
                                                <a href="<?php echo SITE_DOMAIN . str::PRODUCT_SEARCH_ROUTE ?>"><i
                                                            class="mr-3 fa fa-user"></i>Product</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo SITE_DOMAIN . str::PROFILE_ROUTE ?>"><i
                                                            class="mr-3 fa fa-user"></i>My Profile</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo SITE_DOMAIN . str::ORDER_HISTORY_ROUTE ?>"><i
                                                            class="mr-3 fa fa-home"></i>My Orders</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo SITE_DOMAIN . str::PRODUCT_CART_ROUTE ?>"><i
                                                            class="mr-3 fa fa-shopping-cart"></i>My Cart</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo SITE_DOMAIN . str::ADDRESS_LIST_ROUTE ?>"><i
                                                            class="mr-3 fa fa-map-marker"></i>My Address</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo SITE_DOMAIN . str::PRODUCT_WISHLIST_ROUTE ?>"><i
                                                            class="mr-3 fa fa-heart"></i>My Wishlist</a>
                                            </li>
                                          <!--  <li>
                                                <a href="#" onclick="openCustomerSupport()"><i
                                                            class="mr-3 fa fa-question"></i>Customer Support</a>
                                            </li>-->
                                            <li>
                                                <a href="<?php echo SITE_DOMAIN . str::ABOUT_ROUTE ?>"><i
                                                            class="mr-3 fa fa-info-circle"></i>About Us</a>
                                            </li>
                                            <li>
                                                <a href="<?php echo SITE_DOMAIN . str::LOGIN_ROUTE ?>"><i
                                                            class="mr-3 fa fa-sign-out"></i>Logout</a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                            <div class="brand-logo">
                                <a href="<?php echo SITE_DOMAIN . str::HOME_ROUTE ?>">
                                    <img src="<?php echo THEME_URL ?>/images/logo.png"></a>
                            </div>
                            <nav id="main-nav">
                                <div class="toggle-nav">
                                    <i class="ti-menu-alt"></i>
                                </div>
                                <ul id="main-menu" class="sm pixelstrap sm-horizontal">
                                    <li>
                                        <div class="mobile-back text-right">
                                            Back<i class="fa fa-angle-right pl-2" aria-hidden="true"></i>
                                        </div>
                                    </li>
                                    <li>
                                        <a href="<?php echo SITE_DOMAIN . str::HOME_ROUTE ?>">HOME</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo SITE_DOMAIN . str::PRODUCT_SEARCH_ROUTE ?>">Product</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo SITE_DOMAIN . str::ABOUT_ROUTE ?>">ABout US</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo SITE_DOMAIN . str::ORDER_HISTORY_ROUTE ?>">My Orders</a>
                                    </li>
                                    <li>
                                        <a href="<?php echo SITE_DOMAIN . str::PRODUCT_CART_ROUTE ?>">My Cart</a>
                                    </li>
                                   <!-- <li>
                                        <?php if(isset($chat_user['uid'])) { ?>
                                        <a href="#" onclick="openCustomerSupport(<?php echo $chat_user['uid']; ?>)">Customer Support</a>
                                        <?php } else { ?>
                                            <a href="#" onclick="alert('Please Login For Support')">Customer Support</a>
                                        <?php } ?>
                                    </li>-->
                                    <li>
                                        <a href="<?php echo SITE_DOMAIN . str::CONTACT_US_ROUTE ?>">Contact Us</a>
                                    </li>
                                    <?php if (Session::inst()->getIsLogin()) { ?>
                                    <li>
                                        <a href="<?php echo SITE_DOMAIN . str::LOGOUT_ROUTE ?>">logout</a>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="right-section">
                        <div class="lower-section">
                            <div class="nav-icon">
                                <ul>
                                 
                                    <li class="onhover-div wishlist-icon">
                                        <a href="<?php echo SITE_DOMAIN . str::PRODUCT_WISHLIST_ROUTE ?>">
                                            <img src="<?php echo THEME_URL ?>/images/wishlist.png" alt=""
                                                 class="wishlist-img">
                                            <span class="cart-count totalfavcont" id="productCartLength"></span>
                                            <i class="ti-heart mobile-icon"></i>
                                        </a>
                                        
                                    </li>
                                    <li class="onhover-div user-icon">
                                        <?php if (!Session::inst()->getIsLogin()) { ?>
                                        <a href="<?php echo SITE_DOMAIN . str::LOGIN_ROUTE ?>">
                                            <img src="<?php echo THEME_URL ?>/images/user.png" alt="" class="user-img">
                                            <i class="ti-user mobile-icon"></i>
                                        </a>
                                        
                                        <?php } else {  ?>
                                            <a href="<?php echo SITE_DOMAIN . str::PROFILE_ROUTE ?>">
                                                <img src="<?php echo THEME_URL ?>/images/user.png" alt="" class="user-img">
                                                <i class="ti-user mobile-icon"></i>
                                            </a>
                                      <?php  } ?>
                                    </li>
                                    <li class="onhover-div cart-icon">
                                        <a href="<?php echo SITE_DOMAIN . str::PRODUCT_CART_ROUTE ?>">
                                            <img src="<?php echo THEME_URL ?>/images/shopping-cart.png" alt=""
                                                 class="cart-image">
                                            <span class="cart-count cartitems" id="productCartLength"></span>
                                            <i class="ti-shopping-cart mobile-icon"></i>
                                        </a>
                                        <div class="cart  icon-detail">

                                            <!--<h6 class="up-cls"><span id="cartitems"></span></h6>-->
                                            <h6><a href="<?php echo SITE_DOMAIN . str::PRODUCT_CART_ROUTE ?>"></a></h6>
                                        </div>
                                    </li>
									
										<style>
													.float{
													position:fixed;
													width:50px;
													height:50px;
													bottom:40px;
													right:40px;
													background-color:#fff;
													color:#FFF;
													border-radius:50px;
													text-align:center;
													box-shadow: 2px 2px 3px #999;
													z-index:99;
													text-align:center;
												}
													.float123{													
													width:50px;
													height:50px;
													float:right;	
													background-color:#d41d36;
													color:#FFF;
													border-radius:50px;
													text-align:center;
													box-shadow: 2px 2px 3px #999;
													z-index:99;
													text-align:center;margin:5px;cursor:pointer
												}
												.my-float{
													margin-top:22px;
												}
										</style>
										
												<div id="suppbtn" class="float" >
														<a href="#" style="position:absolute;top:-12px;right:-5px" onclick="closesupp()">x</a>
														<a onclick="openWhatsapp()" target="_blank" style="cursor: pointer;">
															  <img src="<?php echo THEME_URL ?>/images/whatsapp.png" style="height: 48px;width: 48px;margin-top:2px">
														</a>
													</div>

													<div id="result"></div>

												

													<script>
													var lastname = sessionStorage.getItem("supp123");
													if (lastname == "close") {
														document.getElementById("suppbtn").style.display = "none";
													}
													function closesupp() {
														sessionStorage.setItem("supp123", "close");
														document.getElementById('suppbtn').style.display = "none";
														}
													</script>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</header>
<!-- header part end -->