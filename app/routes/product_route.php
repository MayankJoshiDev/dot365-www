<?php
Route::get('product/',function(){
ProductController::inst()->index();
});

Route::get(str::PRODUCT_LIST_ROUTE,function(){
ProductController::inst()->productList();
});

Route::get(str::PRODUCT_DETAIL_ROUTE,function(){
ProductController::inst()->productDetail();
});

Route::get(str::PRODUCT_SEARCH_ROUTE,function(){
ProductController::inst()->productSearch();
});

Route::get(str::PRODUCT_CART_ROUTE,function(){
ProductController::inst()->productCart();
});

Route::get(str::PRODUCT_WISHLIST_ROUTE,function(){
ProductController::inst()->productWishList();
});