<?php


Route::post('/support_list',function (){
   SupportController::inst()->supportList();
});

Route::post('/star_new_support',function (){
   SupportController::inst()->startNewSupport();
});

Route::post('/view_support_detail',function (){
   SupportController::inst()->viewSupportDetail();
});

Route::post('/refresh_support_chat',function (){
   SupportController::inst()->refreshChatDetail();
});



Route::post('/send_support_message',function (){
   SupportController::inst()->sendSupportMessage();
});

Route::post('/refresh_Chat_Detail', function (){
    SupportController::inst()->refreshChatDetail();
});

