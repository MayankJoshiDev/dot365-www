<?php
Route::get('/home',function(){
    HomeController::inst()->index();
});
Route::get('/test9876',function(){
    HomeController::inst()->test9876();
});
Route::get(str::SUB_CATEGORY_ROUTE,function(){
    HomeController::inst()->subCategoryList();
});

Route::get(str::ABOUT_ROUTE,function(){
    HomeController::inst()->about();
});

Route::get(str::CONTACT_US_ROUTE,function(){
    HomeController::inst()->contactUs();
});

Route::post('/contact_message',function(){
    HomeController::inst()->contactMessage();
});