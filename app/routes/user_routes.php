<?php
/**
 * Created by PhpStorm.
 * User: CPU011
 * Date: 3/15/2018
 * Time: 12:17 AM
 */

Route::get(str::PROFILE_ROUTE, function () {
    UserController::inst()->profile();
});

Route::post('/customer_profile',function (){
    UserController::inst()->customerProfile();
});

Route::get(str::LOGIN_ROUTE,function(){
    LoginController::inst()->login();
});

Route::get('/',function(){
    LoginController::inst()->login();
});

Route::get(str::SIGNUP_ROUTE,function(){
    LoginController::inst()->signUp();
});

Route::post('/create_account',function(){
    LoginController::inst()->create_account();
});

Route::get(str::FORGOT_PASSWORD_ROUTE,function(){
    UserController::inst()->forgotPassword();
});

Route::post(str::OTP_ROUTE,function(){
    LoginController::inst()->verifyOTP();
});

Route::post('/otp_step_2',function(){
    LoginController::inst()->otpStep2();
});

Route::get(str::CHANGE_PASSWORD_ROUTE,function(){
    UserController::inst()->changePassword();
});

Route::get(str::NEW_ADDRESS_ROUTE,function(){
    UserController::inst()->addNewAddress();
});

Route::get(str::ADDRESS_LIST_ROUTE,function(){
    UserController::inst()->addressList();
});

Route::post('/address_action',function(){
    UserController::inst()->addressAction();
});

Route::post('/address_delete',function(){
    UserController::inst()->addressDelete();
});

Route::get('/termandconditions',function(){
    LoginController::inst()->termandconditions();
});


Route::get('/doLogout',function(){
    UserController::inst()->doLogout();
});