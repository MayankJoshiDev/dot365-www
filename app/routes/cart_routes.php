<?php

Route::post('/add_to_cart_all',function (){
   CartController::inst()->addToCart();
});
Route::post('/add_to_cart',function (){
   CartController::inst()->addToCart();
});

Route::post('/view_customer_cart',function (){
   CartController::inst()->viewCustomerCart();
});

Route::post('/delete_customer_cart_item',function (){
    CartController::inst()->deletecartitem();
});


Route::post('/add_to_favourite',function (){
    CartController::inst()->addToFavourite();
});

Route::post('/getFavcount',function (){
    CartController::inst()->getFavcount();
});

Route::post('/getcartitem',function (){
    CartController::inst()->getCartcount();
});

