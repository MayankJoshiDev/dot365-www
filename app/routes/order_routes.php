<?php
/**
 * Created by PhpStorm.
 * User: CPU011
 * Date: 3/15/2018
 * Time: 12:17 AM
 */

Route::get(str::ORDER_HISTORY_ROUTE, function () {
    OrderController::inst()->orderHistory();
});

Route::get(str::ORDER_SUCCESS_ROUTE, function () {
    OrderController::inst()->orderSuccess();
});

Route::get(str::ORDER_DETAIL_ROUTE, function () {
    OrderController::inst()->orderDetail();
});
Route::get('/get_del_charge', function () {
    OrderController::inst()->getdelcharge();
});

Route::post('/OrderAgain', function () {
    OrderController::inst()->order_agian();
});



Route::get(str::CHECKOUT_ROUTE, function () {
    OrderController::inst()->checkout();
});

Route::post('/payonline', function () {
    OrderController::inst()->payonline();
});
Route::post('/payonline2', function () {
    OrderController::inst()->payonline2();
});

Route::post('/verify', function () {
    OrderController::inst()->verify();
});

Route::post('/placeorder', function () {
    OrderController::inst()->placeOrder();
});

Route::post('/ordercancel', function () {
    OrderController::inst()->cancelorder();
});

Route::post('/order_item_update', function () {
    OrderController::inst()->changeOrderItem();
});

Route::post('/order_item_remove', function () {
    OrderController::inst()->changeOrderRemove();
});