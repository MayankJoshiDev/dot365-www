<?php

class str
{
    /*Home Routes */
    const SUB_CATEGORY_ROUTE = "/sub_category_list";
    const HOME_ROUTE = "/home";
    const ABOUT_ROUTE = "/about_us";
    const CONTACT_US_ROUTE = "/contact_us";

    /*Product Routes */
    const PRODUCT_LIST_ROUTE = "/product_list";
    const PRODUCT_DETAIL_ROUTE = "/product_detail";
    const PRODUCT_SEARCH_ROUTE = "/product_search";
    const PRODUCT_CART_ROUTE = "/product_cart";
    const PRODUCT_WISHLIST_ROUTE = "/product_wish_list";

    /*Profile Route*/
    const PROFILE_ROUTE = "/profile";
    const LOGIN_ROUTE = "/login";
    const LOGOUT_ROUTE = "/doLogout";
    const SIGNUP_ROUTE = "user/sign_up";
    const FORGOT_PASSWORD_ROUTE = "/forgot_password";
    const OTP_ROUTE = "/verify_otp";
    const CHANGE_PASSWORD_ROUTE = "/change_password";
    const NEW_ADDRESS_ROUTE = "/new_address";
    const ADDRESS_LIST_ROUTE = "/address_list";

    /*Order Route*/
    const ORDER_HISTORY_ROUTE = "/order_history";
    const ORDER_SUCCESS_ROUTE = "/order_success";
    const ORDER_DETAIL_ROUTE = "/order_detail";
    const CHECKOUT_ROUTE = "/checkout";


}