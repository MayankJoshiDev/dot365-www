<?php

/**
 * Created by PhpStorm.
 * User: OM
 * Date: 06-Feb-19
 * Time: 3:29 PM
 */
class OTP
{
    static function generate()
    {
        //return '1111';
        
        return rand(1111, 9999);
    }
}

class SMS
{
    static function send($to, $message)
    {
        //return TRUE;
       // $url = 'http://ui.netsms.co.in/API/SendSMS.aspx?APIkey=fUehFWHBnWwSsiv7Hy6Sta7l36&SenderID=Selwyn&SMSType=4&Mobile=' . $to . '&MsgText=' . urlencode($message);
      //  $url = 'https://2factor.in/API/V1/4cde2a10-f352-11ea-9fa5-0200cd936042/ADDON_SERVICES/SEND/TSMS?From=SMSDOT&To='.$to.'&TemplateName=Customer_New&VAR1=' . $message ;
        //$response = file_get_contents($url);
				$post = [
				'From' => 'DOTSMS',
				'To' => $to,
				'TemplateName'   => 'Customer_New2',
				'VAR1'=>$message
				];

				$ch = curl_init('https://2factor.in/API/V1/4cde2a10-f352-11ea-9fa5-0200cd936042/ADDON_SERVICES/SEND/TSMS');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

				// execute!
				$response = curl_exec($ch);

				// close the connection, release resources used
				curl_close($ch);
        error_log($response . PHP_EOL, 0, '/sms_log');
        return $response;
    }
     static function sendAdmin($to, $message1, $message2, $message3, $message4)
    {
        //return TRUE;
       // $url = 'http://ui.netsms.co.in/API/SendSMS.aspx?APIkey=fUehFWHBnWwSsiv7Hy6Sta7l36&SenderID=Selwyn&SMSType=4&Mobile=' . $to . '&MsgText=' . urlencode($message);
       // $url = 'https://2factor.in/API/V1/4cde2a10-f352-11ea-9fa5-0200cd936042/ADDON_SERVICES/SEND/TSMS?From=DOTSMS&To='.$to.'&TemplateName=AdminSMS&VAR1=' . $message ;
      //  $response = file_get_contents($url);
		$post = [
				'From' => 'DOTSMS',
				'To' => $to,
				'TemplateName'   => 'AdminSMS2',
				'VAR1'=>$message1,
				'VAR2'=>$message2,
				'VAR3'=>$message3,
				'VAR4'=>$message4
				];

				$ch = curl_init('https://2factor.in/API/V1/4cde2a10-f352-11ea-9fa5-0200cd936042/ADDON_SERVICES/SEND/TSMS');
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $post);

				// execute!
				$response = curl_exec($ch);

				// close the connection, release resources used
				curl_close($ch);
        error_log($response . PHP_EOL, 0, '/sms_log');
        return $response;
    }
    static function sendOTP($mobileNumber,$otp){
        try{
            ini_set('display_errors','1');
            $twoFactorKey = '5e8d0fba-bc32-11ea-9fa5-0200cd936042';
            $url = 'http://2factor.in/API/V1/'.$twoFactorKey.'/SMS/'.$mobileNumber.'/'.$otp.'/Dot365OTP';
            $response = file_get_contents($url);
            error_log($response.PHP_EOL,0,'/sms_log');
            return $response;
        }catch (Exception $e){
            return false;
        }
    }
    
    static function sendOptIn($to, $message)
    {
        //return true;4cde2a10-f352-11ea-9fa5-0200cd936042
        $url = 'http://ui.netsms.co.in/API/SendSMS.aspx?APIkey=fUehFWHBnWwSsiv7Hy6Sta7l36&SenderID=Selwyn&SMSType=3&Mobile=' . $to . '&MsgText=' . urlencode($message);
        $response = file_get_contents($url);
        error_log($response . PHP_EOL, 0, '/sms_log');
        return $response;
    }
    
    static function OrderTemplate($orderNumber)
    {
        return "Thanks your order has been received. Your Order id is " . $orderNumber . ". You can expect delivery by 24 hours. You can track and manage your orders on Dot365 App. Help line number: +917045873218" . PHP_EOL . "Follow us on Instagram: https://www.instagram.com/dot365official/" . PHP_EOL . "Facebook: fb.me/Dot365official";
    }
}

class DT
{
    static function formatDate($date)
    {
        $newDate = date("d-m-Y", strtotime($date));
        return $newDate;
    }
    
    static function formatTime($date)
    {
        $newDate = date("h:m a", strtotime($date));
        return $newDate;
    }
    
}

class Image
{
    static $w = 200;
    static $h = 200;
    
    static function Upload($w, $h, $ImageData, $destinationPath, $newFileName)
    {
        //$uploadedFile = $_FILES['file']['tmp_name'];
        
        $uploadedFile = $ImageData['tmp_name'];
        
        $sourceProperties = getimagesize($uploadedFile);
        if ($sourceProperties[0] > $w) {
            self::$w = $w;
            self::$h = round($sourceProperties[1] * $w / $sourceProperties[0]);
        } else {
            self::$w = $sourceProperties[0];
            self::$h = $sourceProperties[1];
        }
        
        /*$newFileName = time();*/
        $dirPath = $destinationPath;
        $ext = pathinfo($ImageData['name'], PATHINFO_EXTENSION);
        
        $imageType = $sourceProperties[2];
        
        switch ($imageType) {
            case IMAGETYPE_PNG:
                $imageSrc = imagecreatefrompng($uploadedFile);
                $tmp = self::imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1]);
                imagepng($tmp, $dirPath . $newFileName . "." . $ext);
                break;
            
            case IMAGETYPE_JPEG:
                $imageSrc = imagecreatefromjpeg($uploadedFile);
                
                $tmp = self::imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1]);
                imagejpeg($tmp, $dirPath . $newFileName . "." . $ext);
                break;
            
            case IMAGETYPE_GIF:
                $imageSrc = imagecreatefromgif($uploadedFile);
                $tmp = self::imageResize($imageSrc, $sourceProperties[0], $sourceProperties[1]);
                imagegif($tmp, $dirPath . $newFileName . "." . $ext);
                break;
            
            default:
                echo "Invalid Image type.";
                exit;
                break;
        }
        
        //move_uploaded_file($uploadedFile, $dirPath. $newFileName. ".". $ext);
        return $dirPath . $newFileName . "." . $ext;
        
    }
    
    static function imageResize($imageSrc, $imageWidth, $imageHeight)
    {
        
        $newImageWidth = self::$w;
        $newImageHeight = self::$h;
        $newImageLayer = imagecreatetruecolor($newImageWidth, $newImageHeight);
        imagecopyresampled($newImageLayer, $imageSrc, 0, 0, 0, 0, $newImageWidth, $newImageHeight, $imageWidth, $imageHeight);
        
        return $newImageLayer;
    }
    
    
}

class TICKET
{
    static function number()
    {
        //$number = rand('1111','9999');
        $s = SelwynDatabase::query("SELECT getSequence('SupportTicket') as seq");
        $number = $s[0]->seq;
        $date = date('ymd');
        $orderNumber = $number;
        return $orderNumber;
    }
}
class ORDER{
    static function number(){
        //$number = rand('11111','99999');
        $s = SelwynDatabase::query("SELECT getSequence('Order".date('Ymd')."') as seq");
        $number = $s[0]->seq;
        $date = date('Ymd');
        //$orderNumber =$date.'/'.$number;
		 $orderNumber =$date.'/'.str_pad($number, 2, '0', STR_PAD_LEFT);
        return $orderNumber;
    }
}

