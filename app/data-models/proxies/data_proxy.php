<?php

class AccGroupTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'acc_group' ; const DF_ID = 'id';
 private $id;
 const DF_NAME = 'name';
 private $name;
 const DF_UNDER_GROUP = 'under_group';
 private $under_group;
 const DF_GROUP_TYPE = 'group_type';
 private $group_type;
 const DF_IS_LOCKED = 'is_locked';
 private $is_locked;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getName(){
 return $this->name;
 }
 public function getUnderGroup(){
 return $this->under_group;
 }
 public function getGroupType(){
 return $this->group_type;
 }
 public function getIsLocked(){
 return $this->is_locked;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setName($name){
 $this->name = $name;
 }
 public function setUnderGroup($under_group){
 $this->under_group = $under_group;
 }
 public function setGroupType($group_type){
 $this->group_type = $group_type;
 }
 public function setIsLocked($is_locked){
 $this->is_locked = $is_locked;
 }
 function getDataSet(){ return get_object_vars($this);} }
class AdminTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'admin' ; const DF_ID = 'id';
 private $id;
 const DF_MOBILE_NUMBER = 'mobile_number';
 private $mobile_number;
 const DF_PASSWORD = 'password';
 private $password;
 const DF_FIRST_NAME = 'first_name';
 private $first_name;
 const DF_LAST_NAME = 'last_name';
 private $last_name;
 const DF_LAST_LOGIN = 'last_login';
 private $last_login;
 const DF_IS_ACTIVE = 'is_active';
 private $is_active;
 const DF_POSITION = 'position';
 private $position;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getMobileNumber(){
 return $this->mobile_number;
 }
 public function getPassword(){
 return $this->password;
 }
 public function getFirstName(){
 return $this->first_name;
 }
 public function getLastName(){
 return $this->last_name;
 }
 public function getLastLogin(){
 return $this->last_login;
 }
 public function getIsActive(){
 return $this->is_active;
 }
 public function getPosition(){
 return $this->position;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setMobileNumber($mobile_number){
 $this->mobile_number = $mobile_number;
 }
 public function setPassword($password){
 $this->password = $password;
 }
 public function setFirstName($first_name){
 $this->first_name = $first_name;
 }
 public function setLastName($last_name){
 $this->last_name = $last_name;
 }
 public function setLastLogin($last_login){
 $this->last_login = $last_login;
 }
 public function setIsActive($is_active){
 $this->is_active = $is_active;
 }
 public function setPosition($position){
 $this->position = $position;
 }
 function getDataSet(){ return get_object_vars($this);} }
class CategoryTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'category' ; const DF_ID = 'id';
 private $id;
 const DF_NAME = 'name';
 private $name;
 const DF_PARENT_ID = 'parent_id';
 private $parent_id;
 const DF_IS_ACTIVE = 'is_active';
 private $is_active;
 const DF_IS_DELETED = 'is_deleted';
 private $is_deleted;
 const DF_CATEGORY_IMAGE = 'category_image';
 private $category_image;
 const DF_CITY_ID = 'city_id';
 private $city_id;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getName(){
 return $this->name;
 }
 public function getParentId(){
 return $this->parent_id;
 }
 public function getIsActive(){
 return $this->is_active;
 }
 public function getIsDeleted(){
 return $this->is_deleted;
 }
 public function getCategoryImage(){
 return $this->category_image;
 }
 public function getCityId(){
 return $this->city_id;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setName($name){
 $this->name = $name;
 }
 public function setParentId($parent_id){
 $this->parent_id = $parent_id;
 }
 public function setIsActive($is_active){
 $this->is_active = $is_active;
 }
 public function setIsDeleted($is_deleted){
 $this->is_deleted = $is_deleted;
 }
 public function setCategoryImage($category_image){
 $this->category_image = $category_image;
 }
 public function setCityId($city_id){
 $this->city_id = $city_id;
 }
 function getDataSet(){ return get_object_vars($this);} }
class CityTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'city' ; const DF_ID = 'id';
 private $id;
 const DF_CITY_NAME = 'city_name';
 private $city_name;
 const DF_IS_ACTIVE = 'is_active';
 private $is_active;
 const DF_WHATSAPP_NUMBER = 'whatsapp_number';
 private $whatsapp_number;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const DF_UPDATED_AT = 'updated_at';
 private $updated_at;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getCityName(){
 return $this->city_name;
 }
 public function getIsActive(){
 return $this->is_active;
 }
 public function getWhatsappNumber(){
 return $this->whatsapp_number;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function getUpdatedAt(){
 return $this->updated_at;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setCityName($city_name){
 $this->city_name = $city_name;
 }
 public function setIsActive($is_active){
 $this->is_active = $is_active;
 }
 public function setWhatsappNumber($whatsapp_number){
 $this->whatsapp_number = $whatsapp_number;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 public function setUpdatedAt($updated_at){
 $this->updated_at = $updated_at;
 }
 function getDataSet(){ return get_object_vars($this);} }
class CustomerTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'customer' ; const DF_ID = 'id';
 private $id;
 const DF_EMAIL = 'email';
 private $email;
 const DF_MOBILE_NUMBER = 'mobile_number';
 private $mobile_number;
 const DF_PASSWORD = 'password';
 private $password;
 const DF_PROFILE_IMAGE = 'profile_image';
 private $profile_image;
 const DF_FIRST_NAME = 'first_name';
 private $first_name;
 const DF_LAST_NAME = 'last_name';
 private $last_name;
 const DF_GENDER = 'gender';
 private $gender;
 const DF_DOB = 'dob';
 private $dob;
 const DF_IS_ACTIVE = 'is_active';
 private $is_active;
 const DF_IS_VERIFIED = 'is_verified';
 private $is_verified;
 const DF_OTP = 'otp';
 private $otp;
 const DF_OTP_EXPIRES = 'otp_expires';
 private $otp_expires;
 const DF_LEDGER_ID = 'ledger_id';
 private $ledger_id;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getEmail(){
 return $this->email;
 }
 public function getMobileNumber(){
 return $this->mobile_number;
 }
 public function getPassword(){
 return $this->password;
 }
 public function getProfileImage(){
 return $this->profile_image;
 }
 public function getFirstName(){
 return $this->first_name;
 }
 public function getLastName(){
 return $this->last_name;
 }
 public function getGender(){
 return $this->gender;
 }
 public function getDob(){
 return $this->dob;
 }
 public function getIsActive(){
 return $this->is_active;
 }
 public function getIsVerified(){
 return $this->is_verified;
 }
 public function getOtp(){
 return $this->otp;
 }
 public function getOtpExpires(){
 return $this->otp_expires;
 }
 public function getLedgerId(){
 return $this->ledger_id;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setEmail($email){
 $this->email = $email;
 }
 public function setMobileNumber($mobile_number){
 $this->mobile_number = $mobile_number;
 }
 public function setPassword($password){
 $this->password = $password;
 }
 public function setProfileImage($profile_image){
 $this->profile_image = $profile_image;
 }
 public function setFirstName($first_name){
 $this->first_name = $first_name;
 }
 public function setLastName($last_name){
 $this->last_name = $last_name;
 }
 public function setGender($gender){
 $this->gender = $gender;
 }
 public function setDob($dob){
 $this->dob = $dob;
 }
 public function setIsActive($is_active){
 $this->is_active = $is_active;
 }
 public function setIsVerified($is_verified){
 $this->is_verified = $is_verified;
 }
 public function setOtp($otp){
 $this->otp = $otp;
 }
 public function setOtpExpires($otp_expires){
 $this->otp_expires = $otp_expires;
 }
 public function setLedgerId($ledger_id){
 $this->ledger_id = $ledger_id;
 }
 function getDataSet(){ return get_object_vars($this);} }
class CustomerAddressTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'customer_address' ; const DF_ID = 'id';
 private $id;
 const DF_CUSTOMER_ID = 'customer_id';
 private $customer_id;
 const DF_ADDRESS_NAME = 'address_name';
 private $address_name;
 const DF_CUSTOMER_NAME = 'customer_name';
 private $customer_name;
 const DF_MOBILE_NUMBER = 'mobile_number';
 private $mobile_number;
 const DF_LINE1 = 'line1';
 private $line1;
 const DF_LINE2 = 'line2';
 private $line2;
 const DF_LANDMARK = 'landmark';
 private $landmark;
 const DF_AREA = 'area';
 private $area;
 const DF_CITY = 'city';
 private $city;
 const DF_STATE_ID = 'state_id';
 private $state_id;
 const DF_PINCODE = 'pincode';
 private $pincode;
 const DF_ADDRESS_LAT = 'address_lat';
 private $address_lat;
 const DF_ADDRESS_LONG = 'address_long';
 private $address_long;
 const DF_GST_NUMBER = 'gst_number';
 private $gst_number;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getCustomerId(){
 return $this->customer_id;
 }
 public function getAddressName(){
 return $this->address_name;
 }
 public function getCustomerName(){
 return $this->customer_name;
 }
 public function getMobileNumber(){
 return $this->mobile_number;
 }
 public function getLine1(){
 return $this->line1;
 }
 public function getLine2(){
 return $this->line2;
 }
 public function getLandmark(){
 return $this->landmark;
 }
 public function getArea(){
 return $this->area;
 }
 public function getCity(){
 return $this->city;
 }
 public function getStateId(){
 return $this->state_id;
 }
 public function getPincode(){
 return $this->pincode;
 }
 public function getAddressLat(){
 return $this->address_lat;
 }
 public function getAddressLong(){
 return $this->address_long;
 }
 public function getGstNumber(){
 return $this->gst_number;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setCustomerId($customer_id){
 $this->customer_id = $customer_id;
 }
 public function setAddressName($address_name){
 $this->address_name = $address_name;
 }
 public function setCustomerName($customer_name){
 $this->customer_name = $customer_name;
 }
 public function setMobileNumber($mobile_number){
 $this->mobile_number = $mobile_number;
 }
 public function setLine1($line1){
 $this->line1 = $line1;
 }
 public function setLine2($line2){
 $this->line2 = $line2;
 }
 public function setLandmark($landmark){
 $this->landmark = $landmark;
 }
 public function setArea($area){
 $this->area = $area;
 }
 public function setCity($city){
 $this->city = $city;
 }
 public function setStateId($state_id){
 $this->state_id = $state_id;
 }
 public function setPincode($pincode){
 $this->pincode = $pincode;
 }
 public function setAddressLat($address_lat){
 $this->address_lat = $address_lat;
 }
 public function setAddressLong($address_long){
 $this->address_long = $address_long;
 }
 public function setGstNumber($gst_number){
 $this->gst_number = $gst_number;
 }
 function getDataSet(){ return get_object_vars($this);} }
class CustomerCartTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'customer_cart' ; const DF_ID = 'id';
 private $id;
 const DF_PRODUCT_ID = 'product_id';
 private $product_id;
 const DF_CUSTOMER_ID = 'customer_id';
 private $customer_id;
 const DF_ADDED_ON = 'added_on';
 private $added_on;
 const DF_QTY = 'qty';
 private $qty;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getProductId(){
 return $this->product_id;
 }
 public function getCustomerId(){
 return $this->customer_id;
 }
 public function getAddedOn(){
 return $this->added_on;
 }
 public function getQty(){
 return $this->qty;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setProductId($product_id){
 $this->product_id = $product_id;
 }
 public function setCustomerId($customer_id){
 $this->customer_id = $customer_id;
 }
 public function setAddedOn($added_on){
 $this->added_on = $added_on;
 }
 public function setQty($qty){
 $this->qty = $qty;
 }
 function getDataSet(){ return get_object_vars($this);} }
class DeletedOrdersTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'deleted_orders' ; const DF_ID = 'id';
 private $id;
 const DF_DELETED_ORDER_ID = 'deleted_order_id';
 private $deleted_order_id;
 const DF_CUSTOMER_ID = 'customer_id';
 private $customer_id;
 const DF_ADDRESS_ID = 'address_id';
 private $address_id;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const DF_ORDER_NUMBER = 'order_number';
 private $order_number;
 const DF_STATUS = 'status';
 private $status;
 const DF_ORDER_TOTAL = 'order_total';
 private $order_total;
 const DF_WINWIN_CASHBACK = 'winwin_cashback';
 private $winwin_cashback;
 const DF_CASH = 'cash';
 private $cash;
 const DF_ELITE_CASHBACK = 'elite_cashback';
 private $elite_cashback;
 const DF_PAYMENT_GATEWAY_ID = 'payment_gateway_id';
 private $payment_gateway_id;
 const DF_CAPTURE_RESPONSE = 'capture_response';
 private $capture_response;
 const DF_PAYMENT_METHOD = 'payment_method';
 private $payment_method;
 const DF_REFERRAL_CODE = 'referral_code';
 private $referral_code;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getDeletedOrderId(){
 return $this->deleted_order_id;
 }
 public function getCustomerId(){
 return $this->customer_id;
 }
 public function getAddressId(){
 return $this->address_id;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function getOrderNumber(){
 return $this->order_number;
 }
 public function getStatus(){
 return $this->status;
 }
 public function getOrderTotal(){
 return $this->order_total;
 }
 public function getWinwinCashback(){
 return $this->winwin_cashback;
 }
 public function getCash(){
 return $this->cash;
 }
 public function getEliteCashback(){
 return $this->elite_cashback;
 }
 public function getPaymentGatewayId(){
 return $this->payment_gateway_id;
 }
 public function getCaptureResponse(){
 return $this->capture_response;
 }
 public function getPaymentMethod(){
 return $this->payment_method;
 }
 public function getReferralCode(){
 return $this->referral_code;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setDeletedOrderId($deleted_order_id){
 $this->deleted_order_id = $deleted_order_id;
 }
 public function setCustomerId($customer_id){
 $this->customer_id = $customer_id;
 }
 public function setAddressId($address_id){
 $this->address_id = $address_id;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 public function setOrderNumber($order_number){
 $this->order_number = $order_number;
 }
 public function setStatus($status){
 $this->status = $status;
 }
 public function setOrderTotal($order_total){
 $this->order_total = $order_total;
 }
 public function setWinwinCashback($winwin_cashback){
 $this->winwin_cashback = $winwin_cashback;
 }
 public function setCash($cash){
 $this->cash = $cash;
 }
 public function setEliteCashback($elite_cashback){
 $this->elite_cashback = $elite_cashback;
 }
 public function setPaymentGatewayId($payment_gateway_id){
 $this->payment_gateway_id = $payment_gateway_id;
 }
 public function setCaptureResponse($capture_response){
 $this->capture_response = $capture_response;
 }
 public function setPaymentMethod($payment_method){
 $this->payment_method = $payment_method;
 }
 public function setReferralCode($referral_code){
 $this->referral_code = $referral_code;
 }
 function getDataSet(){ return get_object_vars($this);} }
class FavouriteProductTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'favourite_product' ; const DF_ID = 'id';
 private $id;
 const DF_PRODUCT_ID = 'product_id';
 private $product_id;
 const DF_CUSTOMER_ID = 'customer_id';
 private $customer_id;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getProductId(){
 return $this->product_id;
 }
 public function getCustomerId(){
 return $this->customer_id;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setProductId($product_id){
 $this->product_id = $product_id;
 }
 public function setCustomerId($customer_id){
 $this->customer_id = $customer_id;
 }
 function getDataSet(){ return get_object_vars($this);} }
class HsnTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'hsn' ; const DF_ID = 'id';
 private $id;
 const DF_HSN_CODE = 'hsn_code';
 private $hsn_code;
 const DF_IGST = 'igst';
 private $igst;
 const DF_SGST = 'sgst';
 private $sgst;
 const DF_TOTAL_GST = 'total_gst';
 private $total_gst;
 const DF_CESS = 'cess';
 private $cess;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const DF_ADMIN_ID = 'admin_id';
 private $admin_id;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getHsnCode(){
 return $this->hsn_code;
 }
 public function getIgst(){
 return $this->igst;
 }
 public function getSgst(){
 return $this->sgst;
 }
 public function getTotalGst(){
 return $this->total_gst;
 }
 public function getCess(){
 return $this->cess;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function getAdminId(){
 return $this->admin_id;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setHsnCode($hsn_code){
 $this->hsn_code = $hsn_code;
 }
 public function setIgst($igst){
 $this->igst = $igst;
 }
 public function setSgst($sgst){
 $this->sgst = $sgst;
 }
 public function setTotalGst($total_gst){
 $this->total_gst = $total_gst;
 }
 public function setCess($cess){
 $this->cess = $cess;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 public function setAdminId($admin_id){
 $this->admin_id = $admin_id;
 }
 function getDataSet(){ return get_object_vars($this);} }
class LedgerTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'ledger' ; const DF_ID = 'id';
 private $id;
 const DF_NAME = 'name';
 private $name;
 const DF_PAN_CARD = 'pan_card';
 private $pan_card;
 const DF_GST_NUMBER = 'gst_number';
 private $gst_number;
 const DF_ACC_GROUP_ID = 'acc_group_id';
 private $acc_group_id;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const DF_REF_TABLE = 'ref_table';
 private $ref_table;
 const DF_REF_ID = 'ref_id';
 private $ref_id;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getName(){
 return $this->name;
 }
 public function getPanCard(){
 return $this->pan_card;
 }
 public function getGstNumber(){
 return $this->gst_number;
 }
 public function getAccGroupId(){
 return $this->acc_group_id;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function getRefTable(){
 return $this->ref_table;
 }
 public function getRefId(){
 return $this->ref_id;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setName($name){
 $this->name = $name;
 }
 public function setPanCard($pan_card){
 $this->pan_card = $pan_card;
 }
 public function setGstNumber($gst_number){
 $this->gst_number = $gst_number;
 }
 public function setAccGroupId($acc_group_id){
 $this->acc_group_id = $acc_group_id;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 public function setRefTable($ref_table){
 $this->ref_table = $ref_table;
 }
 public function setRefId($ref_id){
 $this->ref_id = $ref_id;
 }
 function getDataSet(){ return get_object_vars($this);} }
class OrderItemsTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'order_items' ; const DF_ID = 'id';
 private $id;
 const DF_ORDER_ID = 'order_id';
 private $order_id;
 const DF_PRODUCT_ID = 'product_id';
 private $product_id;
 const DF_QTY = 'qty';
 private $qty;
 const DF_SELLING_PRICE = 'selling_price';
 private $selling_price;
 const DF_CASH = 'cash';
 private $cash;
 const DF_WINWIN_CASHBACK = 'winwin_cashback';
 private $winwin_cashback;
 const DF_ELITE_CASHBACK = 'elite_cashback';
 private $elite_cashback;
 const DF_GST_SLAB = 'gst_slab';
 private $gst_slab;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getOrderId(){
 return $this->order_id;
 }
 public function getProductId(){
 return $this->product_id;
 }
 public function getQty(){
 return $this->qty;
 }
 public function getSellingPrice(){
 return $this->selling_price;
 }
 public function getCash(){
 return $this->cash;
 }
 public function getWinwinCashback(){
 return $this->winwin_cashback;
 }
 public function getEliteCashback(){
 return $this->elite_cashback;
 }
 public function getGstSlab(){
 return $this->gst_slab;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setOrderId($order_id){
 $this->order_id = $order_id;
 }
 public function setProductId($product_id){
 $this->product_id = $product_id;
 }
 public function setQty($qty){
 $this->qty = $qty;
 }
 public function setSellingPrice($selling_price){
 $this->selling_price = $selling_price;
 }
 public function setCash($cash){
 $this->cash = $cash;
 }
 public function setWinwinCashback($winwin_cashback){
 $this->winwin_cashback = $winwin_cashback;
 }
 public function setEliteCashback($elite_cashback){
 $this->elite_cashback = $elite_cashback;
 }
 public function setGstSlab($gst_slab){
 $this->gst_slab = $gst_slab;
 }
 function getDataSet(){ return get_object_vars($this);} }
class OrderNotesTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'order_notes' ; const DF_ID = 'id';
 private $id;
 const DF_ORDER_ID = 'order_id';
 private $order_id;
 const DF_NOTE = 'note';
 private $note;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const DF_ADMIN_ID = 'admin_id';
 private $admin_id;
 const DF_NOTE_BY = 'note_by';
 private $note_by;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getOrderId(){
 return $this->order_id;
 }
 public function getNote(){
 return $this->note;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function getAdminId(){
 return $this->admin_id;
 }
 public function getNoteBy(){
 return $this->note_by;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setOrderId($order_id){
 $this->order_id = $order_id;
 }
 public function setNote($note){
 $this->note = $note;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 public function setAdminId($admin_id){
 $this->admin_id = $admin_id;
 }
 public function setNoteBy($note_by){
 $this->note_by = $note_by;
 }
 function getDataSet(){ return get_object_vars($this);} }
class OrdersTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'orders' ; const DF_ID = 'id';
 private $id;
 const DF_CUSTOMER_ID = 'customer_id';
 private $customer_id;
 const DF_ADDRESS_ID = 'address_id';
 private $address_id;
 const DF_CITY_ID = 'city_id';
 private $city_id;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const DF_ORDER_NUMBER = 'order_number';
 private $order_number;
 const DF_STATUS = 'status';
 private $status;
 const DF_ORDER_TOTAL = 'order_total';
 private $order_total;
 const DF_WINWIN_CASHBACK = 'winwin_cashback';
 private $winwin_cashback;
 const DF_CASH = 'cash';
 private $cash;
 const DF_ELITE_CASHBACK = 'elite_cashback';
 private $elite_cashback;
 const DF_PAYMENT_GATEWAY_ID = 'payment_gateway_id';
 private $payment_gateway_id;
 const DF_CAPTURE_RESPONSE = 'capture_response';
 private $capture_response;
 const DF_PAYMENT_METHOD = 'payment_method';
 private $payment_method;
 const DF_REFERRAL_CODE = 'referral_code';
 private $referral_code;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getCustomerId(){
 return $this->customer_id;
 }
 public function getAddressId(){
 return $this->address_id;
 }
 public function getCityId(){
 return $this->city_id;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function getOrderNumber(){
 return $this->order_number;
 }
 public function getStatus(){
 return $this->status;
 }
 public function getOrderTotal(){
 return $this->order_total;
 }
 public function getWinwinCashback(){
 return $this->winwin_cashback;
 }
 public function getCash(){
 return $this->cash;
 }
 public function getEliteCashback(){
 return $this->elite_cashback;
 }
 public function getPaymentGatewayId(){
 return $this->payment_gateway_id;
 }
 public function getCaptureResponse(){
 return $this->capture_response;
 }
 public function getPaymentMethod(){
 return $this->payment_method;
 }
 public function getReferralCode(){
 return $this->referral_code;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setCustomerId($customer_id){
 $this->customer_id = $customer_id;
 }
 public function setAddressId($address_id){
 $this->address_id = $address_id;
 }
 public function setCityId($city_id){
 $this->city_id = $city_id;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 public function setOrderNumber($order_number){
 $this->order_number = $order_number;
 }
 public function setStatus($status){
 $this->status = $status;
 }
 public function setOrderTotal($order_total){
 $this->order_total = $order_total;
 }
 public function setWinwinCashback($winwin_cashback){
 $this->winwin_cashback = $winwin_cashback;
 }
 public function setCash($cash){
 $this->cash = $cash;
 }
 public function setEliteCashback($elite_cashback){
 $this->elite_cashback = $elite_cashback;
 }
 public function setPaymentGatewayId($payment_gateway_id){
 $this->payment_gateway_id = $payment_gateway_id;
 }
 public function setCaptureResponse($capture_response){
 $this->capture_response = $capture_response;
 }
 public function setPaymentMethod($payment_method){
 $this->payment_method = $payment_method;
 }
 public function setReferralCode($referral_code){
 $this->referral_code = $referral_code;
 }
 function getDataSet(){ return get_object_vars($this);} }
class PendingItemsTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'pending_items' ; const DF_ID = 'id';
 private $id;
 const DF_ORDER_ID = 'order_id';
 private $order_id;
 const DF_PRODUCT_ID = 'product_id';
 private $product_id;
 const DF_QTY = 'qty';
 private $qty;
 const DF_SELLING_PRICE = 'selling_price';
 private $selling_price;
 const DF_CASH = 'cash';
 private $cash;
 const DF_WINWIN_CASHBACK = 'winwin_cashback';
 private $winwin_cashback;
 const DF_ELITE_CASHBACK = 'elite_cashback';
 private $elite_cashback;
 const DF_GST_SLAB = 'gst_slab';
 private $gst_slab;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getOrderId(){
 return $this->order_id;
 }
 public function getProductId(){
 return $this->product_id;
 }
 public function getQty(){
 return $this->qty;
 }
 public function getSellingPrice(){
 return $this->selling_price;
 }
 public function getCash(){
 return $this->cash;
 }
 public function getWinwinCashback(){
 return $this->winwin_cashback;
 }
 public function getEliteCashback(){
 return $this->elite_cashback;
 }
 public function getGstSlab(){
 return $this->gst_slab;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setOrderId($order_id){
 $this->order_id = $order_id;
 }
 public function setProductId($product_id){
 $this->product_id = $product_id;
 }
 public function setQty($qty){
 $this->qty = $qty;
 }
 public function setSellingPrice($selling_price){
 $this->selling_price = $selling_price;
 }
 public function setCash($cash){
 $this->cash = $cash;
 }
 public function setWinwinCashback($winwin_cashback){
 $this->winwin_cashback = $winwin_cashback;
 }
 public function setEliteCashback($elite_cashback){
 $this->elite_cashback = $elite_cashback;
 }
 public function setGstSlab($gst_slab){
 $this->gst_slab = $gst_slab;
 }
 function getDataSet(){ return get_object_vars($this);} }
class PendingsTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'pendings' ; const DF_ID = 'id';
 private $id;
 const DF_CUSTOMER_ID = 'customer_id';
 private $customer_id;
 const DF_ADDRESS_ID = 'address_id';
 private $address_id;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const DF_ORDER_NUMBER = 'order_number';
 private $order_number;
 const DF_STATUS = 'status';
 private $status;
 const DF_ORDER_TOTAL = 'order_total';
 private $order_total;
 const DF_WINWIN_CASHBACK = 'winwin_cashback';
 private $winwin_cashback;
 const DF_CASH = 'cash';
 private $cash;
 const DF_ELITE_CASHBACK = 'elite_cashback';
 private $elite_cashback;
 const DF_PAYMENT_GATEWAY_ID = 'payment_gateway_id';
 private $payment_gateway_id;
 const DF_CAPTURE_RESPONSE = 'capture_response';
 private $capture_response;
 const DF_PAYMENT_METHOD = 'payment_method';
 private $payment_method;
 const DF_REFERRAL_CODE = 'referral_code';
 private $referral_code;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getCustomerId(){
 return $this->customer_id;
 }
 public function getAddressId(){
 return $this->address_id;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function getOrderNumber(){
 return $this->order_number;
 }
 public function getStatus(){
 return $this->status;
 }
 public function getOrderTotal(){
 return $this->order_total;
 }
 public function getWinwinCashback(){
 return $this->winwin_cashback;
 }
 public function getCash(){
 return $this->cash;
 }
 public function getEliteCashback(){
 return $this->elite_cashback;
 }
 public function getPaymentGatewayId(){
 return $this->payment_gateway_id;
 }
 public function getCaptureResponse(){
 return $this->capture_response;
 }
 public function getPaymentMethod(){
 return $this->payment_method;
 }
 public function getReferralCode(){
 return $this->referral_code;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setCustomerId($customer_id){
 $this->customer_id = $customer_id;
 }
 public function setAddressId($address_id){
 $this->address_id = $address_id;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 public function setOrderNumber($order_number){
 $this->order_number = $order_number;
 }
 public function setStatus($status){
 $this->status = $status;
 }
 public function setOrderTotal($order_total){
 $this->order_total = $order_total;
 }
 public function setWinwinCashback($winwin_cashback){
 $this->winwin_cashback = $winwin_cashback;
 }
 public function setCash($cash){
 $this->cash = $cash;
 }
 public function setEliteCashback($elite_cashback){
 $this->elite_cashback = $elite_cashback;
 }
 public function setPaymentGatewayId($payment_gateway_id){
 $this->payment_gateway_id = $payment_gateway_id;
 }
 public function setCaptureResponse($capture_response){
 $this->capture_response = $capture_response;
 }
 public function setPaymentMethod($payment_method){
 $this->payment_method = $payment_method;
 }
 public function setReferralCode($referral_code){
 $this->referral_code = $referral_code;
 }
 function getDataSet(){ return get_object_vars($this);} }
class PorderNotesTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'porder_notes' ; const DF_ID = 'id';
 private $id;
 const DF_ORDER_ID = 'order_id';
 private $order_id;
 const DF_NOTE = 'note';
 private $note;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const DF_ADMIN_ID = 'admin_id';
 private $admin_id;
 const DF_NOTE_BY = 'note_by';
 private $note_by;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getOrderId(){
 return $this->order_id;
 }
 public function getNote(){
 return $this->note;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function getAdminId(){
 return $this->admin_id;
 }
 public function getNoteBy(){
 return $this->note_by;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setOrderId($order_id){
 $this->order_id = $order_id;
 }
 public function setNote($note){
 $this->note = $note;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 public function setAdminId($admin_id){
 $this->admin_id = $admin_id;
 }
 public function setNoteBy($note_by){
 $this->note_by = $note_by;
 }
 function getDataSet(){ return get_object_vars($this);} }
class ProductTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'product' ; const DF_ID = 'id';
 private $id;
 const DF_CITY_ID = 'city_id';
 private $city_id;
 const DF_NAME = 'name';
 private $name;
 const DF_NAME_EXTENSION = 'name_extension';
 private $name_extension;
 const DF_MODEL = 'model';
 private $model;
 const DF_BRAND = 'brand';
 private $brand;
 const DF_VENDOR_ID = 'vendor_id';
 private $vendor_id;
 const DF_HSN_ID = 'hsn_id';
 private $hsn_id;
 const DF_SKU = 'sku';
 private $sku;
 const DF_QTY_IN_STOCK = 'qty_in_stock';
 private $qty_in_stock;
 const DF_WEIGHT_KG = 'weight_kg';
 private $weight_kg;
 const DF_PURCHASE_PRICE = 'purchase_price';
 private $purchase_price;
 const DF_GST_PERCENT = 'gst_percent';
 private $gst_percent;
 const DF_COST_PRICE = 'cost_price';
 private $cost_price;
 const DF_MRP = 'mrp';
 private $mrp;
 const DF_MARKET_PRICE = 'market_price';
 private $market_price;
 const DF_SELLING_PRICE = 'selling_price';
 private $selling_price;
 const DF_WINWIN_CASHBACK_ALLOWED = 'winwin_cashback_allowed';
 private $winwin_cashback_allowed;
 const DF_ELITE_CASHBACK_ALLOWED = 'elite_cashback_allowed';
 private $elite_cashback_allowed;
 const DF_AVG_DISCOUNT_RATE = 'avg_discount_rate';
 private $avg_discount_rate;
 const DF_SIZE_APPLICABLE = 'size_applicable';
 private $size_applicable;
 const DF_COLOR_APPLICABLE = 'color_applicable';
 private $color_applicable;
 const DF_PARENT_ID = 'parent_id';
 private $parent_id;
 const DF_IS_TRENDING = 'is_trending';
 private $is_trending;
 const DF_IS_ONLINE = 'is_online';
 private $is_online;
 const DF_FREE_SHIPPING = 'free_shipping';
 private $free_shipping;
 const DF_SHIPPING_CHARGE = 'shipping_charge';
 private $shipping_charge;
 const DF_ADDON_APPLICABLE = 'addon_applicable';
 private $addon_applicable;
 const DF_ADDON_JSON = 'addon_json';
 private $addon_json;
 const DF_PACKET_SIZE = 'packet_size';
 private $packet_size;
 const DF_UNIT = 'unit';
 private $unit;
 const DF_IGST = 'IGST';
 private $IGST;
 const DF_CGST = 'CGST';
 private $CGST;
 const DF_SGST = 'SGST';
 private $SGST;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getCityId(){
 return $this->city_id;
 }
 public function getName(){
 return $this->name;
 }
 public function getNameExtension(){
 return $this->name_extension;
 }
 public function getModel(){
 return $this->model;
 }
 public function getBrand(){
 return $this->brand;
 }
 public function getVendorId(){
 return $this->vendor_id;
 }
 public function getHsnId(){
 return $this->hsn_id;
 }
 public function getSku(){
 return $this->sku;
 }
 public function getQtyInStock(){
 return $this->qty_in_stock;
 }
 public function getWeightKg(){
 return $this->weight_kg;
 }
 public function getPurchasePrice(){
 return $this->purchase_price;
 }
 public function getGstPercent(){
 return $this->gst_percent;
 }
 public function getCostPrice(){
 return $this->cost_price;
 }
 public function getMrp(){
 return $this->mrp;
 }
 public function getMarketPrice(){
 return $this->market_price;
 }
 public function getSellingPrice(){
 return $this->selling_price;
 }
 public function getWinwinCashbackAllowed(){
 return $this->winwin_cashback_allowed;
 }
 public function getEliteCashbackAllowed(){
 return $this->elite_cashback_allowed;
 }
 public function getAvgDiscountRate(){
 return $this->avg_discount_rate;
 }
 public function getSizeApplicable(){
 return $this->size_applicable;
 }
 public function getColorApplicable(){
 return $this->color_applicable;
 }
 public function getParentId(){
 return $this->parent_id;
 }
 public function getIsTrending(){
 return $this->is_trending;
 }
 public function getIsOnline(){
 return $this->is_online;
 }
 public function getFreeShipping(){
 return $this->free_shipping;
 }
 public function getShippingCharge(){
 return $this->shipping_charge;
 }
 public function getAddonApplicable(){
 return $this->addon_applicable;
 }
 public function getAddonJson(){
 return $this->addon_json;
 }
 public function getPacketSize(){
 return $this->packet_size;
 }
 public function getUnit(){
 return $this->unit;
 }
 public function getIGST(){
 return $this->IGST;
 }
 public function getCGST(){
 return $this->CGST;
 }
 public function getSGST(){
 return $this->SGST;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setCityId($city_id){
 $this->city_id = $city_id;
 }
 public function setName($name){
 $this->name = $name;
 }
 public function setNameExtension($name_extension){
 $this->name_extension = $name_extension;
 }
 public function setModel($model){
 $this->model = $model;
 }
 public function setBrand($brand){
 $this->brand = $brand;
 }
 public function setVendorId($vendor_id){
 $this->vendor_id = $vendor_id;
 }
 public function setHsnId($hsn_id){
 $this->hsn_id = $hsn_id;
 }
 public function setSku($sku){
 $this->sku = $sku;
 }
 public function setQtyInStock($qty_in_stock){
 $this->qty_in_stock = $qty_in_stock;
 }
 public function setWeightKg($weight_kg){
 $this->weight_kg = $weight_kg;
 }
 public function setPurchasePrice($purchase_price){
 $this->purchase_price = $purchase_price;
 }
 public function setGstPercent($gst_percent){
 $this->gst_percent = $gst_percent;
 }
 public function setCostPrice($cost_price){
 $this->cost_price = $cost_price;
 }
 public function setMrp($mrp){
 $this->mrp = $mrp;
 }
 public function setMarketPrice($market_price){
 $this->market_price = $market_price;
 }
 public function setSellingPrice($selling_price){
 $this->selling_price = $selling_price;
 }
 public function setWinwinCashbackAllowed($winwin_cashback_allowed){
 $this->winwin_cashback_allowed = $winwin_cashback_allowed;
 }
 public function setEliteCashbackAllowed($elite_cashback_allowed){
 $this->elite_cashback_allowed = $elite_cashback_allowed;
 }
 public function setAvgDiscountRate($avg_discount_rate){
 $this->avg_discount_rate = $avg_discount_rate;
 }
 public function setSizeApplicable($size_applicable){
 $this->size_applicable = $size_applicable;
 }
 public function setColorApplicable($color_applicable){
 $this->color_applicable = $color_applicable;
 }
 public function setParentId($parent_id){
 $this->parent_id = $parent_id;
 }
 public function setIsTrending($is_trending){
 $this->is_trending = $is_trending;
 }
 public function setIsOnline($is_online){
 $this->is_online = $is_online;
 }
 public function setFreeShipping($free_shipping){
 $this->free_shipping = $free_shipping;
 }
 public function setShippingCharge($shipping_charge){
 $this->shipping_charge = $shipping_charge;
 }
 public function setAddonApplicable($addon_applicable){
 $this->addon_applicable = $addon_applicable;
 }
 public function setAddonJson($addon_json){
 $this->addon_json = $addon_json;
 }
 public function setPacketSize($packet_size){
 $this->packet_size = $packet_size;
 }
 public function setUnit($unit){
 $this->unit = $unit;
 }
 public function setIGST($IGST){
 $this->IGST = $IGST;
 }
 public function setCGST($CGST){
 $this->CGST = $CGST;
 }
 public function setSGST($SGST){
 $this->SGST = $SGST;
 }
 function getDataSet(){ return get_object_vars($this);} }
class ProductBannerTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'product_banner' ; const DF_ID = 'id';
 private $id;
 const DF_BANNER_URL = 'banner_url';
 private $banner_url;
 const DF_CATEGORY_ID = 'category_id';
 private $category_id;
 const DF_CREATED_ON = 'created_on';
 private $created_on;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getBannerUrl(){
 return $this->banner_url;
 }
 public function getCategoryId(){
 return $this->category_id;
 }
 public function getCreatedOn(){
 return $this->created_on;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setBannerUrl($banner_url){
 $this->banner_url = $banner_url;
 }
 public function setCategoryId($category_id){
 $this->category_id = $category_id;
 }
 public function setCreatedOn($created_on){
 $this->created_on = $created_on;
 }
 function getDataSet(){ return get_object_vars($this);} }
class ProductCategoryTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'product_category' ; const DF_ID = 'id';
 private $id;
 const DF_PRODUCT_ID = 'product_id';
 private $product_id;
 const DF_CATEGORY_ID = 'category_id';
 private $category_id;
 const DF_PARENT_CATEGORY_ID = 'parent_category_id';
 private $parent_category_id;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getProductId(){
 return $this->product_id;
 }
 public function getCategoryId(){
 return $this->category_id;
 }
 public function getParentCategoryId(){
 return $this->parent_category_id;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setProductId($product_id){
 $this->product_id = $product_id;
 }
 public function setCategoryId($category_id){
 $this->category_id = $category_id;
 }
 public function setParentCategoryId($parent_category_id){
 $this->parent_category_id = $parent_category_id;
 }
 function getDataSet(){ return get_object_vars($this);} }
class ProductChildTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'product_child' ; const DF_ID = 'id';
 private $id;
 const DF_PRODUCT_ID = 'product_id';
 private $product_id;
 const DF_CHILD_ID = 'child_id';
 private $child_id;
 const DF_TITLE = 'title';
 private $title;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getProductId(){
 return $this->product_id;
 }
 public function getChildId(){
 return $this->child_id;
 }
 public function getTitle(){
 return $this->title;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setProductId($product_id){
 $this->product_id = $product_id;
 }
 public function setChildId($child_id){
 $this->child_id = $child_id;
 }
 public function setTitle($title){
 $this->title = $title;
 }
 function getDataSet(){ return get_object_vars($this);} }
class ProductImageTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'product_image' ; const DF_ID = 'id';
 private $id;
 const DF_PRODUCT_ID = 'product_id';
 private $product_id;
 const DF_URL = 'url';
 private $url;
 const DF_IS_LIFESTYLE = 'is_lifestyle';
 private $is_lifestyle;
 const DF_IS_PRIMARY = 'is_primary';
 private $is_primary;
 const DF_INDEX = 'index';
 private $index;
 const DF_ADDED_ON = 'added_on';
 private $added_on;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getProductId(){
 return $this->product_id;
 }
 public function getUrl(){
 return $this->url;
 }
 public function getIsLifestyle(){
 return $this->is_lifestyle;
 }
 public function getIsPrimary(){
 return $this->is_primary;
 }
 public function getIndex(){
 return $this->index;
 }
 public function getAddedOn(){
 return $this->added_on;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setProductId($product_id){
 $this->product_id = $product_id;
 }
 public function setUrl($url){
 $this->url = $url;
 }
 public function setIsLifestyle($is_lifestyle){
 $this->is_lifestyle = $is_lifestyle;
 }
 public function setIsPrimary($is_primary){
 $this->is_primary = $is_primary;
 }
 public function setIndex($index){
 $this->index = $index;
 }
 public function setAddedOn($added_on){
 $this->added_on = $added_on;
 }
 function getDataSet(){ return get_object_vars($this);} }
class ProductRatingTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'product_rating' ; const DF_ID = 'id';
 private $id;
 const DF_CUSTOMER_ID = 'customer_id';
 private $customer_id;
 const DF_PRODUCT_ID = 'product_id';
 private $product_id;
 const DF_ORDER_ITEM_ID = 'order_item_id';
 private $order_item_id;
 const DF_STAR_RATING = 'star_rating';
 private $star_rating;
 const DF_REVIEW = 'review';
 private $review;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getCustomerId(){
 return $this->customer_id;
 }
 public function getProductId(){
 return $this->product_id;
 }
 public function getOrderItemId(){
 return $this->order_item_id;
 }
 public function getStarRating(){
 return $this->star_rating;
 }
 public function getReview(){
 return $this->review;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setCustomerId($customer_id){
 $this->customer_id = $customer_id;
 }
 public function setProductId($product_id){
 $this->product_id = $product_id;
 }
 public function setOrderItemId($order_item_id){
 $this->order_item_id = $order_item_id;
 }
 public function setStarRating($star_rating){
 $this->star_rating = $star_rating;
 }
 public function setReview($review){
 $this->review = $review;
 }
 function getDataSet(){ return get_object_vars($this);} }
class ProductSearchTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'product_search' ; const DF_ID = 'id';
 private $id;
 const DF_NAME = 'name';
 private $name;
 const DF_URL = 'url';
 private $url;
 const DF_SELLING_PRICE = 'selling_price';
 private $selling_price;
 const AI_COL = '';
 private $PRI = array();
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getName(){
 return $this->name;
 }
 public function getUrl(){
 return $this->url;
 }
 public function getSellingPrice(){
 return $this->selling_price;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setName($name){
 $this->name = $name;
 }
 public function setUrl($url){
 $this->url = $url;
 }
 public function setSellingPrice($selling_price){
 $this->selling_price = $selling_price;
 }
 function getDataSet(){ return get_object_vars($this);} }
class ProductSizeTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'product_size' ; const DF_ID = 'id';
 private $id;
 const DF_PRODUCT_ID = 'product_id';
 private $product_id;
 const DF_SIZE = 'size';
 private $size;
 const DF_PRODUCT_COLOR = 'product_color';
 private $product_color;
 const DF_SKU = 'sku';
 private $sku;
 const DF_MRP = 'mrp';
 private $mrp;
 const DF_COST_PRICE = 'cost_price';
 private $cost_price;
 const DF_SELLING_PRICE = 'selling_price';
 private $selling_price;
 const DF_CASHBACK = 'cashback';
 private $cashback;
 const DF_IN_STOCK = 'in_stock';
 private $in_stock;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getProductId(){
 return $this->product_id;
 }
 public function getSize(){
 return $this->size;
 }
 public function getProductColor(){
 return $this->product_color;
 }
 public function getSku(){
 return $this->sku;
 }
 public function getMrp(){
 return $this->mrp;
 }
 public function getCostPrice(){
 return $this->cost_price;
 }
 public function getSellingPrice(){
 return $this->selling_price;
 }
 public function getCashback(){
 return $this->cashback;
 }
 public function getInStock(){
 return $this->in_stock;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setProductId($product_id){
 $this->product_id = $product_id;
 }
 public function setSize($size){
 $this->size = $size;
 }
 public function setProductColor($product_color){
 $this->product_color = $product_color;
 }
 public function setSku($sku){
 $this->sku = $sku;
 }
 public function setMrp($mrp){
 $this->mrp = $mrp;
 }
 public function setCostPrice($cost_price){
 $this->cost_price = $cost_price;
 }
 public function setSellingPrice($selling_price){
 $this->selling_price = $selling_price;
 }
 public function setCashback($cashback){
 $this->cashback = $cashback;
 }
 public function setInStock($in_stock){
 $this->in_stock = $in_stock;
 }
 function getDataSet(){ return get_object_vars($this);} }
class ProductSpecificationTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'product_specification' ; const DF_ID = 'id';
 private $id;
 const DF_PRODUCT_ID = 'product_id';
 private $product_id;
 const DF_TITLE = 'title';
 private $title;
 const DF_TEXT = 'text';
 private $text;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getProductId(){
 return $this->product_id;
 }
 public function getTitle(){
 return $this->title;
 }
 public function getText(){
 return $this->text;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setProductId($product_id){
 $this->product_id = $product_id;
 }
 public function setTitle($title){
 $this->title = $title;
 }
 public function setText($text){
 $this->text = $text;
 }
 function getDataSet(){ return get_object_vars($this);} }
class ProductStockTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'product_stock' ; const DF_ID = 'id';
 private $id;
 const DF_PRODUCT_ID = 'product_id';
 private $product_id;
 const DF_STOCKLIMIT = 'stocklimit';
 private $stocklimit;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getProductId(){
 return $this->product_id;
 }
 public function getStocklimit(){
 return $this->stocklimit;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setProductId($product_id){
 $this->product_id = $product_id;
 }
 public function setStocklimit($stocklimit){
 $this->stocklimit = $stocklimit;
 }
 function getDataSet(){ return get_object_vars($this);} }
class RegisterOtpTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'register_otp' ; const DF_ID = 'id';
 private $id;
 const DF_MOBILE_NUMBER = 'mobile_number';
 private $mobile_number;
 const DF_OTP = 'otp';
 private $otp;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getMobileNumber(){
 return $this->mobile_number;
 }
 public function getOtp(){
 return $this->otp;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setMobileNumber($mobile_number){
 $this->mobile_number = $mobile_number;
 }
 public function setOtp($otp){
 $this->otp = $otp;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 function getDataSet(){ return get_object_vars($this);} }
class SequenceTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'sequence' ; const DF_ID = 'id';
 private $id;
 const DF_NAME = 'name';
 private $name;
 const DF_NUMBER = 'number';
 private $number;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getName(){
 return $this->name;
 }
 public function getNumber(){
 return $this->number;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setName($name){
 $this->name = $name;
 }
 public function setNumber($number){
 $this->number = $number;
 }
 function getDataSet(){ return get_object_vars($this);} }
class ShipmentTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'shipment' ; const DF_ID = 'id';
 private $id;
 const DF_ORDER_ID = 'order_id';
 private $order_id;
 const DF_SHIPMENT_NUMBER = 'shipment_number';
 private $shipment_number;
 const DF_STATUS = 'status';
 private $status;
 const DF_ADMIN_ID = 'admin_id';
 private $admin_id;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getOrderId(){
 return $this->order_id;
 }
 public function getShipmentNumber(){
 return $this->shipment_number;
 }
 public function getStatus(){
 return $this->status;
 }
 public function getAdminId(){
 return $this->admin_id;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setOrderId($order_id){
 $this->order_id = $order_id;
 }
 public function setShipmentNumber($shipment_number){
 $this->shipment_number = $shipment_number;
 }
 public function setStatus($status){
 $this->status = $status;
 }
 public function setAdminId($admin_id){
 $this->admin_id = $admin_id;
 }
 function getDataSet(){ return get_object_vars($this);} }
class ShipmentLogTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'shipment_log' ; const DF_ID = 'id';
 private $id;
 const DF_SHIPMENT_ID = 'shipment_id';
 private $shipment_id;
 const DF_ADMIN_ID = 'admin_id';
 private $admin_id;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const DF_NOTE = 'note';
 private $note;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getShipmentId(){
 return $this->shipment_id;
 }
 public function getAdminId(){
 return $this->admin_id;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function getNote(){
 return $this->note;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setShipmentId($shipment_id){
 $this->shipment_id = $shipment_id;
 }
 public function setAdminId($admin_id){
 $this->admin_id = $admin_id;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 public function setNote($note){
 $this->note = $note;
 }
 function getDataSet(){ return get_object_vars($this);} }
class SkuTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'sku' ; const DF_ID = 'id';
 private $id;
 const DF_SKU = 'sku';
 private $sku;
 const DF_PRODUCT_ID = 'product_id';
 private $product_id;
 const DF_QTY = 'qty';
 private $qty;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getSku(){
 return $this->sku;
 }
 public function getProductId(){
 return $this->product_id;
 }
 public function getQty(){
 return $this->qty;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setSku($sku){
 $this->sku = $sku;
 }
 public function setProductId($product_id){
 $this->product_id = $product_id;
 }
 public function setQty($qty){
 $this->qty = $qty;
 }
 function getDataSet(){ return get_object_vars($this);} }
class StateTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'state' ; const DF_ID = 'id';
 private $id;
 const DF_NAME = 'name';
 private $name;
 const DF_CODE = 'code';
 private $code;
 const DF_SHORT_NAME = 'short_name';
 private $short_name;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getName(){
 return $this->name;
 }
 public function getCode(){
 return $this->code;
 }
 public function getShortName(){
 return $this->short_name;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setName($name){
 $this->name = $name;
 }
 public function setCode($code){
 $this->code = $code;
 }
 public function setShortName($short_name){
 $this->short_name = $short_name;
 }
 function getDataSet(){ return get_object_vars($this);} }
class SupportTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'support' ; const DF_ID = 'id';
 private $id;
 const DF_TICKET_NUMBER = 'ticket_number';
 private $ticket_number;
 const DF_CUSTOMER_ID = 'customer_id';
 private $customer_id;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const DF_TITLE = 'title';
 private $title;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getTicketNumber(){
 return $this->ticket_number;
 }
 public function getCustomerId(){
 return $this->customer_id;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function getTitle(){
 return $this->title;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setTicketNumber($ticket_number){
 $this->ticket_number = $ticket_number;
 }
 public function setCustomerId($customer_id){
 $this->customer_id = $customer_id;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 public function setTitle($title){
 $this->title = $title;
 }
 function getDataSet(){ return get_object_vars($this);} }
class SupportMessageTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'support_message' ; const DF_ID = 'id';
 private $id;
 const DF_SUPPORT_ID = 'support_id';
 private $support_id;
 const DF_CUSTOMER_ID = 'customer_id';
 private $customer_id;
 const DF_ADMIN_ID = 'admin_id';
 private $admin_id;
 const DF_MESSAGE = 'message';
 private $message;
 const DF_FLAG = 'flag';
 private $flag;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getSupportId(){
 return $this->support_id;
 }
 public function getCustomerId(){
 return $this->customer_id;
 }
 public function getAdminId(){
 return $this->admin_id;
 }
 public function getMessage(){
 return $this->message;
 }
 public function getFlag(){
 return $this->flag;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setSupportId($support_id){
 $this->support_id = $support_id;
 }
 public function setCustomerId($customer_id){
 $this->customer_id = $customer_id;
 }
 public function setAdminId($admin_id){
 $this->admin_id = $admin_id;
 }
 public function setMessage($message){
 $this->message = $message;
 }
 public function setFlag($flag){
 $this->flag = $flag;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 function getDataSet(){ return get_object_vars($this);} }
class TermsAndPolicyTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'terms_and_policy' ; const DF_ID = 'id';
 private $id;
 const DF_ABOUT_US = 'about_us';
 private $about_us;
 const DF_POLICY = 'policy';
 private $policy;
 const DF_TERMS = 'terms';
 private $terms;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getAboutUs(){
 return $this->about_us;
 }
 public function getPolicy(){
 return $this->policy;
 }
 public function getTerms(){
 return $this->terms;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setAboutUs($about_us){
 $this->about_us = $about_us;
 }
 public function setPolicy($policy){
 $this->policy = $policy;
 }
 public function setTerms($terms){
 $this->terms = $terms;
 }
 function getDataSet(){ return get_object_vars($this);} }
class TransactionTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'transaction' ; const DF_ID = 'id';
 private $id;
 const DF_VOUCHER_ID = 'voucher_id';
 private $voucher_id;
 const DF_LEDGER_ID = 'ledger_id';
 private $ledger_id;
 const DF_IS_DEBIT = 'is_debit';
 private $is_debit;
 const DF_AMOUNT = 'amount';
 private $amount;
 const DF_BY_LEDGER_ID = 'by_ledger_id';
 private $by_ledger_id;
 const DF_TXN_DATE = 'txn_date';
 private $txn_date;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getVoucherId(){
 return $this->voucher_id;
 }
 public function getLedgerId(){
 return $this->ledger_id;
 }
 public function getIsDebit(){
 return $this->is_debit;
 }
 public function getAmount(){
 return $this->amount;
 }
 public function getByLedgerId(){
 return $this->by_ledger_id;
 }
 public function getTxnDate(){
 return $this->txn_date;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setVoucherId($voucher_id){
 $this->voucher_id = $voucher_id;
 }
 public function setLedgerId($ledger_id){
 $this->ledger_id = $ledger_id;
 }
 public function setIsDebit($is_debit){
 $this->is_debit = $is_debit;
 }
 public function setAmount($amount){
 $this->amount = $amount;
 }
 public function setByLedgerId($by_ledger_id){
 $this->by_ledger_id = $by_ledger_id;
 }
 public function setTxnDate($txn_date){
 $this->txn_date = $txn_date;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 function getDataSet(){ return get_object_vars($this);} }
class VendorTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'vendor' ; const DF_ID = 'id';
 private $id;
 const DF_COMPANY_NAME = 'company_name';
 private $company_name;
 const DF_PERSON_NAME = 'person_name';
 private $person_name;
 const DF_MOBILE_NUMBER = 'mobile_number';
 private $mobile_number;
 const DF_CITY = 'city';
 private $city;
 const DF_STATE_ID = 'state_id';
 private $state_id;
 const DF_ADDRESS_LINE1 = 'address_line1';
 private $address_line1;
 const DF_ADDRESS_LINE2 = 'address_line2';
 private $address_line2;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getCompanyName(){
 return $this->company_name;
 }
 public function getPersonName(){
 return $this->person_name;
 }
 public function getMobileNumber(){
 return $this->mobile_number;
 }
 public function getCity(){
 return $this->city;
 }
 public function getStateId(){
 return $this->state_id;
 }
 public function getAddressLine1(){
 return $this->address_line1;
 }
 public function getAddressLine2(){
 return $this->address_line2;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setCompanyName($company_name){
 $this->company_name = $company_name;
 }
 public function setPersonName($person_name){
 $this->person_name = $person_name;
 }
 public function setMobileNumber($mobile_number){
 $this->mobile_number = $mobile_number;
 }
 public function setCity($city){
 $this->city = $city;
 }
 public function setStateId($state_id){
 $this->state_id = $state_id;
 }
 public function setAddressLine1($address_line1){
 $this->address_line1 = $address_line1;
 }
 public function setAddressLine2($address_line2){
 $this->address_line2 = $address_line2;
 }
 function getDataSet(){ return get_object_vars($this);} }
class VendorNoteTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'vendor_note' ; const DF_ID = 'id';
 private $id;
 const DF_VENDOR_ID = 'vendor_id';
 private $vendor_id;
 const DF_NOTE = 'note';
 private $note;
 const DF_ADMIN_ID = 'admin_id';
 private $admin_id;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getVendorId(){
 return $this->vendor_id;
 }
 public function getNote(){
 return $this->note;
 }
 public function getAdminId(){
 return $this->admin_id;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setVendorId($vendor_id){
 $this->vendor_id = $vendor_id;
 }
 public function setNote($note){
 $this->note = $note;
 }
 public function setAdminId($admin_id){
 $this->admin_id = $admin_id;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 function getDataSet(){ return get_object_vars($this);} }
class VoucherTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'voucher' ; const DF_ID = 'id';
 private $id;
 const DF_VOUCHER_DATE = 'voucher_date';
 private $voucher_date;
 const DF_VOUCHER_TYPE = 'voucher_type';
 private $voucher_type;
 const DF_VOUCHER_NUMBER = 'voucher_number';
 private $voucher_number;
 const DF_FROM_LEDGER_ID = 'from_ledger_id';
 private $from_ledger_id;
 const DF_TO_LEDGER_ID = 'to_ledger_id';
 private $to_ledger_id;
 const DF_AMOUNT = 'amount';
 private $amount;
 const DF_INFO = 'info';
 private $info;
 const DF_VOUCHER_TITTLE = 'voucher_tittle';
 private $voucher_tittle;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getVoucherDate(){
 return $this->voucher_date;
 }
 public function getVoucherType(){
 return $this->voucher_type;
 }
 public function getVoucherNumber(){
 return $this->voucher_number;
 }
 public function getFromLedgerId(){
 return $this->from_ledger_id;
 }
 public function getToLedgerId(){
 return $this->to_ledger_id;
 }
 public function getAmount(){
 return $this->amount;
 }
 public function getInfo(){
 return $this->info;
 }
 public function getVoucherTittle(){
 return $this->voucher_tittle;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setVoucherDate($voucher_date){
 $this->voucher_date = $voucher_date;
 }
 public function setVoucherType($voucher_type){
 $this->voucher_type = $voucher_type;
 }
 public function setVoucherNumber($voucher_number){
 $this->voucher_number = $voucher_number;
 }
 public function setFromLedgerId($from_ledger_id){
 $this->from_ledger_id = $from_ledger_id;
 }
 public function setToLedgerId($to_ledger_id){
 $this->to_ledger_id = $to_ledger_id;
 }
 public function setAmount($amount){
 $this->amount = $amount;
 }
 public function setInfo($info){
 $this->info = $info;
 }
 public function setVoucherTittle($voucher_tittle){
 $this->voucher_tittle = $voucher_tittle;
 }
 function getDataSet(){ return get_object_vars($this);} }
class VoucherGstTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'voucher_gst' ; const DF_ID = 'id';
 private $id;
 const DF_VOUCHER_ID = 'voucher_id';
 private $voucher_id;
 const DF_PRODUCT_ID = 'product_id';
 private $product_id;
 const DF_HSN_CODE = 'hsn_code';
 private $hsn_code;
 const DF_PRICE = 'price';
 private $price;
 const DF_DISCOUNT = 'discount';
 private $discount;
 const DF_REWARD_POINTS = 'reward_points';
 private $reward_points;
 const DF_CASH = 'cash';
 private $cash;
 const DF_TAXABLE_AMOUNT = 'taxable_amount';
 private $taxable_amount;
 const DF_QTY = 'qty';
 private $qty;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const DF_GST_SLAB = 'gst_slab';
 private $gst_slab;
 const DF_IGST_AMOUNT = 'igst_amount';
 private $igst_amount;
 const DF_SGST_AMOUNT = 'sgst_amount';
 private $sgst_amount;
 const DF_CGST_AMOUNT = 'cgst_amount';
 private $cgst_amount;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getVoucherId(){
 return $this->voucher_id;
 }
 public function getProductId(){
 return $this->product_id;
 }
 public function getHsnCode(){
 return $this->hsn_code;
 }
 public function getPrice(){
 return $this->price;
 }
 public function getDiscount(){
 return $this->discount;
 }
 public function getRewardPoints(){
 return $this->reward_points;
 }
 public function getCash(){
 return $this->cash;
 }
 public function getTaxableAmount(){
 return $this->taxable_amount;
 }
 public function getQty(){
 return $this->qty;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function getGstSlab(){
 return $this->gst_slab;
 }
 public function getIgstAmount(){
 return $this->igst_amount;
 }
 public function getSgstAmount(){
 return $this->sgst_amount;
 }
 public function getCgstAmount(){
 return $this->cgst_amount;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setVoucherId($voucher_id){
 $this->voucher_id = $voucher_id;
 }
 public function setProductId($product_id){
 $this->product_id = $product_id;
 }
 public function setHsnCode($hsn_code){
 $this->hsn_code = $hsn_code;
 }
 public function setPrice($price){
 $this->price = $price;
 }
 public function setDiscount($discount){
 $this->discount = $discount;
 }
 public function setRewardPoints($reward_points){
 $this->reward_points = $reward_points;
 }
 public function setCash($cash){
 $this->cash = $cash;
 }
 public function setTaxableAmount($taxable_amount){
 $this->taxable_amount = $taxable_amount;
 }
 public function setQty($qty){
 $this->qty = $qty;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 public function setGstSlab($gst_slab){
 $this->gst_slab = $gst_slab;
 }
 public function setIgstAmount($igst_amount){
 $this->igst_amount = $igst_amount;
 }
 public function setSgstAmount($sgst_amount){
 $this->sgst_amount = $sgst_amount;
 }
 public function setCgstAmount($cgst_amount){
 $this->cgst_amount = $cgst_amount;
 }
 function getDataSet(){ return get_object_vars($this);} }
class WalletTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'wallet' ; const DF_ID = 'id';
 private $id;
 const DF_CUSTOMER_ID = 'customer_id';
 private $customer_id;
 const DF_IS_DEBIT = 'is_debit';
 private $is_debit;
 const DF_AMOUNT = 'amount';
 private $amount;
 const DF_TXN_TITLE = 'txn_title';
 private $txn_title;
 const DF_TXN_DESC = 'txn_desc';
 private $txn_desc;
 const DF_TXN_DATE = 'txn_date';
 private $txn_date;
 const DF_PAYMENT_GATEWAY_ID = 'payment_gateway_id';
 private $payment_gateway_id;
 const DF_ORDER_ID = 'order_id';
 private $order_id;
 const DF_CREATED_AT = 'created_at';
 private $created_at;
 const AI_COL = 'id';
 private $PRI = array('id');
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getId(){
 return $this->id;
 }
 public function getCustomerId(){
 return $this->customer_id;
 }
 public function getIsDebit(){
 return $this->is_debit;
 }
 public function getAmount(){
 return $this->amount;
 }
 public function getTxnTitle(){
 return $this->txn_title;
 }
 public function getTxnDesc(){
 return $this->txn_desc;
 }
 public function getTxnDate(){
 return $this->txn_date;
 }
 public function getPaymentGatewayId(){
 return $this->payment_gateway_id;
 }
 public function getOrderId(){
 return $this->order_id;
 }
 public function getCreatedAt(){
 return $this->created_at;
 }
 public function setId($id){
 $this->id = $id;
 }
 public function setCustomerId($customer_id){
 $this->customer_id = $customer_id;
 }
 public function setIsDebit($is_debit){
 $this->is_debit = $is_debit;
 }
 public function setAmount($amount){
 $this->amount = $amount;
 }
 public function setTxnTitle($txn_title){
 $this->txn_title = $txn_title;
 }
 public function setTxnDesc($txn_desc){
 $this->txn_desc = $txn_desc;
 }
 public function setTxnDate($txn_date){
 $this->txn_date = $txn_date;
 }
 public function setPaymentGatewayId($payment_gateway_id){
 $this->payment_gateway_id = $payment_gateway_id;
 }
 public function setOrderId($order_id){
 $this->order_id = $order_id;
 }
 public function setCreatedAt($created_at){
 $this->created_at = $created_at;
 }
 function getDataSet(){ return get_object_vars($this);} }
class WalletBalanceTblProxy extends SelwynDataModels { 
 const TABLE_NAME = 'wallet_balance' ; const DF_CUSTOMER_ID = 'customer_id';
 private $customer_id;
 const DF_BALANCE = 'balance';
 private $balance;
 const AI_COL = '';
 private $PRI = array();
 function flush($primaryKeyArray = "")
{ if($primaryKeyArray == ""){
$primaryKeyArray = $this->PRI;} 
return parent::flush($this->PRI);} public function getCustomerId(){
 return $this->customer_id;
 }
 public function getBalance(){
 return $this->balance;
 }
 public function setCustomerId($customer_id){
 $this->customer_id = $customer_id;
 }
 public function setBalance($balance){
 $this->balance = $balance;
 }
 function getDataSet(){ return get_object_vars($this);} }