<?php

class HomeModel {

    static function dashboardProductsFx()
    {

        SelwynDatabase::query("SET @assetDomain='".ASSETS_DOMAIN."'",'update');
        SelwynDatabase::query("SET @productPath='".PRODUCT_PATH."'",'update');

        $trending = "SELECT pt.id,pt.packet_size, pt.name,pt.model,pt.brand,pt.sku,pt.mrp,pt.selling_price,pt.winwin_cashback_allowed,pt.avg_discount_rate,pt.is_trending,CONCAT(@assetDomain,@productPath,pi.url) as url
                     FROM product pt
                     INNER JOIN product_image pi ON pt.id = pi.product_id AND pi.is_primary = 1
                     WHERE pt.is_trending = 1 AND pt.is_online = 1
                     ORDER BY RAND()
                     LIMIT 10";


        $trending = SelwynDatabase::query($trending);

        $res['trending_products'] = $trending;

        $bestseller = "SELECT pt.id,pt.packet_size, pt.name,pt.model,pt.brand,pt.sku,pt.mrp,pt.selling_price,pt.winwin_cashback_allowed,pt.avg_discount_rate,pt.is_trending,CONCAT(@assetDomain,@productPath,pi.url) as url
                     FROM product pt
                     INNER JOIN product_image pi ON pt.id = pi.product_id AND pi.is_primary = 1
                     WHERE pt.is_trending = 0 AND pt.is_online = 1
                     GROUP BY pt.id
                     ORDER BY RAND()
                     LIMIT 10";

        $bestseller = SelwynDatabase::query($bestseller);

        $res['bestseller_product'] = $bestseller;

        $banner = "SELECT DISTINCT(pb.category_id),ct.name, CONCAT(@assetDomain,getBannerByCategory(pb.category_id)) as banner FROM product_banner pb
                INNER JOIN category ct ON pb.category_id = ct.id
                -- ORDER BY RAND()
                -- LIMIT 8";

        $banner = "SELECT CONCAT(@assetDomain,banner_url) as banner FROM product_banner";

        $res['banner'] = SelwynDatabase::query($banner);

        return $res;
    }

    static function getPrimaryCategoryOnlyFx()
    {

        $sql = "SELECT *  FROM ".CategoryTbl::TABLE_NAME." Where parent_id = 0 AND id != 1 ORDER BY name ASC";
        $res =  SelwynDatabase::query($sql);
        return $res;
    }
}