<?php

class StaffModel
{
    static function addStaffFx($obj)
    {
        $s = new AdminTbl();
        $s->setMobileNumber($obj->inputMobile);
        $s->setFirstName($obj->inputFirstName);
        $s->setLastName($obj->inputLastName);
        $s->setPosition($obj->inputPosition);
        $s->setPassword($obj->inputPassword);
        $staffId = $s->flush();
        
        return $staffId;
        
    }
    
    static function getAllStaffListFx($staffId = 0)
    {
        $sql = "SELECT * FROM ".AdminTbl::TABLE_NAME;
        if($staffId){
            $sql .= " WHERE id = ".$staffId;
        }
        return SelwynDatabase::query($sql);
    }
    
    static function updateStaffFx($obj)
    {
        $staff = AdminTbl::load($obj->inputStaffId);
        if($staff instanceof AdminTbl){}
        $staff->setFirstName($obj->inputFirstName);
        $staff->setLastName($obj->inputLastName);
        $staff->setPosition($obj->inputPosition);
        $staff->setMobileNumber($obj->inputMobileNumber);
        $staff->flush();
        return true;
    }
}
