<?php

class HsnModel{


    static function getAllHsnFx(){
        $sql = "SELECT hs.id,hs.hsn_code,hs.igst,hs.sgst,hs.total_gst,hs.cess,hs.created_at,am.id as admin_id, am.first_name, am.last_name
                FROM hsn hs
                LEFT JOIN admin am ON hs.admin_id = am.id";
        return SelwynDatabase::query($sql);
    }

    static function hsnAddEditFx($obj){

        $user = $_SESSION['ADMIN'];

        if($user instanceof AdminTbl){

        }
        
        $hsn =  new HsnTbl();
        $hsn->setHsnCode($obj->inputHSNCode);
        $hsn->setIgst($obj->inputIGst);
        $hsn->setSgst($obj->inputSGst);
        $hsn->setTotalGst($obj->inputTotalGst);
        $hsn->setCess($obj->inputCess);
        if($obj->inputHsnId != 0){
            $hsn->setAdminId($user->getId());
            $hsn->setCreatedAt('NOW()');
        }
        if($obj->inputHsnId != 0){
            $hsn->setId($obj->inputHsnId);
        }
        
        $hsnId = $hsn->flush();

        return $hsnId;

    }

    static function DeleteHsnFx($obj){
        $sql = "DELETE FROM hsn WHERE id = ".$obj->inputHsnId;
        SelwynDatabase::query($sql,'update');
    }
}