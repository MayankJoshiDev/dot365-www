<?php

class supportModel
{

    static function supportListFx($obj)
    {

        $customerId = $obj->customer_id;

        $sql = "SELECT st.id,st.ticket_number,st.customer_id,st.created_at,st.title, getSupportMessage(st.id) as message
                FROM support st 
                WHERE st.customer_id = $customerId ORDER BY id DESC";

        $list = SelwynDatabase::query($sql);


        return $list;

    }

    static function supportDetailFx($supportId)
    {
        $sql = "SELECT spm.id, spm.support_id, spm.customer_id, spm.admin_id, spm.message, spm.flag,
            DATE_ADD(spm.created_at, INTERVAL 330 MINUTE) as created_at
                FROM support_message spm 
                WHERE support_id = $supportId
                ORDER BY spm.created_at ASC";

        $res = SelwynDatabase::query($sql);

        foreach ($res as $key => $val) {
            if ($val->flag == "image") {
                $res[$key]->message = RESOURCE_DOMAIN . 'uploads/support/' . $res[$key]->message;
            }
            $last_message_id = $val->id;

        }

        foreach ($res as $key => $val) {
            $res[$key]->last_message_id = $last_message_id;
        }


        return $res;

    }

    static function getNewSupportMessages($supportId, $lastMessageId)
    {
        $sql = "SELECT spm.id, spm.support_id, spm.customer_id, spm.admin_id, spm.message, spm.flag,
                   DATE_ADD(spm.created_at, INTERVAL 330 MINUTE) as created_at
                FROM support_message spm
                WHERE support_id = $supportId AND spm.id > $lastMessageId
                ORDER BY spm.created_at ASC";

        $res = SelwynDatabase::query($sql);

        foreach ($res as $key => $val) {
            if ($val->flag == "image") {
                $res[$key]->message = RESOURCE_DOMAIN . 'uploads/support/' . $res[$key]->message;

            }
            $last_message_id = $val->id;
        }

        foreach ($res as $key => $val) {
            $res[$key]->last_message_id = $last_message_id;
        }

        return $res;
    }
}

