<?php


class orderModel
{



    static function placeOrderFx($obj)
    {


        SelwynDatabase::query("SET @assetDomain='" . ASSETS_DOMAIN . "'", 'update');
        SelwynDatabase::query("SET @productPath='" . PRODUCT_PATH . "'", 'update');

        /* parameters  */
        $addressId = $obj->deliver_address_id;
        $totalCash = $obj->total_cash;
        $paymentId = $obj->gateway_payment_id;
        $totalWinWinPoints = $obj->total_winwin_points;
        $totalOrder = round($obj->total_order,2);
		$totalOrderPaid = $obj->total_order_paid;
        $customerId = $obj->customer_id;
        $orderItems = $obj->order_items;
        $referralCode = $obj->referral_code;
        $paymentMethod = $obj->payment_method;
        $direct = $obj->direct;
        $order_notes = $obj->order_notes;



        /* parameters  */
        
        /*Block 0 Amount Orders*/
        if($totalOrder <= 0.99){
            $res['status'] = FALSE;
            $res['message'] = "No items in order. Please select atleast one item for order";
            return $res;
        }
        /*Block 0 Amount Orders*/

        /*First Capture the Payment*/
					if($paymentId != null and $paymentId != '' ){
							$wallet = new WalletTbl();
							$wallet->setCustomerId($customerId);
							$wallet->setTxnDate(date('Y-m-d'));
							$wallet->setTxnTitle('Add Money');
							$wallet->setTxnDesc("");
							$wallet->setIsDebit(0);
							$wallet->setAmount($totalOrderPaid);
							$wallet->setOrderId('0');
							$wallet->setPaymentGatewayId($paymentId);
							$wallet->setCreatedAt('NOW()');
							$wallet->flush();
					/*Add payment to Wallet*/
					}

		$payment="W";
        /*Now Deduct the RewardPoints From WinWin*/
        /*TODO Implement Reward Point System here*/
        /*Now Deduct the RewardPoints From WinWin*/

        /* order master  */
        $order = new OrdersTbl();
        $order->setCustomerId($customerId);
        $order->setAddressId($addressId);
        $order->setOrderTotal($totalOrder);
        $order->setWinwinCashback($totalWinWinPoints);
        $order->setCash($totalCash);
        $order->setOrderNumber(ORDER::number());
        $order->setStatus('0');
        $order->setCreatedAt('NOW()');
        $order->setEliteCashback(0);
        $order->setPaymentGatewayId($paymentId);
        $order->setCaptureResponse(json_encode($payment));
        $order->setPaymentMethod($paymentMethod);
        $order->setReferralCode($referralCode);
        $orderId = $order->flush();
        /* get order id */

        /*IF Order not COD*/
        if($paymentMethod != "COD"){
            /*Deduct The Wallet*/
            $wallet = new WalletTbl();
            $wallet->setCustomerId($customerId);
            $wallet->setTxnDate(date('Y-m-d'));
            $wallet->setTxnTitle('Order:'.$order->getOrderNumber());
            $wallet->setTxnDesc("");
            $wallet->setIsDebit(1);
            $wallet->setAmount($totalOrder);
            $wallet->setOrderId($orderId);
            $wallet->setPaymentGatewayId("");
            $wallet->setCreatedAt('NOW()');
            $wallet->flush();
        }

        /*$txn->setOrderId($orderId);
        $txn->setOrderNumber($order->getOrderNumber());
        $txn->flush();*/

        /* order items */
        $items = new OrderItemsTbl();
        $soldProductIdArray = array();
        foreach ($orderItems as  $val) {
            $items->setOrderId($orderId);
            $items->setSellingPrice($val->selling_price);
            if($direct == "yes") {
                $items->setQty($obj->qty);
                $items->setProductId($obj->product_id);
				$qty=$obj->qty;
            }
            else
            {
                $items->setQty($val->qty);
                $items->setProductId($val->product_id);
				$qty=$val->qty;
            }

            $items->setWinwinCashback($val->winwin_cashback_allowed);
            $items->setEliteCashback(0);
            $items->setGstSlab($val->gst_percent);
            $items->setCash($val->selling_price * $val->qty);
            $items->flush();
            if($direct == "no")
            {
                SelwynDatabase::query("DELETE FROM customer_cart WHERE product_id = ".$val->product_id." AND customer_id = ".$customerId,'update');
            }
			
			$data1= SelwynDatabase::query("Select qty_in_stock,name from product WHERE id = ".$val->product_id);
			$newqty = (int)$data1[0]->qty_in_stock - (int)$qty;
			$pname = $data1[0]->name;
			if($newqty>0)
			{					
							try {  
									SelwynDatabase::query("update product set qty_in_stock='$newqty' WHERE name = '".$pname."' LIMIT 1",'update');
							}   
							//catch block  
							catch (Exception $e) {  
									SelwynDatabase::query("update product set qty_in_stock='$newqty' WHERE name = '".$pname."' LIMIT 1",'update');
							}  
					
			}
			else 
			{
							try {  
									SelwynDatabase::query("update product set qty_in_stock='0' WHERE name = '".$pname."' LIMIT 1",'update');
							}   
							//catch block  
							catch (Exception $e) {  
									SelwynDatabase::query("update product set qty_in_stock='0' WHERE name = '".$pname."' LIMIT 1",'update');
							}  
						
			}

        }


        // Save Order Note

        if(!empty($order_notes)){
            $orderNotes = new OrderNotesTbl();
            $orderNotes->setNote($order_notes);
            $orderNotes->setOrderId($orderId);
            $orderNotes->setNoteby('customer');
            $orderNotes->setCreatedAt('NOW()');
            $orderNotes->flush();
        }



        $detail = "SELECT odi.id,odi.order_id,odi.product_id,odi.qty,odi.selling_price,odi.cash, odi.winwin_cashback, odi.elite_cashback, odi.gst_slab,
                  od.created_at, od.order_number,status, pt.name as product_name, pt.name_extension, pt.sku, pt.brand, pt.model, CONCAT(@assetDomain,@productPath,pti.url) as url,
                  ad.id,ad.line1,ad.line2, ad.landmark, ad.area,ad.city, st.name as state, ad.pincode,pt.unit
                  FROM order_items odi
                  INNER JOIN orders od ON odi.order_id = od.id
                  INNER JOIN product pt ON odi.product_id = pt.id
                  INNER JOIN customer_address ad ON od.address_id = ad.id
                  INNER JOIN state st ON ad.state_id = st.id 
                  INNER JOIN product_image pti ON odi.product_id = pti.product_id AND pti.is_primary = 1
                  WHERE odi.order_id = $orderId";

        $res['data']['order_detail'] = SelwynDatabase::query($detail);

        $res['data']['kart'] = cartModel::viewCustomerCartFx($customerId,true);

        $res['status'] = TRUE;

        /*Send Notifications*/
        $customer = CustomerTbl::load($customerId);
        if($customer instanceof CustomerTbl){}
        $smsStr = SMS::OrderTemplate($order->getOrderNumber());
        SMS::send($customer->getMobileNumber(),$smsStr);

        /*Admin Notifications*/
        $str1 = "Order Received From ".$customer->getFirstName().' '.$customer->getLastName().' Order id: '.$order->getOrderNumber().' of Rs '.$order->getOrderTotal();
        // SMS::sendAdmin(9769198502,$str1);
        // SMS::sendAdmin(9725999925,$str1);
        // SMS::sendAdmin(9321158986,$str1);
        // SMS::sendAdmin(9328640736,$str1);
        return $res;
    }

    static function orderHistoryFx($obj)
    {

        SelwynDatabase::query("SET @assetDomain='" . ASSETS_DOMAIN . "'", 'update');
        SelwynDatabase::query("SET @productPath='" . PRODUCT_PATH . "'", 'update');
        $customerId = $obj->customer_id;
        $orderNumber = $obj->order_number;
        $fromDate = $obj->from_date;
        $toDate = $obj->to_date;


        $history = "SELECT ods.id,ods.order_total,ods.cash,ods.customer_id,DATE_ADD(ods.created_at, INTERVAL 350 MINUTE) as created_at ,ods.order_number, ods.status
                    FROM orders ods
                    INNER JOIN customer_address ads ON ods.address_id = ads.id
                    WHERE ods.customer_id = $customerId";

        if ($orderNumber != NULL) {

            $history .= " AND ods.order_number = '" . $orderNumber . "'";
        }

        if ($fromDate != NULL AND $toDate != NULL) {
            $history .= " AND (DATE(ods.created_at) >= '" . $fromDate . "' AND DATE(ods.created_at) <= '" . $toDate . "')";
        }

        $history .= " ORDER BY ods.created_at DESC";

        $order = SelwynDatabase::query($history);
        $res['status'] = TRUE;
        $res['data']['order_history'] = $order;
        return $res;
    }


    static function orderDetailFx($obj)
    {
        SelwynDatabase::query("SET @assetDomain='" . ASSETS_DOMAIN . "'", 'update');
        SelwynDatabase::query("SET @productPath='" . PRODUCT_PATH . "'", 'update');

        $orderId = $obj->order_id;

        $order = "SELECT odi.id,ad.mobile_number,odi.order_id,pt.packet_size as pacaket,pt.unit as unit,odi.product_id,odi.qty,odi.selling_price,odi.cash, odi.winwin_cashback, odi.elite_cashback, odi.gst_slab,
                  DATE_ADD(od.created_at, INTERVAL 350 MINUTE) as created_at,od.id as main_id, od.order_number,od.status,od.cash as delivery,pt.name as product_name,od.order_total,od.payment_method, pt.name_extension, pt.sku, pt.brand, pt.model, CONCAT(@assetDomain,@productPath,pti.url) as url,
                  ad.id,ad.line1,ad.line2, ad.landmark, ad.area,ad.city, ad.address_lat, ad.address_long, st.name as state, ad.pincode
                  FROM order_items odi
                  INNER JOIN orders od ON odi.order_id = od.id
                  INNER JOIN product pt ON odi.product_id = pt.id
                  INNER JOIN customer_address ad ON od.address_id = ad.id
                  INNER JOIN state st ON ad.state_id = st.id 
                  INNER JOIN product_image pti ON odi.product_id = pti.product_id AND pti.is_primary = 1
                  WHERE od.id = $orderId";

        $res = SelwynDatabase::query($order);
        return $res;

    }

	static function orderAgianFx($obj)
	{
		SelwynDatabase::query("SET @assetDomain='" . ASSETS_DOMAIN . "'", 'update');
        SelwynDatabase::query("SET @productPath='" . PRODUCT_PATH . "'", 'update');
        $customerId = $obj->customer_id;
		
		$orderNumber = $obj->order_id;
		
		
        $orderItems=SelwynDatabase::query("SELECT oi.order_id,oi.product_id,oi.qty FROM order_items oi join orders o on oi.order_id=o.id WHERE oi.order_id='$orderNumber'");
		$res['items']=$orderItems;
		foreach($orderItems as $key => $val)
		{ 	
			$oitem = new CustomerCartTbl();
			$oitem->setProductId($val->product_id);
			$oitem->setCustomerId($customerId);
			$oitem->setAddedOn('NOW()');
			$oitem->setQty($val->qty);
			$res['data']= $oitem->flush();
			
			$res['status'] = true;
		}
		
	}
    static function orderReviewFx($obj)
    {

        $orderItemId = $obj->order_item_id;
        $productId = $obj->product_id;
        $customerId = $obj->customer_id;
        $rating = $obj->star_rating;
        $reviewText = $obj->review;
        $reviewId = $obj->review_id;


        $review = new ProductRatingTbl();
        $review->setCustomerId($customerId);
        $review->setOrderItemId($orderItemId);
        $review->setProductId($productId);
        $review->setStarRating($rating);
        $review->setReview($reviewText);
        if ($reviewId != 0) {
            $review->setId($reviewId);
        }
        $reviewId = $review->flush();


        $sql = "SELECT ptr.id, ptr.customer_id, ptr.product_id, ptr.order_item_id, ptr.star_rating, ptr.review,
                cr.first_name,cr.last_name
                FROM product_rating ptr
                INNER JOIN customer cr ON ptr.customer_id = cr.id
                WHERE ptr.id = $reviewId";

        $res = SelwynDatabase::query($sql);

        return $res;

    }

    static function orderShipmentFx($obj)
    {

        $orderId = $obj->order_id;
        $sql = "SELECT sp.id,sp.order_id,sp.shipment_number,sp.status,sp.admin_id, spl.note, spl.created_at FROM shipment sp
                INNER JOIN shipment_log spl ON sp.id = spl.shipment_id WHERE sp.order_id =  $orderId
                ORDER BY spl.created_at";

        $res = SelwynDatabase::query($sql);

        return $res;
    }


    static function orderCancelFx($obj)
    {

        if ($obj->order_status = 'cancel' OR $obj->order_status= '5') {

            $order = OrdersTbl::load($obj->order_id);
            if ($order instanceof OrdersTbl) {
            }

            $orderNumber = $order->getOrderNumber();
            $order->setStatus(5);
            $order->flush();

            $reason = "Order Cancelled : " . $orderNumber . ", Title: " . $obj->cancel_title . ", Reason:" . $obj->cancel_reason;

            $orderNotes = new OrderNotesTbl();
            $orderNotes->setNote($reason);
            $orderNotes->setOrderId($obj->order_id);
            $orderNotes->setCreatedAt('NOW()');
            $orderNotes->flush();
        }

        $res['status'] = TRUE;
        $res['order_id'] = $obj->order_id;
        return $res;
    }

    static function removeOrderItemFX($obj) {

        $orderId = $obj->order_id;
        $productId = $obj->product_id;
        $newtotal = $obj->ntotal;
        $sql = "DELETE FROM order_items WHERE order_id = $orderId AND product_id = $productId";

        $res = SelwynDatabase::query($sql,'update');

        if($res)
        {
            $sql1 = "UPDATE orders SET order_total = $newtotal WHERE id = $orderId";

            $res1 = SelwynDatabase::query($sql1,'update');

            $reason = "Order Item Remove :  Title: " . "Remove By Customer" . ", Reason:" . "";

            $orderNotes = new OrderNotesTbl();
            $orderNotes->setNote($reason);
            $orderNotes->setOrderId($obj->order_id);
            $orderNotes->setCreatedAt('NOW()');
            $orderNotes->flush();
        }

        $res2['status'] = TRUE;
        $res2['order_id'] = $obj->order_id;
        return $res2;
    }

    static function updateOrderItemFX($obj) {
        $orderId = $obj->order_id;
        $productId = $obj->product_id;
        $newqty = $obj->qty;

        $sql = "UPDATE order_items SET qty = $newqty WHERE order_id = $orderId AND product_id = $productId";

        $res = SelwynDatabase::query($sql,'update');

        $res2['status'] = TRUE;
        $res2['order_id'] = $obj->order_id;
        return $res2;
    }

    static function getWalletTxn($customerId)
    {
        $sql = "SELECT id,customer_id,DATE_FORMAT(txn_date,'%d/%m/%Y')as txn_date,
                txn_title,txn_desc,is_debit,amount
                FROM wallet
                WHERE customer_id = $customerId
                ORDER BY id desc
                ";
        return SelwynDatabase::query($sql);
    }

    static function getWalletBalanceFx($customerId)
    {
        /*Get New WalletBalance for This User*/
        $sql = "SELECT * FROM wallet_balance WHERE customer_id = " . $customerId;
        $bal = SelwynDatabase::query($sql);
        /*Get New WalletBalance for This User*/
        if ($bal == false OR $bal[0]->balance == NULL) {
            return 0;
        } else {
            return $bal[0]->balance;
        }


    }

	static function payOnlineFx($obj)
	{
		
		$sql = "update orders set payment_method='Online', payment_gateway_id='$obj->payment_id'  WHERE id=" . $obj->order_id;
		$bal = SelwynDatabase::query($sql,'update');	

		$res['status'] = TRUE;
		$res['order_id'] = $obj->order_id;
		return $res;

	}
}
