<?php

class AuthModel
{
    
//    static function loginFx($username, $password)
//    {
//        $fx = new stdClass();
//        $admin = AdminTbl::findOneBy(array(
//            AdminTbl::DF_MOBILE_NUMBER => $username,
//            AdminTbl::DF_PASSWORD => $password,
//        ));
//        if ($admin) {
//            if($admin instanceof AdminTbl){}
//            if($admin->getIsActive() == 1){
//                $adminLogin = AdminTbl::findOneBy($admin->getId());
//                $adminLogin->setLastLogin('NOW()');
//                $adminLogin->flush();
//
//                $fx->status = TRUE;
//                $fx->data = $admin;
//                /*$fx['message'] = "Username or Password Incorrect";*/
//            }else{
//                $fx->status = FALSE;
//                $fx->message = "User not active";
//            }
//        } else {
//            $fx->status = FALSE;
//            $fx->message = "Username or Password Incorrect";
//        }
//        return $fx;
//    }




    static function signupModelFx($obj)
    {

        $firstName = $obj->first_name;
        $lastName = $obj->last_name;
//        $email = $obj->email;
        $mobile = $obj->mobile;
//        $password = $obj->password;


        $sql = "SELECT id FROM customer WHERE mobile_number = '$mobile'";

        $checkCustomer = SelwynDatabase::query($sql);



        if($checkCustomer != null){
            $res['status'] = false;
            $res['message'] = "Customer Already Registered";
        }else{

            $customerObj = new CustomerTbl();
            $customerObj->setFirstName($firstName);
            $customerObj->setLastName($lastName);
            $customerObj->setMobileNumber($mobile);
            $customerObj->setIsActive('1');
            $otp = OTP::generate();
            $otpObj = new RegisterOtpTbl();
            $otpObj->setMobileNumber($mobile);
            $otpObj->setOtp($otp);
            $otpObj->setCreatedAt('NOW()');
            $otpObj->flush();
            $customerObj->setOtp($otp);
            //SMS::send($mobile,$otp." is your OTP for Signup on Dot365 App.");
            SMS::sendOTP($mobile,$otp);
            $customerId = $customerObj->flush();
            $res['data']['customer_id'] = $customerId;
            $res['data']['mobilenumber'] = $mobile;
            $res['status'] = true;
        }

        return $res;

    }

    static function getCustomerFx($id)
    {
        $sql = "SELECT * FROM customer WHERE id = '$id' LIMIT 1";

        $checkCustomer = SelwynDatabase::query($sql);

        return $checkCustomer;
    }


    static function getAllStateListFx(){
        $sql = "SELECT st.id,st.name,st.code,st.short_name FROM state st";

        return SelwynDatabase::query($sql);
    }
}
