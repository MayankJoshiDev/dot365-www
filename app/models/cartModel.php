<?php

class cartModel
{


    static function customerAddCartFx($obj)
    {

       $product = $obj->product;

        $cart = new CustomerCartTbl();
        foreach ($product as $pt) {

            $cart->setCustomerId($obj->customer_id);
            if ($pt['product_id'] != null) {
                $cart->setProductId($pt['product_id']);
            }
            $cart->setAddedOn('NOW()');
            if ($obj->cart_id != 0) {
                $cart->setId($obj->cart_id);
            }
            if ($pt['qty'] != 0) {
                $cart->setQty($pt['qty']);
            }
            $cartId[] = $cart->flush();
        }


        $res['cart_id'] = $cartId;
        $res['status'] = true;
        return $res;
    }
    static function customerEditCartFx($obj)
    {
    
        $cart = CustomerCartTbl::load($obj->cart_id);
        $cart->setQty($obj->qty);
        $cart->flush();
        
        $res['status'] = true;
        return $res;
    }


    static function viewCustomerCartFx($uid,$flag=false)
    {

        SelwynDatabase::query("SET @assetDomain='" . ASSETS_DOMAIN . "'", 'update');
        SelwynDatabase::query("SET @productPath='" . PRODUCT_PATH . "'", 'update');
//        $customerId = $obj->customer_id;
        $customerId = $uid;

        $kart = "SELECT ctc.id,pt.packet_size as pacaket, pt.unit as unit,pt.sku,ctc.product_id,ctc.customer_id,ctc.added_on,ctc.qty,
                pt.name, pt.name_extension, pt.selling_price, pt.mrp, pt.winwin_cashback_allowed,pt.gst_percent, CONCAT(@assetDomain,@productPath,pti.url) as url
                FROM customer_cart  ctc
                INNER JOIN product pt ON ctc.product_id = pt.id
                INNER JOIN product_image pti ON  ctc.product_id = pti.product_id AND is_primary = 1
                WHERE ctc.customer_id = $customerId";

        if($flag){
            return SelwynDatabase::query($kart);
        }else{
            $res['data']['kart'] = SelwynDatabase::query($kart);
            $res['status'] = true;
            return $res;
        }
        
    }


    static function addFavouriteFx($obj)
    {
        SelwynDatabase::query("SET @assetDomain='" . ASSETS_DOMAIN . "'", 'update');
        SelwynDatabase::query("SET @productPath='" . PRODUCT_PATH . "'", 'update');
        $customerId = $obj;
        $customerId = $obj->customer_id;
        $productId = $obj->product_id;
        $action = $obj->action;

        if ($action == "add")
        {
            $check  = FavouriteProductTbl::find(array(
                FavouriteProductTbl::DF_CUSTOMER_ID => $customerId,
                FavouriteProductTbl::DF_PRODUCT_ID => $productId
            ));

            if (!$check) {
                $favouriteProduct = new FavouriteProductTbl();
                $favouriteProduct->setCustomerId($customerId);
                $favouriteProduct->setProductId($productId);
                $favouriteProductId = $favouriteProduct->flush();
                $res['favouriteProductId'] = $favouriteProductId;

            }

        }
        else
        {
            $kart = " DELETE  FROM favourite_product WHERE customer_id = $customerId AND product_id = $productId";
            $res['id'] = SelwynDatabase::query($kart, 'update');
        }

        $res['status'] = true;
        return $res;
    }

    static function customerFavcountFX($id)
    {
        $userid = $id;
        $sql = "SELECT COUNT(*) as favcount FROM favourite_product WHERE customer_id = $userid";
        $res = SelwynDatabase::query($sql);
        return $res;
    }

    static function customercountFX($id)
    {
        $userid = $id;
        $sql = "SELECT COUNT(*) as cartcount FROM customer_cart WHERE customer_id = $userid";
        $res = SelwynDatabase::query($sql);
        return $res;
    }

    static function customerCartDeleteFx($obj)
    {

        $kartId = $obj->cart_id;
        $sql = "DELETE FROM customer_cart WHERE id = $kartId";
        $res = SelwynDatabase::query($sql, 'update');
        return $res;
    }


}