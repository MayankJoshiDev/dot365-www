<?php

class CategoryModel
{
    static function getCategoryListFx()
    {

        $sql = "SELECT * FROM ".CategoryTbl::TABLE_NAME." Where parent_id = 0 AND is_deleted  != 1 ORDER BY ".CategoryTbl::DF_NAME." ASC";
        $category =  SelwynDatabase::query($sql);
        foreach ($category as $key => $val)
        {
            $sql = "SELECT * FROM ".CategoryTbl::TABLE_NAME." Where is_deleted  != 1 AND parent_id = ".$val->id;
            $subCategory = SelwynDatabase::query($sql);
            foreach ($subCategory as $key1 => $sub){
                $subCategory[$key1]-> category_image = ASSETS_DOMAIN.''.CATEGORY_PATH.''.$subCategory[$key1]->category_image;
            }
            $category[$key]->sub = $subCategory;
            $category[$key]->category_image = ASSETS_DOMAIN.''.CATEGORY_PATH.''.$category[$key]->category_image;

        }
        return $category;
    }
    static function getPrimaryCategoryOnlyFx()
    {
        $sql = "SELECT * FROM ".CategoryTbl::TABLE_NAME." Where parent_id = 0 AND is_deleted  != 1 ORDER BY ".CategoryTbl::DF_NAME." asc";
        $category =  SelwynDatabase::query($sql);
        return $category;
    }
    
    
    
    static function categoryAddUpdateFx($obj)
    {


        if($obj->inputIsDelete ==1){
            $sql = "UPDATE ".CategoryTbl::TABLE_NAME." SET is_deleted=1 Where id = ".$obj->inputCategoryId;
            SelwynDatabase::query($sql,'update');
            return true;
        }


        if(isset($obj->files)) {


            $Files = $obj->files['inputCategoryFile'];

            $targetDir = "assets/img/category/";
            if (!file_exists($targetDir)) {
                mkdir($targetDir, 0777, TRUE);
            }
            $fileName = basename($Files['name']);
            $targetFilePath = $targetDir . $fileName;


            if (!empty($Files['name'])) {


                $mainFileName = time();
                /*Upload Original File*/
                $ext = pathinfo($Files['name'], PATHINFO_EXTENSION);

                /*Upload Original File*/

                move_uploaded_file($Files['tmp_name'], $targetDir . $mainFileName . "." . $ext);
                if ($mainFileName) {
                    // Insert image file name into database
                    if ($obj->inputCategoryId) {

                        $category = CategoryTbl::load($obj->inputCategoryId);
                    } else {

                        $category = new CategoryTbl();
                    }

                    $category->setName($obj->inputName);
                    $category->setParentId($obj->inputParentId);
                    $category->setCategoryImage($mainFileName . '.' . $ext);
                    $categoryId = $category->flush();


                    if ($categoryId) {
                        $statusMsg = "The file " . $fileName . " has been uploaded successfully.";
                    } else {
                        $statusMsg = "File upload failed, please try again.";
                    }
                } else {
                    $statusMsg = "Sorry, there was an error uploading your file.";
                }
            } else {

                if ($obj->inputCategoryId) {
                    $category = CategoryTbl::load($obj->inputCategoryId);
                } else {
                    $category = new CategoryTbl();
                }
                $category->setName($obj->inputName);
                $category->setParentId($obj->inputParentId);

                $categoryId = $category->flush();
            }


        }



        return true;
    }


    static function getSubCategoryFx($categoryId=0){
        $sql = "SELECT ct.id, ct.name, ct.parent_id,COUNT(pc.id) as product_count,category_image
                FROM category ct
                LEFT JOIN product_category pc on pc.category_id = ct.id
                WHERE ct.parent_id != 0 AND ct.is_deleted  != 1";
        if($categoryId){
            $sql .= " and ct.parent_id = $categoryId";
        }
        $sql .=" GROUP BY ct.id
                ORDER BY ct.name asc;";
        
        return SelwynDatabase::query($sql);
    }
    
    static function getAllCategoryFx($categoryId=0){
        $sql = "SELECT ct.id, ct.name, ct.parent_id,COUNT(pc.id) as product_count
                FROM category ct
                LEFT JOIN product_category pc on pc.category_id = ct.id
                WHERE ct.is_deleted  != 1";
        if($categoryId){
            $sql .= " and ct.parent_id = $categoryId";
        }
        $sql .=" GROUP BY ct.id
                ORDER BY ct.name asc;";
        
        return SelwynDatabase::query($sql);
    }

    static function getProductCategoryFx($productId){
        $sql = "SELECT id,product_id,category_id,parent_category_id FROM product_category WHERE product_id = $productId";
             return SelwynDatabase::query($sql);
    }



}
