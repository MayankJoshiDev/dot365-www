<?php

class GroupModel
{


    static function getAllGroupListFx(){

        $sql = "SELECT ag.id,ag.name, ag.under_group, ag.group_type,ag.is_locked, acg.name as group_name FROM acc_group ag
                LEFT JOIN acc_group acg ON ag.under_group = acg.id";


            $check = SelwynDatabase::query($sql);

            foreach ($check as $key => $val){
                if($val->under_group == '0'){
                    $val->group_name = 'Primary';
                }
            }

        return $check;


    }

    static function getAllGroupsFx(){

        $sql = "SELECT ag.id,ag.name FROM acc_group ag";
        return SelwynDatabase::query($sql);
    }


    static function getGroupListFx($obj){


        $sql = "SELECT ag.id,ag.name,ag.under_group,ag.group_type,ag.is_locked
                FROM acc_group ag WHERE ag.id = ".$obj;



        return SelwynDatabase::query($sql);

    }


    static function updateGroupListFx($obj){

        $v = new AccGroupTbl();
        $v->setId($obj->inputGroupId);
        $v->setGroupType($obj->inputGroupType);
        $v->setName($obj->inputGroupName);
        $v->setIsLocked($obj->inputIsLocked);
        $v->setUnderGroup($obj->inputUnderGroup);

        $groupId = $v->flush();
        return $groupId;

    }

    static function addGroupFx($obj){

            $lock = '0';
            $v = new AccGroupTbl();
            $v->setName($obj->inputGroupName);
            $v->setGroupType($obj->inputGroupType);
            $v->setUnderGroup($obj->inputUnderGroup);
            $v->setIsLocked($lock);
            $groupId = $v->flush();

        return $groupId;
    }

    static function deleteGroupFx($obj){
        if ($obj->inputIsLocked == '1' )
        {
            echo "failed";
        }
        else
        {
            $sql = "DELETE FROM acc_group WHERE id = ".$obj->inputGroupId." AND is_locked = '0'";
            SelwynDatabase::query($sql,'update');
        }
    }



}