<?php

class VendorModel
{


    static function getAllVendorListFx(){

        $sql = "SELECT vr.id,vr.company_name,vr.person_name,vr.mobile_number,vr.city,vr.address_line1,vr.address_line2,
                st.id as state_id, st.name as state_name
                FROM vendor vr
                INNER JOIN state st ON vr.state_id = st.id";

        return SelwynDatabase::query($sql);

    }

    static function addVendorFx($obj){

            $v = new VendorTbl();
            $v->setCompanyName($obj->inputCompanyName);
            $v->setPersonName($obj->inputContactPerson);
            $v->setMobileNumber($obj->inputMobile);
            $v->setCity($obj->inputCity);
            $v->setStateId($obj->inputState);
            $v->setAddressLine1($obj->inputAddressLine1);
            $v->setAddressLine2($obj->inputAddressLine2);
            if($obj->inputVendorId != null){
                $v->setId($obj->inputVendorId);
            }
            $vendorId = $v->flush();

        return $vendorId;
    }

    static function deleteVendorFx($obj){
        $sql = "DELETE FROM vendor WHERE id = ".$obj->inputVendorId;
        SelwynDatabase::query($sql,'update');
    }



}