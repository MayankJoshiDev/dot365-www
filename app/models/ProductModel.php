<?php


class ProductModel
{


    static function productMenuFx()
    {

        SelwynDatabase::query("SET @assetDomain='" . ASSETS_DOMAIN . "'", 'update');
        SelwynDatabase::query("SET @categoryPath='" . CATEGORY_PATH . "'", 'update');

        $sql = "SELECT * FROM " . CategoryTbl::TABLE_NAME . " Where parent_id = 0 AND is_deleted  != 1 ORDER BY name ASC";
        $res['data']['category'] = $category = SelwynDatabase::query($sql);
        foreach ($category as $key => $val) {
            $sql = "SELECT id,name,parent_id,is_active,is_deleted,CONCAT(@assetDomain,@categoryPath,category_image) as category_image FROM " . CategoryTbl::TABLE_NAME . " Where is_deleted  != 1 AND parent_id = " . $val->id . " ORDER BY name ASC";
            $subCategory = SelwynDatabase::query($sql);
            $category[$key]->sub = $subCategory;
            $category[$key]->category_image = ASSETS_DOMAIN . '' . CATEGORY_PATH . '' . $category[$key]->category_image;

        }
        $res = $category;
        return $res;
    }
	static function getdeliverychrg_oid($obj)
	{
		$orderId = $obj->order_id;
		$rateperkg = $obj->rate_kg;
		$sql = "SELECT pt.weight_kg, oi.qty,sum(pt.weight_kg * oi.qty)*$rate_kg as tt_dcharge FROM `product` pt INNER JOIN order_items oi ON pt.id = oi.product_id WHERE oi.order_id = $orderId";
		$res = SelwynDatabase::query($sql);
        return $res;
	}
	static function getdeliverychrg_cid($uid,$rate_kg)
	{ 
			$res="no data still";
		try{
			$customerId = $uid;
			$rateperkg = $rate_kg;
			$sql = "SELECT cc.product_id,cc.qty,p.weight_kg,sum(cc.qty*p.weight_kg) as total_weight,(sum(cc.qty*p.weight_kg)*$rateperkg)/0.5 as tt_dcharge FROM `customer_cart`cc INNER JOIN product p on cc.product_id=p.id WHERE cc.customer_id= $customerId";
			$res = SelwynDatabase::query($sql);
			$res1="all working";
		}
		catch (Exception $e) {  
			//code to print exception caught in the block  
			$res=$e;
		}  
		
        return $res;
	}
	
    static function allproductListFx()
    {



        SelwynDatabase::query("SET @assetDomain='" . ASSETS_DOMAIN . "'", 'update');
        SelwynDatabase::query("SET @productPath='" . PRODUCT_PATH . "'", 'update');


            $sql = "SELECT pt.id,pt.packet_size as pacaket, pt.unit as unit, pt.name,pt.model,pt.selling_price,pt.mrp,pt.brand,pt.avg_discount_rate,pt.winwin_cashback_allowed, pt.is_online,
                ptc.category_id, ptc.parent_category_id, CONCAT(@assetDomain,@productPath,pti.url) as url,
                'true' as is_fav
                FROM product pt
                INNER JOIN product_category ptc ON pt.id = ptc.product_id
                INNER JOIN favourite_product fpc ON pt.id = fpc.product_id
                INNER JOIN product_image pti ON pt.id =  pti.product_id AND is_primary = 1
                ";
            $sql .= " WHERE is_online = 1 AND is_online = 1 ";



            $sql .= " ORDER BY pt.name ASC ";


        $res['data']['product_list'] = SelwynDatabase::query($sql);
        $res['data']['debug'] =$sql;
        $res['status'] = TRUE;
        return $res;


    }

    static function productListFx($obj)
    {

        $category = $obj->category_id;
        $subCategory = 0;
        $brand = (isset($obj->brand)) ? $obj->brand : '';
        $orderBy = $obj->order_by;
        $favourite = $obj->favourite;
        $customer_id = $obj->customer_id;

        SelwynDatabase::query("SET @assetDomain='" . ASSETS_DOMAIN . "'", 'update');
        SelwynDatabase::query("SET @productPath='" . PRODUCT_PATH . "'", 'update');

        if ($favourite != 'yes') {
            $sql = "SELECT pt.id,pt.packet_size as pacaket, pt.unit as unit, pt.name,pt.model,pt.selling_price,pt.mrp,pt.brand,pt.avg_discount_rate,pt.winwin_cashback_allowed, pt.is_online,
                ptc.category_id, ptc.parent_category_id, CONCAT(@assetDomain,@productPath,pti.url) as url,
                IF(ISNULL(fpc.id),'false','true') as is_fav
                FROM product pt
                INNER JOIN product_category ptc ON pt.id = ptc.product_id
                INNER JOIN product_image pti ON pt.id =  pti.product_id AND is_primary = 1
                LEFT JOIN favourite_product fpc on pt.id = fpc.product_id AND fpc.customer_id = $customer_id
                ";
            $sql .= " WHERE is_online = 1 AND is_online = 1 AND ptc.category_id = ".$category;


            $categoryWhereClause = '';
            /*if ($category != 0 OR $category != NULL) {
                $categoryWhereClause = ' AND (';
                foreach ($category as $key => $val) {
                    if ($key != 0) {
                        $categoryWhereClause .= " OR ";
                    }
                    $categoryWhereClause .= " ptc.parent_category_id = " . $val->id;
                }
                $categoryWhereClause .= ")";
            }*/
            // $sql .= $categoryWhereClause;

            $subCatWhereClause = '';
            if ($subCategory != 0 OR $subCategory != NULL) {
                $subCatWhereClause = ' AND (';
                foreach ($subCategory as $key => $val) {
                    if ($key != 0) {
                        $subCatWhereClause .= ' OR ';
                    }
                    $subCatWhereClause .= " ptc.category_id = " . $val->id;
                }
                $subCatWhereClause .= ' ) ';
            }
            $sql .= $subCatWhereClause;

            if ($brand != 0 OR $brand != NULL) {
                foreach ($brand as $key => $val) {
                    //$sql .= "OR pt.brand LIKE '$val->name' ";
                }
            }
            
           
        }
        else{
            $sql = "SELECT pt.id,pt.packet_size as pacaket, pt.unit as unit, pt.name,pt.model,pt.selling_price,pt.mrp,pt.brand,pt.avg_discount_rate,pt.winwin_cashback_allowed, pt.is_online,
                ptc.category_id, ptc.parent_category_id, CONCAT(@assetDomain,@productPath,pti.url) as url,
                'true' as is_fav
                FROM product pt
                INNER JOIN product_category ptc ON pt.id = ptc.product_id
                INNER JOIN favourite_product fpc ON pt.id = fpc.product_id
                INNER JOIN product_image pti ON pt.id =  pti.product_id AND is_primary = 1
                ";
            $sql .= " WHERE is_online = 1 AND is_online = 1 AND  fpc.customer_id = $customer_id";
        }

        if ($orderBy == 1) {
            $sql .= " ORDER BY pt.winwin_cashback_allowed ASC ";
        } else if ($orderBy == 2) {
            $sql .= " ORDER BY pt.winwin_cashback_allowed ASC ";
        }

       
        $res['data']['product_list'] = SelwynDatabase::query($sql);
        $res['data']['debug'] =$sql;
        $res['status'] = TRUE;
        return $res;


    }


    static function productDetailFx($obj)
    {
        $productId = $obj->product_id;
        if(isset($obj->customer_id)){
            $customerId = $obj->customer_id;
        }else{
            $customerId = 0;
        }

        SelwynDatabase::query("SET @assetDomain='" . ASSETS_DOMAIN . "'", 'update');
        SelwynDatabase::query("SET @productPath='" . PRODUCT_PATH . "'", 'update');
        SelwynDatabase::query("SET @resourceDomain='" . RESOURCE_DOMAIN . "'", 'update');
        SelwynDatabase::query("SET @profilePath='" . IMAGE_PATH . "'", 'update');


        $product = "SELECT pt.id,pt.packet_size as pacaket, pt.unit as unit, pt.name,pt.model,pt.selling_price,pt.mrp,pt.brand,
                pt.qty_in_stock,pt.sku, pt.addon_applicable, pt.addon_json,
                pt.winwin_cashback_allowed,'0' as avg_discount_rate,pt.gst_percent,
                pt.size_applicable,pt.color_applicable,IF(ISNULL(fpc.id),'false','true') as is_fav,
                ptc.category_id
                    FROM product pt
                    INNER JOIN product_category ptc ON pt.id = ptc.product_id
                    LEFT JOIN favourite_product fpc on pt.id = fpc.product_id and fpc.customer_id = $customerId
                    WHERE pt.id = $productId AND pt.is_online = 1 LIMIT 1";
        $res['data']['product_detail'] = $product1 = SelwynDatabase::query($product);


        $addon = json_decode($product1[0]->addon_json);

        if($addon){
            foreach ($addon as $key4 => $add) {
                $p = self::addOnProductFx($add);
                $addOnProducts[] = $p[0] ;
            }
            $product1[0]->addon_json = $addOnProducts;
        }


        $res['data']['product_detail'] = $product1;

        /*product Size Query*/

        $res['data']['product_detail'][0]->size = self::getColorSizeOfProduct($productId);
        /*product Size Query*/
        $image = "SELECT pti.id, CONCAT(@assetDomain,@productPath,pti.url) as url ,pti.index, pti.is_lifestyle,pti.is_primary
                  FROM product_image pti 
                  WHERE pti.product_id = $productId 
                  ORDER BY pti.index";

        $res['data']['product_images'] = SelwynDatabase::query($image);


        $review = "SELECT ptr.id,ptr.customer_id,ptr.product_id,ptr.star_rating,ptr.review, ct.first_name, ct.last_name, ct.profile_image as profile_image
                   FROM product_rating ptr
                   INNER JOIN customer ct ON ptr.customer_id = ct.id 
                   WHERE ptr.product_id = $productId";

        $review = SelwynDatabase::query($review);

        foreach ($review as $key => $rv) {
            if ($rv->profile_image != NULL) {
                $review[$key]->profile_image = RESOURCE_DOMAIN . '' . IMAGE_PATH . '' . $rv->profile_image;
            } else {
                $review[$key]->profile_image = RESOURCE_DOMAIN . '' . IMAGE_PATH . 'user.svg';
            }
        }
        $res['data']['product_review'] = $review;


        $userstar = array(5, 4, 3, 2, 1);


        foreach ($userstar as $key => $val) {
            $rating = "SELECT ptr.product_id,ptr.star_rating ,COUNT(ptr.review) as review, COUNT(ptr.review)  as average
                   FROM product_rating ptr
                   WHERE ptr.product_id = $productId AND ptr.star_rating = $val";

            $rate[$userstar[$key]] = SelwynDatabase::query($rating);

            $sql = "SELECT COUNT(ptr.review) as total
                   FROM product_rating ptr
                   WHERE ptr.product_id = $productId";

            $total = SelwynDatabase::query($sql);

            $total = $total[0]->total;

            foreach ($rate[$userstar[$key]] as $key1 => $rt) {

                if ($total != 0 AND $rt->average != 0) {


                    $rate[$userstar[$key]][$key1]->average = $total / $rt->average;


                } else {
                    $rate[$userstar[$key]][$key1]->average = '0';
                }


            }

            $res['data']['star_review'] = $rate;


        }


        $average = "SELECT ptr.id, ptr.product_id,SUM(ptr.star_rating)/COUNT(ptr.star_rating) as avg_star, COUNT(ptr.review) as review
                   FROM product_rating ptr
                   WHERE ptr.product_id = $productId";
        $res['data']['average_review'] = SelwynDatabase::query($average);


        $rateCount = "SELECT COUNT(ptr.id) as total_rating FROM product_rating ptr WHERE ptr.product_id = $productId";
        $res['data']['product_review_count'] = SelwynDatabase::query($rateCount);


        $specification = "SELECT pts.id,pts.product_id,pts.title,pts.text
                          FROM product_specification  pts
                          WHERE pts.id = $productId";

        $res['data']['product_specification'] = SelwynDatabase::query($specification);


        $sub = $res['data']['product_detail'][0]->category_id;

        $related = "SELECT pt.id,pt.name,pt.model,pt.selling_price,pt.mrp, pt.avg_discount_rate, pt.brand, ptc.category_id,
                    CONCAT(@assetDomain,@productPath,pti.url) as url 
                    FROM product pt
                    INNER JOIN product_category ptc ON pt.id = ptc.product_id
                    INNER JOIN product_image pti ON pt.id =  pti.product_id AND is_primary = 1 
                    WHERE ptc.category_id = $sub AND pt.is_online = 1
                    ORDER BY RAND()
                    LIMIT 10";

        $res['data']['related_products'] = SelwynDatabase::query($related);
        $res['status'] = TRUE;

        return $res;
    }


    static function productSearchFx($search)
    {
        $sql = "SELECT ps.id,ps.name,ps.url,ps.selling_price FROM product_search ps";

        if ($search != null){
            $productLike = $search;
            $sql .= " WHERE ps.name like '%$productLike%'";
        }


        $sql .= " ORDER BY ps.name  LIMIT 1000";

        $res = SelwynDatabase::query($sql);

        foreach ($res as $key => $val) {
            if ($val->url != '') {
                $res[$key]->url = ASSETS_DOMAIN . PRODUCT_PATH . $val->url;
            }
        }

        return $res;
    }

    static function getColorSizeOfProduct($productId)
    {
        $finalArray = array();
        $product = ProductTbl::load($productId);
        if ($product instanceof ProductTbl) {
        }
        if ($product->getSizeApplicable() == 1 || $product->getColorApplicable() == 1) {
            $productSizeSql = "SELECT DISTINCT(size) as 'size' FROM product_size WHERE product_id = $productId and in_stock = 1 ";
            $size = SelwynDatabase::query($productSizeSql);
            foreach ($size as $k => $v) {
                $s1 = new stdClass();
                $s1->color_applicable = $product->getColorApplicable();
                $s1->size_name = $v->size;
                $sql = "SELECT sku,mrp,selling_price,cashback,product_color FROM product_size WHERE product_id = $productId and in_stock = 1 AND `size` =  '" . $v->size . "'";
                $s1->colors = SelwynDatabase::query($sql);
                $finalArray[] = $s1;
            }
        }
        return $finalArray;
    }


    static function addOnProductFx($productId)
    {


        $json = "SELECT pt.id,pt.name,pt.model,pt.selling_price,pt.mrp,pt.brand,
                pt.qty_in_stock,pt.sku, pt.addon_applicable, pt.addon_json, 
                pt.winwin_cashback_allowed,'0' as avg_discount_rate,
                pt.size_applicable,pt.color_applicable,
                ptc.category_id,  CONCAT(@assetDomain,@productPath,pti.url) as url
                    FROM product pt
                    INNER JOIN product_category ptc ON pt.id = ptc.product_id 
                    LEFT JOIN  product_image pti ON pt.id = pti.product_id AND pti.is_primary = 1
                    WHERE pt.id = $productId AND pt.is_online = 1";
        return SelwynDatabase::query($json);

    }


}