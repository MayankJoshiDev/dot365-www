<?php

class customerModel
{


    static function customerProfileModelFx($obj)
    {
        $customerId = $obj->customer_id;

        $sql = "SELECT cr.id,cr.mobile_number,cr.profile_image,cr.password,cr.first_name,cr.last_name,cr.gender,cr.dob,cr.is_active,cr.is_verified,cr.otp,cr.otp_expires,cr.email
                FROM customer cr
                WHERE cr.id = $customerId";

        $sql1 = "SELECT cad.id,cad.gst_number,cad.customer_id,cad.customer_name, cad.mobile_number,cad.address_name,cad.line1,cad.line2,cad.landmark,cad.area,cad.city,cad.state_id,st.name as state,cad.pincode, cad.address_lat, cad.address_long
                 FROM customer_address cad
                 INNER JOIN state st ON cad.state_id = st.id
                 WHERE customer_id = $customerId";


        $res['data']['customer_detail'] = SelwynDatabase::query($sql);

        if ($res) {
            if(isset($res['data']['customer_detail'][0]->profile_image)){
                if ($res['data']['customer_detail'][0]->profile_image != '') {
                    $res['data']['customer_detail'][0]->profile_image = RESOURCE_DOMAIN . 'uploads/profile_photos/' . $res['data']['customer_detail'][0]->profile_image;
                } else {
                    $res['data']['customer_detail'][0]->profile_image = RESOURCE_DOMAIN . 'uploads/profile_photos/defaultprofile.jpg';
                }
            }

        }

        $res['data']['customer_address'] = SelwynDatabase::query($sql1);
        $res['status'] = TRUE;

        return $res;
    }
    static function editCustomerProfile($obj)
    {

        $customerId = $obj->customer_id;
        $firstName = $obj->first_name;
        $lastName = $obj->last_name;
        $gender = $obj->gender;
        $dob = $obj->dob;
        $mobile_number = $obj->mobile_number;
        $email = $obj->email;

        $customer = CustomerTbl::load($customerId);
        if ($customer instanceof CustomerTbl) {

        }
        $customer->setFirstName($firstName);
        $customer->setLastName($lastName);
        $customer->setGender($gender);
        $customer->setDob($dob);
        $customer->setEmail($email);
//        $customer->setMobileNumber($mobile_number);
        $customerId = $customer->flush();

        $res['customer_id'] = $customerId;
        $res['status'] = TRUE;

        return $res;

    }

    static function customerProfileUpload($obj, $file)
    {

        $customerId = $obj->customer_id;

        $fileName = $customerId . '_' . time() . '.jpg';

        if (!is_dir(ROOT_DIR . '/uploads/profile_photos')) {
            mkdir(ROOT_DIR . '/uploads/profile_photos', 0777, TRUE);
        }

        $photo = move_uploaded_file($file['tmp_name'], ROOT_DIR . '/uploads/profile_photos/' . $fileName);
        if ($photo) {
            $c = CustomerTbl::load($customerId);
            if ($c instanceof CustomerTbl) {
            }
            $c->setProfileImage($fileName);
            $c->flush();
            $data['status'] = TRUE;
        } else {
            $data['status'] = FALSE;
            $data['message'] = "File Upload failed. please try again";
        }

        return $data;
    }

    static function customerAddressModelFx($obj)
    {

        $customerId = $obj->customer_id;
        $addressId = $obj->address_id;
        $addressName = $obj->address_name;
        $customerName = $obj->customer_name;
        $mobileNumber = $obj->mobile_number;
        $line1 = $obj->line1;
        $line2 = $obj->line2;
        $landmark = $obj->landmark;
        $area = $obj->area;
        $city = $obj->city;
        $stateId = $obj->state_id;
        $pincode = $obj->pincode;
        $addressLat = $obj->address_lat;
        if(!isset($obj->gst_number)){
            $obj->gst_number = '';
        }

        $addressLong = $obj->address_long;

        $address = new CustomerAddressTbl();
        $address->setAddressName($addressName);
        $address->setCustomerId($customerId);
        $address->setCustomerName($customerName);
        $address->setMobileNumber($mobileNumber);
        $address->setLine1($line1);
        $address->setLine2($line2);
        $address->setLandmark($landmark);
        $address->setArea($area);
        $address->setCity($city);
        $address->setStateId($stateId);
        $address->setPincode($pincode);
        $address->setAddressLat($addressLat);
        $address->setGstNumber($obj->gst_number);
        $address->setAddressLong($addressLong);
        if ($addressId != 0) {
            $address->setId($addressId);
        }
        $addressId = $address->flush();
        $res['address_id'] = $addressId;
        $res['status'] = TRUE;

        return $res;
    }

    static function customerDeleteAddressFx($obj)
    {

        $addressId = $obj->address_id;

        $sql = "DELETE FROM customer_address WHERE id = $addressId";
        SelwynDatabase::query($sql, 'update');

        $res['status'] = TRUE;
        return $res;
    }

    static function customerAddressFx($id)
    {

        $addressId = $id;

        $sql = "SELECT * FROM customer_address WHERE id = $addressId";
        $res['data'] = SelwynDatabase::query($sql);

        $res['status'] = TRUE;
        return $res;
    }
}


