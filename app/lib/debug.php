<?php
/**
 * Created by PhpStorm.
 * User: CPU011
 * Date: 4/30/2018
 * Time: 3:38 PM
 */
class Debug extends SelwynDebugger
{
    static $inst;
    static function inst()
    {
        if(self::$inst){
            return self::$inst;
        }else{
            self::$inst = new self();
            return self::$inst;
        }
    }

    static function show($string)
    {
        $classString = self::inst()->getClassAndMethodName(debug_backtrace());
        //$newString = "[".$classString."] [".date('H:i:s')."] ".$string."<br>";
        $newString = "[".$classString."] ".$string."<br>";
        Response::inst()->debugBuffer($newString);
    }

    private function getClassAndMethodName($bcktrace)
    {
        $clsStr = $bcktrace[1]['class']."/".$bcktrace[1]['function'];
        return $clsStr;
    }
}