<?php
/**
 * Created by PhpStorm.
 * User: CPU011
 * Date: 4/23/2018
 * Time: 10:49 PM
 */

class FrontendController extends SelwynController
{
    function __construct()
    {
        parent::__construct();
        Response::inst();
        /*Check Session*/
//        $user = Session::inst()->getData('user');
//        if(!$user){
//              Response::redirect('/');
//        }
        
        Response::setTheme('frontend');
        Response::setHeader('header');
        Response::setFooter('footer');
    }

    function loadPage($page,$data=null)
    {


        Response::loadHeader();
        //Response::loadHTML($page,$data);
        //Response::loadFooter();
        Response::sendHtml();

    }
}