<?php
/**
 * Created by PhpStorm.
 * User: CPU011
 * Date: 4/23/2018
 * Time: 10:49 PM
 */

class BackendController extends SelwynController
{
    protected $active = '';
    function __construct()
    {
        parent::__construct();
        
        
        Response::inst();
        /*Check Session*/
        $user = Session::inst()->getData('user');
        if (!Session::inst()->getIsLogin()) {
            //Response::redirect('/');
        }

        Response::setTheme('admin');
        Response::setHeader('header');
        Response::setFooter('footer');
    }
    
    function loadPage($page,$data=null)
    {
        
        $d['active'] = $this->active;
        if (Session::inst()->getIsLogin()) {
            $user = Session::inst()->getData('ADMIN');
            $input = (object) array('customer_id' => $user[0]->id);

            $chats['tickets'] = supportModel::supportListFx($input);
            $chats['chat_user'] = array("Name" => $user[0]->first_name." ".$user[0]->last_name , "uid" => $user[0]->id);
            Response::bindData($chats);
        }

        Response::bindData($data);
        Response::loadHeader();
        Response::loadHTML($page);
        Response::loadFooter();
        Response::sendHtml();
        
    }
    
}