<?php

$html = 'ccua-image.php';
$data = file_get_contents($html);

$path = (getenv('MPDF_ROOT')) ? getenv('MPDF_ROOT') : __DIR__;
require_once $path . '/vendor/autoload.php';

$mpdf = new \Mpdf\Mpdf(['mode' => 'c']);

$mpdf->WriteHTML($data);
$mpdf->Output();
