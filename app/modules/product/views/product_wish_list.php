<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space" style="padding-top: 20px;padding-bottom: 20px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>Favourite</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo SITE_DOMAIN. str::HOME_ROUTE ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Favourite</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->


<!--section start-->
<section class="wishlist-section section-b-space" style="padding-top: 0px;padding-bottom: 0;min-height: calc(100vh - 200px)">
    <div class="container" style="padding-top: 30px;padding-bottom: 30px;background-color: #fff !important;min-height: calc(100vh - 200px)">
        <div class="row">
            <div class="col-sm-12">
                <table class="table cart-table table-responsive-xs">
                    <thead>
                    <tr class="table-head">
                        <th scope="col">image</th>
                        <th scope="col">product name</th>
                        <th scope="col">price</th>
                        <th scope="col">availability</th>
                        <th scope="col">action</th>
                    </tr>
                    </thead>
                  <?php foreach ($productbycat['data']['product_list'] as $flist) {  ?>
                    <tbody id="favtbody_<?php echo $flist->id; ?>" class="tbodycount">
                    <tr>
                        <td>
                            <a href="#"><img src="<?php echo $flist->url; ?>" alt=""></a>
                        </td>
                        <td><a href="#"><?php echo $flist->name; ?></a>
                            <div class="mobile-cart-content row">
                                <div class="col-xs-3">
                                    <p><?php echo ($flist->is_online == 1)?"in stock":""; ?></p>
                                </div>
                                <div class="col-xs-3">
                                    <h2 class="td-color">&#8377;<?php echo $flist->selling_price; ?></h2></div>
                                <div class="col-xs-3">
                                    <h2 class="td-color"><a href="#" class="icon mr-1"  onclick="Favouritedel(<?php echo $flist->id; ?>,<?php  if(isset($chat_user)) { echo $chat_user['uid']; }; ?>)"><i class="ti-close"></i> </a><a href="/product_detail?id=<?php echo $flist->id; ?>" class="cart" ><i class="ti-shopping-cart"></i></a></h2></div>
                            </div>
                        </td>
                        <td>
                            <h2>&#8377;<?php echo $flist->selling_price; ?></h2></td>
                        <td>

                            <p><?php echo ($flist->is_online == 1)?"in stock":""; ?></p>
                        </td>
                        <td><a href="#" class="icon mr-3" onclick="Favouritedel(<?php echo $flist->id; ?>,<?php  if(isset($chat_user)) { echo $chat_user['uid']; }; ?>)"><i class="ti-close"></i> </a><a href="/product_detail?id=<?php echo $flist->id; ?>" class="cart" ><i class="ti-shopping-cart"></i></a>

                        </td>
                    </tr>
                    </tbody>
                    <?php } ?>
                    <tbody class="wishlistshowmsg" >
                        <tr>
                            <td colspan="5">No Item In Your Wishlist</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>



    </div>
</section>
<!--section end-->
<script>

</script>