 <!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space" style="padding-bottom: 20px;padding-top: 20px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>search</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo SITE_DOMAIN. str::HOME_ROUTE ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">search</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->


<!--section start-->
<section class="authentication-page" style="padding-top: 0px">
    <div class="container">
        <section class="search-block">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <form class="form-header" action="/product_search">
                            <div class="input-group">
                                <input type="text" class="form-control" value="<?php echo isset($_GET['search'])?$_GET['search']:''; ?>" name="search" aria-label="Amount (to the nearest dollar)" placeholder="Search Products......">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-solid"><i class="fa fa-search"></i>Search</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
</section>
<!-- section end -->

<!-- section start -->
<section class="section-b-space ratio_square" style="padding-top: 0px;padding-bottom: 0">
    <div class="collection-wrapper">
        <div class="container"  style="padding-bottom: 30px;">
            <div class="row">
                <div class="collection-content col">
                    <div class="page-main-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="collection-product-wrapper">

                                    <div class="product-wrapper-grid">
                                        <div class="row">
                                            <?php foreach ($detail as $key=>$val) { ?>
                                            <div class="col-lg-2 col-md-4 col-6 col-grid-box">
                                                <div class="product-box">
                                                    <div class="img-block">
                                                        <a href="<?php echo SITE_DOMAIN. str::PRODUCT_DETAIL_ROUTE ?>?id=<?php echo $val->id; ?>">
                                                            <img src="<?php echo $val->url; ?>" class=" img-fluid bg-img" alt=""></a>
<!--                                                        <div class="cart-details">-->
<!--                                                            <button tabindex="0" class="addcart-box" title="Quick shop"><i class="ti-shopping-cart" ></i></button>-->
<!--                                                            <a href="javascript:void(0)" title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></a>-->
<!--                                                            <a href="#" data-toggle="modal" data-target="#quick-view"  title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>-->
<!--                                                            <a href="compare.html"  title="Compare"><i class="ti-reload" aria-hidden="true"></i></a>-->
<!--                                                        </div>-->
                                                    </div>
                                                    <div class="product-info">
                                                        <div>
                                                            <a href="<?php echo SITE_DOMAIN. str::PRODUCT_DETAIL_ROUTE ?>?id=<?php echo $val->id; ?>"><h6><?php echo $val->name; ?></h6>

                                                            <h5>&#8377;<?php echo $val->selling_price; ?></h5>
                                                            </a>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section End -->
