<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space" style="padding-top: 20px; padding-bottom: 20px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>PRODUCT</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo SITE_DOMAIN. str::HOME_ROUTE ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">PRODUCT DETAIL</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->


<!-- section start -->
<section style="padding-top: 0">
    <div class="collection-wrapper">
        <div class="container" style="padding-top: 30px; background-color: white !important;">
            <div class="row" style="background-color: white">
                <div class="col-lg-1 col-sm-2 col-xs-12">
                    <div class="row">
                        <div class="col-12 p-0">
                            <div class="slider-right-nav">
							 <?php foreach ($detail['data']['product_images'] as $key => $val) { ?>
							 <div><img src="<?php echo $val->url; ?>" alt="" class="img-fluid " onclick="focusimg('<?php echo $val->url; ?>');"></div>
							 <?php } ?>
                              <!--  <div><img src="../assets/images/product/1.jpg" alt="" class="img-fluid "></div>
                                <div><img src="../assets/images/product/2.jpg" alt="" class="img-fluid "></div>
                                <div><img src="../assets/images/product/27.jpg" alt="" class="img-fluid "></div>
                                <div><img src="../assets/images/product/27.jpg" alt="" class="img-fluid "></div>-->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-5 col-sm-10 col-xs-12 order-up">
                    <div class="">

                        <div><img src="<?php echo $detail['data']['product_images'][0]->url; ?>" alt="" id="main_img"
                                  class="img-fluid image_zoom_cls-0"></div>
                    </div>
                </div>
                <div class="col-lg-6 rtl-text">
                    <div class="product-right">
                        <h2><?php echo $detail['data']['product_detail'][0]->name; ?></h2>
                        <div class="row">
                            <div class="col-sm-2">
                                <div class="product-icon">
                                    <div class="d-inline-block">
                                        <button id="faviriotid_<?php echo $detail['data']['product_detail'][0]->id; ?>" class="wishlist-btn <?php echo ($detail['data']['product_detail'][0]->is_fav == "true")?"active":""; ?>" onclick="Favouriteadd(<?php echo $detail['data']['product_detail'][0]->id; ?>,<?php  if(isset($chat_user)) { echo $chat_user['uid']; }; ?>)"><i class="fa fa-heart"></i>
                                        </button>
                                        <input type="hidden" id="favsts_<?php echo $detail['data']['product_detail'][0]->id; ?>" value="<?php echo $detail['data']['product_detail'][0]->is_fav; ?>">
                                    </div>
                                </div>
                            </div>
								
								<?php if($detail['data']['product_detail'][0]->category_id=='91'){ ?>
									 <div class="col-sm-10" ><h3><span style="text-decoration: line-through;color:red"> &#8377;<?php echo $detail['data']['product_detail'][0]->mrp; ?></span> &#8377;<?php echo $detail['data']['product_detail'][0]->selling_price; ?></h3></div>
								<?php }else {?>
									
									<div class="col-sm-10" ><h3>&#8377;<?php echo $detail['data']['product_detail'][0]->selling_price; ?></h3></div>

									<?php } ?>
								
							   
                            <input type="hidden" id="sprice" value="<?php echo $detail['data']['product_detail'][0]->selling_price; ?>">
                            <input type="hidden" id="sid" value="<?php echo $detail['data']['product_detail'][0]->id; ?>">
                        </div>



                        <div class="product-description border-product">
                            <h6 class="product-title">quantity</h6>
                            <div class="qty-box">
                                <div class="input-group">
                                    <span onclick="changeProductQty()" class="input-group-prepend"><button type="button"
                                                                              class="btn quantity-left-minus"
                                                                              data-type="minus" data-field=""><i
                                                    class="ti-angle-left"></i></button></span>
                                    <input type="hidden" id="qtyCounter" onchange="changeProductQty()"
                                           class="form-control input-number"
                                           value="1">
                                    <input type="number" id="showQty" style="background: white; width: 100px;font-weight: bold"
                                           readonly name="quantity" class="form-control "
                                           value="<?php echo $detail['data']['product_detail'][0]->pacaket ?>">
                                    <input type="hidden" id="defaultPacaketSize"
                                           value="<?php echo $detail['data']['product_detail'][0]->pacaket ?>">
                                    <input type="text" name="unit" class="form-control"
                                           value="<?php echo $detail['data']['product_detail'][0]->unit ?>">
                                    <span class="input-group-prepend" onclick="changeProductQty()">
                                        <button type="button" class="btn quantity-right-plus" data-type="plus" data-field="">
                                            <i class="ti-angle-right"></i></button>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="product-buttons">
                            <a href="#" data-toggle="modal" data-target="#addtocart" onclick="addtocart(<?php echo $detail['data']['product_detail'][0]->id; ?>,<?php  if(isset($chat_user)) { echo $chat_user['uid']; }; ?>)" class="btn btn-solid">add to cart</a>
                          <!--  <button onclick="orderdirect()"  class="btn btn-solid">buy now</button>-->
							</div>
                        <div class="border-product" id="productSpecification">
                            <h6 class="product-title">product details</h6>
                            <p><?php echo $detail['data']['product_specification'][0]->text; ?></p>
                        </div>
<!--                        <div class="border-product">-->
<!--                            <div class="product-icon">-->
<!--                                <form class="d-inline-block">-->
<!--                                    <button class="wishlist-btn"><i class="fa fa-heart"></i><span class="title-font">Add To WishList</span>-->
<!--                                    </button>-->
<!--                                </form>-->
<!--                            </div>-->
<!--                        </div>-->
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Section ends -->

<!-- product section start -->
<section class="section-b-space ratio_square product-related" style="padding-top: 0;padding-bottom: 0;min-height: calc(100vh - 200px)">
    <div class="container" style="padding-top: 20px;background-color: white !important;min-height: calc(100vh - 200px)">
        <div class="row">
            <div class="col-12 product-related">
                <h2 class="title pt-0">related products</h2></div>
        </div>
        <div class="slide-6">
            <?php foreach ($detail['data']['related_products'] as $key => $val) { ?>
                <div class="">
                    <div class="product-box">
                        <div class="img-block">
                            <a href="<?php echo SITE_DOMAIN . str::PRODUCT_DETAIL_ROUTE ?>?id=<?php echo $val->id; ?>">
                                <img src="<?php echo $val->url ?>" class=" img-fluid bg-img" alt=""></a>

                        </div>
                        <div class="product-info">
                            <a href="<?php echo SITE_DOMAIN . str::PRODUCT_DETAIL_ROUTE ?>?id=<?php echo $val->id; ?>">
                                <h6><?php echo $val->name; ?></h6>
								<?php if($detail['data']['product_detail'][0]->category_id=='91'){ ?>
									<h5><span style="text-decoration: line-through;color:red"> &#8377;<?php echo $val->mrp; ?></span> &#8377;<?php echo $val->selling_price; ?></h5>
								<?php }else {?>
									<h5>&#8377;<?php echo $val->mrp; ?></h5>
								<?php }?>
                            </a>
                        </div>
                        <div class="addtocart_box">
                            <div class="addtocart_detail">
                                <div>
                                    <div class="addtocart_btn">
                                        <a href="javascript:void(0)" data-toggle="modal" class="closeCartbox"
                                           data-target="#addtocart" tabindex="0">add to cart</a>
                                    </div>
                                </div>
                            </div>
                            <div class="close-cart">
                                <i class="fa fa-times" aria-hidden="true"></i>
                            </div>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>
<!-- product section end -->
<form action="/checkout" method="get" id="directorder" class="d-none">
    <input type="text" name="direct" value="yes">
    <input type="text" name="pid" id="opid">
    <input type="text" name="pqty" id="opqty">
    <input type="text" name="psprice" id="opsprice">
    <button type="button" id="submitBtn">Submit Form</button>
</form>
<Script>
    function changeProductQty() {
        var counter = $('#qtyCounter').val();
        var pacaketSize = $('#defaultPacaketSize').val();
        var newQty = counter * pacaketSize;
        $('#showQty').val(newQty);

    }

    function orderdirect() {
        var qty =  $('#showQty').val();
        var price = $('#sprice').val();
        var sid = $('#sid').val();

        $("#opid").val(sid);
        $("#opqty").val(qty);
        $("#opsprice").val(price);

        $("#directorder").submit();

    }
	function focusimg(val)
	{
	
		document.getElementById("main_img").src = val;
		
	}


</Script>