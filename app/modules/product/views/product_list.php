<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space" style="padding-top: 20px;padding-bottom: 20px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>COLLECTION</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo SITE_DOMAIN. str::HOME_ROUTE ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page"><?php echo $_GET['name'];?></li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->


<!-- section start -->
<section class="section-b-space ratio_square" style="padding-top: 0;padding-bottom: 0;min-height: calc(100vh - 200px);">
    <div class="collection-wrapper">
        <div class="container" style="padding-bottom: 20px;min-height: calc(100vh - 200px);">
            <div class="row">
                <div class="collection-content col">
                    <div class="page-main-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="collection-product-wrapper">
                                    <div class="product-wrapper-grid">
                                        <div class="row">
                                            <?php foreach ($productbycat['data']['product_list'] as $key => $val) { ?>
                                                <div class="col-xl-3 col-6 col-grid-box">
                                                    <div class="product-box">
																	 <div class="float123"  data-toggle="modal" data-target="#addtocart" onclick="addtocartall(<?php echo $val->id; ?>,<?php  if(isset($chat_user)) { echo $chat_user['uid']; }; ?>)">
									
									<i class="fa fa-plus my-float"  ></i>
									
                           
								</div>
                                                        <div class="img-block">
                                                            <a href="<?php echo SITE_DOMAIN. str::PRODUCT_DETAIL_ROUTE ?>?id=<?php echo $val->id; ?>">
                                                                <img src="<?php echo $val->url; ?>" class=" img-fluid bg-img" alt=""></a>
                                                            <!--<div class="cart-details">
                                                                <button tabindex="0" class="addcart-box" title="Quick shop"><i class="ti-shopping-cart" ></i></button>
                                                                <a href="javascript:void(0)" title="Add to Wishlist"><i class="ti-heart" aria-hidden="true"></i></a>
                                                                <a href="#" data-toggle="modal" data-target="#quick-view"  title="Quick View"><i class="ti-search" aria-hidden="true"></i></a>
                                                                <a href="compare.html"  title="Compare"><i class="ti-reload" aria-hidden="true"></i></a>
                                                            </div>-->
                                                        </div>
                                                        <div class="product-info">
                                                            <div>
                                                                <a href="<?php echo SITE_DOMAIN. str::PRODUCT_DETAIL_ROUTE ?>?id=<?php echo $val->id; ?>">
                                                                    <h6><?php echo $val->name; ?></h6>
																		<?php if($val->category_id=='91'){ ?>
																			<h5><span style="text-decoration: line-through;color:red"> &#8377;<?php echo $val->mrp; ?></span>  &#8377;<?php echo $val->selling_price ?> <span style="background-color:green;color:#fff;border-radius:10px;font-size:10px;padding:2px;position:relative;top:-8px">offer price</span></h5>
																		<?php }else {?>

																			<h5>&#8377;<?php echo $val->selling_price ?></h5>

																		<?php } ?>
                                                                       <input type="number" id="showQty_<?php echo $val->id; ?>" 
																			 style="display:none;background: white; width: 100px;font-weight: bold"
                                           readonly name="quantity"  type="hidden"
                                           value="<?php echo $val->pacaket ?>">
                                                                </a>
                                                            </div>
                                                        </div>
                                                        <div class="addtocart_box">
                                                            <div class="addtocart_detail">
                                                                <div>
                                                                    <div class="color">
                                                                        <h5>color</h5>
                                                                        <ul class="color-variant">
                                                                            <li class="light-purple active"></li>
                                                                            <li class="theme-blue"></li>
                                                                            <li class="theme-color"></li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="size">
                                                                        <h5>size</h5>
                                                                        <ul class="size-box">
                                                                            <li class="active">xs</li>
                                                                            <li>s</li>
                                                                            <li>m</li>
                                                                            <li>l</li>
                                                                            <li>xl</li>
                                                                        </ul>
                                                                    </div>
                                                                    <div class="addtocart_btn">
                                                                        <a href="javascript:void(0)"  data-toggle="modal" class="closeCartbox" data-target="#addtocart" tabindex="0">add to cart</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="close-cart">
                                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <!--                                    <div class="product-pagination mb-0">-->
                                    <!--                                        <div class="theme-paggination-block">-->
                                    <!--                                            <div class="container-fluid p-0">-->
                                    <!--                                                <div class="row">-->
                                    <!--                                                    <div class="col-xl-6 col-md-6 col-sm-12">-->
                                    <!--                                                        <nav aria-label="Page navigation">-->
                                    <!--                                                            <ul class="pagination">-->
                                    <!--                                                                <li class="page-item"><a class="page-link" href="#" aria-label="Previous"><span aria-hidden="true"><i class="fa fa-chevron-left" aria-hidden="true"></i></span> <span class="sr-only">Previous</span></a></li>-->
                                    <!--                                                                <li class="page-item active"><a class="page-link" href="#">1</a></li>-->
                                    <!--                                                                <li class="page-item"><a class="page-link" href="#">2</a></li>-->
                                    <!--                                                                <li class="page-item"><a class="page-link" href="#">3</a></li>-->
                                    <!--                                                                <li class="page-item"><a class="page-link" href="#" aria-label="Next"><span aria-hidden="true"><i class="fa fa-chevron-right" aria-hidden="true"></i></span> <span class="sr-only">Next</span></a></li>-->
                                    <!--                                                            </ul>-->
                                    <!--                                                        </nav>-->
                                    <!--                                                    </div>-->
                                    <!--                                                    <div class="col-xl-6 col-md-6 col-sm-12">-->
                                    <!--                                                        <div class="product-search-count-bottom">-->
                                    <!--                                                            <h5>Showing Products 1-24 of 10 Result</h5></div>-->
                                    <!--                                                    </div>-->
                                    <!--                                                </div>-->
                                    <!--                                            </div>-->
                                    <!--                                        </div>-->
                                    <!--                                    </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- section End -->