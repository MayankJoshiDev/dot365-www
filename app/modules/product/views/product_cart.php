<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space" style="padding-bottom: 20px;padding-top: 20px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>cart</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo SITE_DOMAIN . str::HOME_ROUTE ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Cart</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->


<!--section start-->
<section class="cart-section1 section-b-space" style="padding-top: 0px;padding-bottom: 0px;min-height: calc(100vh - 200px);">
    <div class="container" style="overflow:scroll;padding-top: 50px;padding-bottom: 50px;background-color: #fff !important;min-height: calc(100vh - 200px);">
        <div class="row">
            <?php if (!empty($data['data']['kart'])) { ?>
                <div class="col-sm-12" >

                    <table class="table cart-table table-responsive-xs striped-table">
                        <thead style="text-align:center;">
                            <tr class="table-head">
                                <th scope="col">IMAGE</th>
                                <th scope="col">PRODUCT NAME</th>
                                <th scope="col">PRICE</th>
                                <th scope="col">QUANTITY</th>
                                <th scope="col">ACTION</th>
                                <th scope="col">TOTAL</th>
                            </tr>
                        </thead>
                        <?php foreach ($data['data']['kart'] as $cdata) { ?>
                            <?php $Total[] = $cdata->selling_price * $cdata->qty; ?>
                            <input type="hidden" class="cart_id_<?php echo $cdata->product_id; ?>" value="<?php echo $cdata->id; ?>" >
                            <tbody>
                                <tr style="background-color:#f8f8f8;">
                                    <td style="min-width: 10px;vertical-align:middle;text-align:center;" >
                                        <a href="#"><img src="<?php echo $cdata->url; ?>" alt="" style="height:90px"></a>
                                    </td>
                                    <td style="min-width: unset !important;width: 200px;vertical-align:middle;text-align:center;"><a href="#" style="color:#777777;text-decoration:none"><?php echo $cdata->name; ?></a>

                                    </td>
                                    <td style="min-width: 20px;vertical-align:middle">
									
									      
                                        <h2 style="text-decoration:none;font-weight:400;font-size:24px">&#8377;<?php echo $cdata->selling_price; ?></h2>
                                    </td>
                                    <td style="min-width: 43px;vertical-align:middle">
                                        <div class="qty-box">
                                            <div class="input-group">
                                                <!--                                    <input type="number" name="quantity" class="form-control input-number" value="--><?php //echo $cdata->qty;
                                                                                                                                                                            ?>
                                                <!--">-->
                                                <span class="input-group-prepend">
                                                    <button type="button" class="btn btn-counter quantity-left-minus_<?php echo $cdata->product_id ?>"  data-type="minus" data-btnname="down_count" data-pid="<?php echo $cdata->product_id ?>">
                                                        <i class="ti-angle-left"></i></button>
                                                </span>
                                                <input type="hidden" id="qtyCounter_<?php echo $cdata->product_id ?>" class="form-control input-number_<?php echo $cdata->product_id ?>" value="1">
                                                <input type="number" id="showQty_<?php echo $cdata->product_id ?>" style="background: white; width: 75px;font-weight: bold;padding: 0" readonly name="quantity" class="qty-display form-control " data-packet="<?php echo $cdata->pacaket ?>" value="<?php echo $cdata->qty ?>" onchange="changeProductQty(<?php echo $cdata->product_id ?>)">
                                                <input type="hidden" id="price_<?php echo $cdata->product_id ?>" value="<?php echo $cdata->selling_price; ?>">
                                                <input type="hidden" id="defaultPacaketSize_<?php echo $cdata->product_id ?>" value="<?php echo $cdata->pacaket ?>">
                                                <input type="text" name="unit" class="form-control" value="<?php echo $cdata->unit ?>">
                                                <span class="input-group-prepend">
                                                    <button type="button" class="btn btn-counter quantity-right-plus_<?php echo $cdata->product_id ?>"  data-type="plus" data-btnname="up_count" data-pid="<?php echo $cdata->product_id ?>">
                                                        <i class="ti-angle-right"></i>
                                                    </button>
                                                </span>

                                            </div>
                                        </div>
                                    </td>
                                    <td style="min-width: 20px;vertical-align:middle;text-align:center;"><a href="#" class="icon" onclick="deletecart(<?php echo $cdata->id; ?>,<?php echo $cdata->product_id; ?>)"><i class="ti-close" style="color:#777777"></i></a></td>
                                    <td style="min-width: 30px;vertical-align:middle;text-align:center;">
                                   <h2 class="td-color subtotalprice" id="customprice_<?php echo $cdata->product_id ?>" style="font-weight:400;font-size: 24px;color:#d41d36">&#8377;<?php echo $cdata->selling_price * $cdata->qty; ?></h2>
                                  
                                  
								   </td>
                                </tr>
                            </tbody>
                        <?php } ?>
						   <tfoot>
                            <tr> 
                                <td></td> <td></td> <td></td> <td></td> <td>Total price :</td>
                                <td>
                                    <h2 class="totalsums">&#8377;<?php echo array_sum($Total); ?></h2>
									
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                
                </div>
            <?php } else { ?>
                <div class="col-sm-12 text-center">
                    <h2><b>Your Cart is Empty</b></h2>
                    <a href="<?php echo SITE_DOMAIN . str::HOME_ROUTE ?>" class="btn btn-solid mt-5">Go For Buy</a>
                </div>

            <?php } ?>
        </div>
	
        <?php if (!empty($data['data']['kart'])) { ?>
            <div class="row cart-buttons">
                <div class="col-6"><a href="<?php echo SITE_DOMAIN . str::HOME_ROUTE ?>" class="btn btn-solid">continue shopping</a></div>

                <div class="col-6" ><a style="float:right" href="<?php echo SITE_DOMAIN . str::CHECKOUT_ROUTE ?>" class="btn btn-solid">check out</a></div>

            </div>
        <?php } ?>
    </div>
</section>
<script src="<?php echo THEME_URL ?>/js/jquery-3.3.1.min.js"></script>
<script type="text/javascript">
    var pid = 0;
    var $value = $('#showQty_' + pid).val();
    var packetVal = $('#showQty_' + pid).data('packet');
    $('#customprice_' + pid).html($value * packetVal);
    $('.btn-counter').on('click', function(e) {
        pid = $(this).data('pid');
        var $value = $('#showQty_' + pid);
        var currentVal = parseInt($value.val());
        var packetVal = $('#showQty_' + pid).data('packet');
        console.log('packetVal' + packetVal)
        var increment = currentVal / packetVal;
        var button_name = $(this).data('btnname');
        var price = $('#price_' + pid).val();
        var cart_id = $(".cart_id_"+pid).val();
        var sum = 0;
        if (button_name !== "down_count") {
            increment += 1;
            $value.val(increment * packetVal);
            updateitem(pid,$value.val(),cart_id);
            var totalPrice = ($value.val()) * price
            $('#customprice_' + pid).html("&#8377;"+totalPrice);
            $(".subtotalprice").each(function() {
               
                sum += +$(this).text().replace('\u20b9','');
            });
            $(".totalsums").html("&#8377;"+sum);
        } else {
            if (increment <= 1) {
                return false;
            }
            increment -= 1;
            $value.val(increment * packetVal);
            var totalPrice = ($value.val()) * price;
            updateitem(pid,$value.val(),cart_id);
            $('#customprice_' + pid).html("&#8377;"+totalPrice);
            $(".subtotalprice").each(function() {
                sum += +$(this).text().replace('\u20b9','');
            });
            $(".totalsums").html("&#8377;"+sum);
        }


    });

    //changeProductQty(pid);
    function changeProductQty(id) {
        var counter = $('#qtyCounter_' + id).val();
        var price = $('#price_' + id).val();
        var pacaketSize = $('#showQty_' + pid).data('packet');
        /*var pacaketSize = $('#defaultPacaketSize_'+id).val();*/
        console.log('Packet Size' + pacaketSize * price);
        var newQty = counter * pacaketSize;
        console.log('newQty' + newQty);
        $('#showQty_' + id).val(newQty);
        var price = $('#price_' + id).val();
        console.log('Price' + price);
        var total_new = price * newQty;
        console.log('total_new' + total_new);
        $('#customprice_' + id).html(total_new);

    }

    
    function updateitem(id, pqty,cid) {
      
        

        $.ajax({
            type: "POST",
            url: "/add_to_cart",           
            data: {
                product_id: id,
                qty: pqty,
                action: 'edit',
                cart_id: cid
            },
            success: function(response) {
             
                   return true;
                    
               
            }
        });
   
    }

    function deletecart(id, pid) {
        $.ajax({
            url: "/delete_customer_cart_item",
            type: "POST", //send it through get method
            data: {
                cart_id: id,


            },
            success: function(response) {

                const array = JSON.parse(localStorage.getItem('cartproduct')) || [];
                const index = array.indexOf(pid);
                if (index > -1) {
                    array.splice(index, 1);
                }
                // Alert the array value
                localStorage.setItem('cartproduct', JSON.stringify(array));

                total = JSON.parse(localStorage.getItem('cartproduct')) || [];

                $(".cartitems").html(total.length);

                toastFunction("Item Removed From Cart");
                location.reload();
            },
            error: function(xhr) {
                //Do Something to handle error
                toastFunction("Something Wrong Try Again!!");
            }
        });
    }

    function removeFrmArr(array, element) {
        return array.filter(e => e !== element);
    };
</script>
<!--section end-->