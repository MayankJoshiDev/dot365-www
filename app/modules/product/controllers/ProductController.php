<?php


class ProductController extends BackendController
{
    static $inst;
    static function inst()
    {
        if(!self::$inst) { self::$inst = new self();}
        return self::$inst;
    }

    function index()
    {

        $this->loadPage('product/sub_category_list');
    }

    function productList()
    {

        $user = Session::inst()->getData('ADMIN');
        $tmpobj = (object) array("category_id" => $_GET['id'],"order_by"=>1,"favourite"=>'NO',"customer_id"=>$user[0]->id);
        $data['productbycat'] = ProductModel::productListFx($tmpobj);

        $this->loadPage('product/product_list',$data);



    }

    function productDetail()
    {
        $user = Session::inst()->getData('ADMIN');
        $array = (object) array("product_id" => $_GET['id'],'customer_id' => $user[0]->id );
        $product['detail'] = ProductModel::productDetailFx($array);

         $this->loadPage('product/product_detail',$product);

    }

    function productSearch()
    {
        if(isset($_GET['search'])){
            $product['detail'] = ProductModel::productSearchFx($_GET['search']);
        }
        else
        {

            $product['detail'] = ProductModel::productSearchFx($search = null);
        }

        $this->loadPage('product/product_search' , $product);

    }

    function productCart()
    {
        if (Session::inst()->getIsLogin()) {
            $user = Session::inst()->getData('ADMIN');
            $uid =  $user[0]->id;
            $cart['data'] = cartModel::viewCustomerCartFx($uid);

            $this->loadPage('product/product_cart', $cart);
        }
        else
        {
            Response::redirect('/login');
        }


    }

    function productWishList()
    {
        if (Session::inst()->getIsLogin()) {
            $user = Session::inst()->getData('ADMIN');
            $uid = $user[0]->id;
            $tmpobj = (object)array("category_id" => '', "order_by" => 1, "favourite" => 'yes',"customer_id" => $uid);
            $data['productbycat'] = ProductModel::productListFx($tmpobj);

            $this->loadPage('product/product_wish_list',$data);
        }
        else
        {
            Response::redirect('/login');
        }



    }


   
}