<?php


class UserController extends BackendController
{
    static $inst;
    
    static function inst()
    {
        if (!self::$inst) {
            self::$inst = new self();
        }
        return self::$inst;
    }
    
    function profile()
    {
        if (!Session::inst()->getIsLogin()) {
            
            $this->loadPage('user/login');
        } else {
            $user = Session::inst()->getData('ADMIN');
            $input = (object)array('customer_id' => $user[0]->id);
            $r = customerModel::customerProfileModelFx($input);
            $this->loadPage('user/profile', $r);
        }
        
        
    }
    
  
    
    function customerProfile()
    {
        $input = Req::formData();
        $customer = new CustomerTbl();
        
        $input = (object)$_REQUEST;
        
        
        $action = $input->action;
        
        if ($action == 'edit') {
            customerModel::editCustomerProfile($input);
            $res = customerModel::customerProfileModelFx($input);
            
        } else if ($action == 'upload') {
            $image = $_FILES['profile_image'];
            customerModel::customerProfileUpload($input, $image);
            $res = customerModel::customerProfileModelFx($input);
            
        } else {
            $res = customerModel::customerProfileModelFx($input);
            
        }
        
        
        Response::redirect('/profile');
//       Response::sendJson($res);
    
    }
    
    function changePassword()
    {
        
        $this->loadPage('user/change_password');
        
    }
    
    function addNewAddress()
    {
        if(isset($_GET['id']))
        {
            $res['state'] = stateModel::stateListFx();
            $res['data'] = customerModel::customerAddressFx($_GET['id']);

            $this->loadPage('user/new_address',$res);
        }
        else
        {
            $res['state'] = stateModel::stateListFx();

            $this->loadPage('user/new_address',$res);
        }
    }

    function addressAction() {

        $input = Req::formData();
        $customer = new CustomerTbl();

        $action = $input->action;


        if ($action == 'add') {
            customerModel::customerAddressModelFx($input);

        } else if ($action == 'edit') {
            customerModel::customerAddressModelFx($input);


        }


        if($input->getback == "true")
        {
            Response::redirect('/checkout');
        }
        else
        {
            Response::redirect('/address_list');
        }


    }

    function addressDelete()
    {

        $input = Req::json();
        $customer = new CustomerTbl();
        $res = customerModel::customerDeleteAddressFx($input);

        Response::sendJson($res);
    }
    
    function addressList()
    {

        if (!Session::inst()->getIsLogin()) {

            $this->loadPage('user/login');

        } else {
            $user = Session::inst()->getData('ADMIN');
            $input = (object)array('customer_id' => $user[0]->id);
            $res = customerModel::customerProfileModelFx($input);

            $this->loadPage('user/address_list',$res);
        }

        
    }
    
    function doLogout()
    {
        Session::inst()->logout();
        Response::redirect('/home');
    }
    
    
}

class LoginController extends SelwynController
{
    static $inst;
    
    static function inst()
    {
        if (!self::$inst) {
            self::$inst = new self();
        }
        return self::$inst;
    }

    function termandconditions()
    {
        Response::setTheme('admin');
        Response::loadHTML('user/term&condition');
        //Response::loadFooter();
        Response::sendHtml();
    }
    
    function login()
    {
        if (!Session::inst()->getIsLogin()) {
            
            //$this->loadPage('user/login');
            //Response::bindData($data);
            //Response::loadHeader();
            Response::setTheme('admin');
            Response::loadHTML('user/login');
            //Response::loadFooter();
            Response::sendHtml();
        } else {
            Response::redirect('/home');
        }
    }
    
    function signUp()
    {

//        Response::bindData($res);
        //Response::loadHeader();
        Response::setTheme('admin');
        Response::loadHTML('user/sign_up');
        //Response::loadFooter();
        Response::sendHtml();
        
//        $this->loadPage('user/sign_up');
    }
    
    function create_account()
    {

        $input = Req::formData();
        $res = authModel::signupModelFx($input);


        if ($res['status'] != FALSE) {

            Response::bindData($res);
            //Response::loadHeader();
            Response::setTheme('admin');
            Response::loadHTML('user/verify_otp');
            //Response::loadFooter();
            Response::sendHtml();
            
//            $this->loadPage('user/verify_otp', $res);
        } else {

            Response::bindData($res);
            //Response::loadHeader();
            Response::setTheme('admin');
            Response::loadHTML('user/sign_up');
            //Response::loadFooter();
            Response::sendHtml();
//            $this->loadPage('user/sign_up', $res);
        }
    }
    
    function forgotPassword()
    {
        
        $this->loadPage('user/forgot_password');
    }
    
    function verifyOTP()
    {
        
        $input = Req::formData();
        $mobileNumber = $input->inputMobileNumber;
        $dummy = New CustomerTbl();
        $c = CustomerTbl::findOneBy(array(CustomerTbl::DF_MOBILE_NUMBER => $mobileNumber));
        /*Send OTP*/
        if ($c) {
            $r['data']['mobilenumber'] = $mobileNumber;
            $r['data']['is_registered'] = TRUE;
            $otp = OTP::generate();
            //SMS::send($mobileNumber, $otp . " is your OTP for Dot365 Application");
            SMS::sendOTP($mobileNumber, $otp );
            $r['status'] = TRUE;
            $otpObj = new RegisterOtpTbl();
            $otpObj->setMobileNumber($mobileNumber);
            $otpObj->setOtp($otp);
            $otpObj->setCreatedAt('NOW()');
            $otpObj->flush();
            
           // $this->loadPage('user/verify_otp', $r);
    
            Response::bindData($r);
            //Response::loadHeader();
            Response::setTheme('admin');
            Response::loadHTML('user/verify_otp');
            //Response::loadFooter();
            Response::sendHtml();
        } else {

            $r['data']['is_registered'] = FALSE;
            $r['data']['mobilenumber'] = $mobileNumber;
            
            if(strlen($mobileNumber)!= 10){
                $r['message'] = "Please enter valid mobile number";
                Response::bindData($r);
                //Response::loadHeader();
                Response::setTheme('admin');
                Response::loadHTML('user/login');
                //Response::loadFooter();
                Response::sendHtml();
            }else{
                Response::bindData($r);
                //Response::loadHeader();
                Response::setTheme('admin');
                Response::loadHTML('user/sign_up');
                //Response::loadFooter();
                Response::sendHtml();
            }
            
            
        }
        
        
    }
    
    function otpStep2()
    {
        $input = Req::formData();
        $mobileNumber = $input->mobile_number;
        $otp = $input->otp;
        
        
        $customer = New CustomerTbl();
        
        $verifyOtp = SelwynDatabase::query("SELECT *
                    FROM register_otp
                    WHERE mobile_number = $mobileNumber AND created_at > DATE_ADD(NOW(), INTERVAL -30 MINUTE)
                    AND otp = $otp");
        if ($verifyOtp) {
            /*Find Customer*/
            $customer = CustomerTbl::findOneBy(array(
                CustomerTbl::DF_MOBILE_NUMBER => $mobileNumber
            ));
            
            $customerId = $customer->getId();
            $sql = "SELECT id, first_name, last_name FROM customer WHERE id = '$customerId' LIMIT 1";
            
            $checkCustomer = SelwynDatabase::query($sql);
            
            $r['status'] = TRUE;
            
            Session::inst()->setIsLogIn(TRUE);
            Session::inst()->setAdmin($checkCustomer);
            Response::redirect('/home');
        } else {
            /*OTP Not Verified*/
            $r['status'] = FALSE;
            $r['message'] = "OTP Verification Failed. Please try again";
            $r['data']['mobilenumber'] = $mobileNumber;
            //$this->loadPage('user/verify_otp', $r);
    
            Response::bindData($r);
            
            //Response::loadHeader();
            Response::setTheme('admin');
            Response::loadHTML('user/verify_otp');
            //Response::loadFooter();
            Response::sendHtml();
        }
        
    }
}