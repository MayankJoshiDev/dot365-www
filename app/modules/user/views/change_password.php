<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="bigboost">
    <meta name="keywords" content="bigboost">
    <meta name="author" content="bigboost">
    <link rel="icon" href="<?php echo THEME_URL ?>/assets/images/favicon/11.png" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo THEME_URL ?>/assets/images/favicon/11.png" type="image/x-icon"/>
    <title>DOT365.in</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/style.css">
    <!-- Icons -->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/fontawesome.css">

    <!-- Icons -->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/flipclock.css">

    <!--Slick slider css-->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/slick-theme.css">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/bootstrap.css">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/themify-icons.css">

    <!-- Theme css -->
    <link rel="stylesheet" id="color" type="text/css" href="<?php echo THEME_URL ?>/css/color11.css">

</head>
<body>

<!-- loader start -->
<div class="loader-wrapper">
    <div class=" bar">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>

<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>change password</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">change password</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->


<!--section start-->
<section class="pwd-page section-b-space ">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6 ">
                <h2>Change Your Password</h2>
                <form class="theme-form">
                    <div class="form-row">
                        <div class="col-md-12">
                            <input type="password" class="form-control"  placeholder="New Password" required="">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="col-md-12">
                            <input type="password" class="form-control"  placeholder="Re Enter Password" required="">
                        </div>
                        <a href="<?php echo SITE_DOMAIN. str::LOGIN_ROUTE ?>" class="btn btn-solid">Change Password</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!--Section ends-->

<!-- latest jquery-->
<script src="<?php echo THEME_URL ?>/js/jquery-3.3.1.min.js" ></script>

<!-- menu js-->
<script src="<?php echo THEME_URL ?>/js/menu.js"></script>

<!-- timer js-->
<script src="<?php echo THEME_URL ?>/js/flipclock.js"></script>

<!-- popper js-->
<script src="<?php echo THEME_URL ?>/js/popper.min.js" ></script>

<!-- slick js-->
<script  src="<?php echo THEME_URL ?>/js/slick.js"></script>

<!-- Bootstrap js-->
<script src="<?php echo THEME_URL ?>/js/bootstrap.js" ></script>


<!-- Bootstrap Notification js-->
<script src="<?php echo THEME_URL ?>/js/bootstrap-notify.min.js"></script>

<!-- Theme js-->
<script src="<?php echo THEME_URL ?>/js/script.js" ></script>

<script>
    $(window).on('load', function() {
        $('#exampleModal').modal('show');
    });
</script>

<!-- modal js-->
<script src="<?php echo THEME_URL ?>/assets/js/modal.js" ></script>

</body>

</html>