<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="bigboost">
    <meta name="keywords" content="bigboost">
    <meta name="author" content="bigboost">
    <link rel="icon" href="<?php echo THEME_URL ?>/assets/images/favicon/11.png" type="image/x-icon"/>
    <link rel="shortcut icon" href="<?php echo THEME_URL ?>/assets/images/favicon/11.png" type="image/x-icon"/>
    <title>DOT365.in</title>

    <!--Google font-->
    <link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/style.css">
    <!-- Icons -->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/fontawesome.css">

    <!-- Icons -->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/animate.css">
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/flipclock.css">

    <!--Slick slider css-->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/slick.css">
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/slick-theme.css">

    <!-- Bootstrap css -->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/bootstrap.css">

    <!-- Themify icon -->
    <link rel="stylesheet" type="text/css" href="<?php echo THEME_URL ?>/css/themify-icons.css">

    <!-- Theme css -->
    <link rel="stylesheet" id="color" type="text/css" href="<?php echo THEME_URL ?>/css/color11.css">

</head>
<body>

<!-- loader start -->
<div class="loader-wrapper">
    <div class=" bar">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
    </div>
</div>

<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space" style="padding-bottom: 20px;padding-top: 20px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>register</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">register</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->


<!--section start-->
<section class="register-page section-b-space" style="padding-bottom: 0;padding-top: 0;background-color: #fff !important;height: calc(100vh - 200px)">
    <div class="container" style="padding-bottom: 50px;padding-top: 50px;background-color: #fff !important;">
        <br>
        <div class="row">
            <div class="col-lg-12">
                <h3 class="title pt-0">create account</h3>
                <div class="theme-card">
                    <p class="text-danger"><?php if(isset($message)){
                        echo $message;
                        } ?></p>
                    <form class="theme-form" method="post" action="/create_account">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="email">First Name</label>
                                <input type="text" class="form-control" name="first_name" id="fname" placeholder="First Name" required="">
                            </div>
                            <div class="col-md-6">
                                <label for="review">Last Name</label>
                                <input type="text" class="form-control" name="last_name" id="lname" placeholder="Last Name" required="">
                            </div>
                        </div>


                                <input type="hidden" class="form-control" name="mobile" value="<?php echo $data['mobilenumber']; ?>" placeholder="Enter Mobile Number" required="">
                        <input type="checkbox" name="termcondition" value="1" required style="margin-left: 10px;
margin-right: 10px;"><a href="/termandconditions" target="_blank">Terms and Conditions</a>
                        <br style="clear: both">
                        <div class="form-row">
                            
                            <button type="submit" class="btn btn-solid">Create Account</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <br>
    </div>
</section>
<!--Section ends-->
<footer>
    <div class="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6 col-sm-12">
                    <div class="footer-end">
                        <p><i class="fa fa-copyright" aria-hidden="true"></i>  2020-21 Dot365</p>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 col-sm-12">
                    <div class="payment-card-bottom">
                        <ul>
                            <li>
                                <a class="text-uppercase" href="/termandconditions" style="color: #fff;font-weight: bold" target="_blank">Terms and Conditions</a>
                            </li>
                            <li>
                                <a href="fb.me/Dot365official" target="_blank">
                                    <img src="<?php echo THEME_URL ?>/images/fb.png" style="height: 25px;width: 25px">
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/dot365official/" target="_blank">
                                    <img src="<?php echo THEME_URL ?>/images/instagram.png" style="height: 25px;width: 25px">
                                </a>
                            </li>
                            <li>
                                <a href="#" onclick="openWhatsapp()" target="_blank">
                                    <img src="<?php echo THEME_URL ?>/images/whatsapp.png" style="height: 28px;width: 28px">
                                </a>
                            </li>
                            <li>
                                <a href="#" onclick="openCall()" target="_blank">
                                    <img src="<?php echo THEME_URL ?>/images/call.png" style="height: 28px;width: 28px">
                                </a>
                            </li>
                            <li>
                                <a href="mailto:info@dot365.in" target="_blank">
                                    <img src="<?php echo THEME_URL ?>/images/gmail.png" style="height: 25px;width: 25px">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- latest jquery-->
<script src="<?php echo THEME_URL ?>/js/jquery-3.3.1.min.js"></script>

<!-- menu js-->
<script src="<?php echo THEME_URL ?>/js/menu.js"></script>

<!-- timer js-->
<script src="<?php echo THEME_URL ?>/js/flipclock.js"></script>

<!-- popper js-->
<script src="<?php echo THEME_URL ?>/js/popper.min.js"></script>

<!-- slick js-->
<script src="<?php echo THEME_URL ?>/js/slick.js"></script>

<!-- Bootstrap js-->
<script src="<?php echo THEME_URL ?>/js/bootstrap.js"></script>


<!-- Bootstrap Notification js-->
<script src="<?php echo THEME_URL ?>/js/bootstrap-notify.min.js"></script>

<!-- Theme js-->
<script src="<?php echo THEME_URL ?>/js/script.js"></script>

<script>
    $(window).on('load', function () {
        $('#exampleModal').modal('show');
    });
</script>

<!-- modal js-->
<script src="<?php echo THEME_URL ?>/assets/js/modal.js"></script>

</body>

</html>