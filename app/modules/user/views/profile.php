<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space" style="padding-bottom: 20px;padding-top: 20px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>profile</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo SITE_DOMAIN. str::HOME_ROUTE ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">profile</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->


<!-- personal deatail section start -->
<section class="contact-page register-page section-b-space" style="padding-bottom: 0;padding-top: 0;min-height: calc(100vh - 200px);">
    <div class="container" style="padding-bottom: 30px;padding-top: 30px;background-color: #fff !important;min-height: calc(100vh - 200px);">
        <div class="row">
            <div class="col-sm-6">
                <div class="card card-style-1">
                    <div class="card-body">
                        <div class="d-flex flex-column align-items-center text-center mb-3">
                            <img src="<?php echo $data['customer_detail'][0]->profile_image; ?>" id="pimg" alt="Customer" class="rounded-circle" width="150">
                            <div class="mt-3">
                                <h4><?php  echo $data['customer_detail'][0]->first_name.' '.$data['customer_detail'][0]->last_name;?></h4>
                                <p class="text-secondary mb-1"><?php echo $data['customer_detail'][0]->mobile_number; ?></p>
                            </div>
                        </div>
                        <form class="theme-form" method="post" action="/customer_profile"  enctype="multipart/form-data">
                            <div class="form-row">
                                <div class="col-md-12">

                                    <input type="file" name="profile_image" id="profile_image" style="display: none;" required/>

                                </div>
                                <input type="hidden" name="customer_id" value="<?php echo $data['customer_detail'][0]->id;?>">
                                <input type="hidden" name="action"  value="upload">
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-solid">Update Profile Image</button>

                                </div>
                            </div>
                        </form>
                        <div class="col-md-12 text-center mt-4">
                        <a href="<?php echo SITE_DOMAIN . str::ADDRESS_LIST_ROUTE ?>" class="btn-solid btn">My Address</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <h3 class="title pt-0">PERSONAL DETAIL</h3>
                <form class="theme-form" method="post" action="/customer_profile"  >
                    <div class="form-row">
                        <div class="col-md-12">
                            <label for="name">First Name</label>
                            <input type="text" value="<?php echo $data['customer_detail'][0]->first_name; ?>" class="form-control" name="first_name" id="first_name" placeholder="Enter Your name" required="">
                        </div>
                        <div class="col-md-12">
                            <label for="email">Last Name</label>
                            <input type="text" value="<?php echo $data['customer_detail'][0]->last_name; ?>" class="form-control" name="last_name" id="last_name" placeholder="Email" required="">
                        </div>
                        <div class="col-md-12">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" value="<?php echo $data['customer_detail'][0]->email; ?>" name="email"  placeholder="Email" >
                        </div>
                        <div class="col-md-12">
                            <label for="date">Date Of Birth</label>
                            <input type="date" class="form-control" name="dob" value="<?php echo $data['customer_detail'][0]->dob; ?>"  placeholder="Enter Date Of Birth" >
                        </div>
                        <div class="col-md-12 select_input">
                            <label for="review">Gender</label>
                            <select class="form-control" name="gender" size="1">
                                <option value="0" <?php echo ($data['customer_detail'][0]->gender == 0)?'selected':''; ?>>Select Gender</option>
                                <option value="1" <?php echo ($data['customer_detail'][0]->gender == 1)?'selected':''; ?>>Male</option>
                                <option value="2" <?php echo ($data['customer_detail'][0]->gender == 2)?'selected':''; ?>>Female</option>
                            </select>
                        </div>

                        <input type="hidden" name="customer_id" value="<?php echo $data['customer_detail'][0]->id;?>">
                        <input type="hidden" name="action"  value="edit">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-solid">Save Profile</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
</section>
<!-- Section ends -->