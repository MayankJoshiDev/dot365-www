<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space" style="padding-top: 20px;padding-bottom: 20px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>Address</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo SITE_DOMAIN. str::HOME_ROUTE ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">My Address</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->

<!-- section start -->
<section class="section-b-space" style="padding-bottom: 0;padding-top: 0;min-height: calc(100vh - 200px)">
    <div class="container" style="padding-bottom: 30px;padding-top: 30px;background-color: #fff !important;min-height: calc(100vh - 200px);">
        <div class="checkout-page">
            <div class="checkout-form">
                <form>
                    <div class="row">
                        <div class="col-lg-6 col-sm-12 col-xs-12">
                            <div class="checkout-title">
                                <h3>My Address</h3></div>
                            <div class="">
                                <div id="addressHolder">
                                    <?php foreach($data['customer_address'] as $add) { ?>
                                    <div class="border-radius-0 border-bottom m-0 card">
                                        <div class="p-2 card-block">
                                            <h4><?php echo $add->customer_name; ?>
                                                <span class="float-right">
                                                   <a href="<?php echo SITE_DOMAIN . str::NEW_ADDRESS_ROUTE ?>?id=<?php echo $add->id; ?>"><i class="icon-size ti-pencil mr-2"></i></a>
                                                   <i class="icon-size ti-trash" onclick="Deleteadd(<?php echo $add->id; ?>)"></i>
                                                </span>
                                            </h4>
                                            <p class="mb-2"><?php echo $add->line1; ?><br>
                                                <?php echo $add->line2; ?> <?php echo $add->landmark; ?> <?php echo $add->city; ?> - <?php echo $add->pincode; ?> , India</p>
                                            <p class="mb-2">Mobile Number :<?php echo $add->mobile_number; ?></p>
                                            <p class="mb-2">Type :<?php echo $add->address_name; ?></p>
                                        </div>
                                    </div>
                                   <?php } ?>
                                </div>
                            </div>
                            <br>
                            <div class="text-right"><a href="<?php echo SITE_DOMAIN . str::NEW_ADDRESS_ROUTE ?>" class="btn-solid btn">Add New Address</a></div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- section end -->

<script>
   function Deleteadd(id) {
       var result = confirm("Want to delete?");
       if (result) {
           $.ajax({
               url: "/address_delete",
               type: "POST",
               data: {
                   address_id: id,

               },
               success: function (result) {
                   var parsedJson = $.parseJSON(result);
                   if (parsedJson.status == true) {
                       location.reload();
                   }


               }
           });
       }
   }
</script>