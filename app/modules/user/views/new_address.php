<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>New Address</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo SITE_DOMAIN. str::HOME_ROUTE ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">New Address</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->

<!-- address section start -->
<section class="contact-page register-page section-b-space" style="padding-bottom: 0;padding-top: 0;min-height: calc(100vh - 200px);">
    <div class="container" style="padding-bottom: 30px;padding-top: 30px;background-color: #fff !important;min-height: calc(100vh - 200px);">
        <div class="row">
            <div class="col-sm-12">
                <h3 class="title pt-0">SHIPPING ADDRESS</h3>
                <?php if(isset($_GET['id'])) { ?>
                    <form class="theme-form" action="/address_action" method="post">
                        <input type="hidden" name="getback" class="form-control" value="false" required="">
                        <input type="hidden" name="action" class="form-control" value="edit" required="">
                        <input type="hidden" name="address_lat" class="form-control" value="<?php echo $data['data'][0]->address_lat; ?>" required="">
                        <input type="hidden" name="address_long" class="form-control" value="<?php echo $data['data'][0]->address_long; ?>" required="">
                        <input type="hidden" name="address_id" class="form-control" value="<?php echo $data['data'][0]->id; ?>" required="">
                        <input type="hidden" name="customer_id" class="form-control" value="<?php  if(isset($chat_user)) { echo $chat_user['uid']; }; ?>" required="">
                        <div class="form-row">
                            <div class="col-md-6">
                                <label for="name">Billing / Invoice Name</label>
                                <input type="text" name="customer_name" class="form-control" value="<?php echo $data['data'][0]->customer_name; ?>" placeholder="Enter Name" required="">
                            </div>
                            <div class="col-md-6">
                                <label for="number">Mobile Number</label>
                                <input type="number" name="mobile_number" class="form-control" value="<?php echo $data['data'][0]->mobile_number; ?>" placeholder="Enter Mobile Number" required="">
                            </div>
                            <div class="col-md-6">
                                <label for="name">Flat / House No.</label>
                                <input type="text" name="line1" class="form-control" value="<?php echo $data['data'][0]->line1; ?>" placeholder="Flat / House No." required="">
                            </div>
                            <div class="col-md-6">
                                <label for="name">Colony / Street / Locality</label>
                                <input type="text" name="line2" class="form-control" value="<?php echo $data['data'][0]->line2; ?>" placeholder="Colony / Street / Locality" required="">
                            </div>
                            <div class="col-md-6">
                                <label for="name">Landmark</label>
                                <input type="text" name="landmark" class="form-control" value="<?php echo $data['data'][0]->landmark; ?>" placeholder="Landmark" required="">
                            </div>
                            <div class="col-md-6">
                                <label for="name">Area</label>
                                <input type="text" name="area" class="form-control" value="<?php echo $data['data'][0]->area; ?>" placeholder="Area" required="">
                            </div>
                            <div class="col-md-6">
                                <label for="name">City</label>
                                <input type="text" name="city" class="form-control" value="<?php echo $data['data'][0]->city; ?>" placeholder="City" required="">
                            </div>
                            <div class="col-md-6">
                                <label for="number">Pin Code</label>
                                <input type="number" name="pincode" class="form-control" value="<?php echo $data['data'][0]->pincode; ?>" placeholder="Pin Code" required="">
                            </div>
                            <div class="col-md-6">
                                <label for="number">Gst Number</label>
                                <input type="text" name="gst_number" class="form-control" value="<?php echo $data['data'][0]->gst_number; ?>" placeholder="Gst Number" >
                            </div>
                            <div class="col-md-6 select_input">
                                <label for="review">State</label>
                                <select class="form-control" size="1" name="state_id">
                                    <option value="0">Select State</option>
                                    <?php foreach ($state['data']['state'] as $st) { ?>
                                        <option value=<?php echo $st->id; ?> <?php echo ($data['data'][0]->state_id == $st->id)?"selected":""; ?>><?php echo $st->name; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <label for="review">Address Type</label>
                                <ul class="chec-radio">
                                    <li class="pz">
                                        <label class="radio-inline">
                                            <input type="radio" id="hotelType" name="address_name" class="pro-chx" value="Hotel" <?php echo ($data['data'][0]->address_name == "Hotel")?"checked":""; ?>>
                                            <div class="clab">HOTEL</div>
                                        </label>
                                    </li>
                                    <li class="pz">
                                        <label class="radio-inline">
                                            <input type="radio" id="retailType" name="address_name" class="pro-chx" value="Retail" <?php echo ($data['data'][0]->address_name == "Retail")?"checked":""; ?>>
                                            <div class="clab">RETAIL</div>
                                        </label>
                                    </li>
                                    <li class="pz">
                                        <label class="radio-inline">
                                            <input type="radio" id="personalType" name="address_name" class="pro-chx" value="Personal" <?php echo ($data['data'][0]->address_name == "Personal")?"checked":""; ?>>
                                            <div class="clab">PERSONAL</div>
                                        </label>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-sm btn-solid" >Update Address</button>
                            </div>
                        </div>
                    </form>
                <?php } else { ?>
                <form class="theme-form" action="/address_action" method="post">
                    <input type="hidden" name="action" class="form-control" value="add" required="">
                    <?php if(isset($_GET['getback'])) { ?>
                        <input type="hidden" name="getback" class="form-control" value="true" required="">
                    <?php  } else { ?>
                        <input type="hidden" name="getback" class="form-control" value="false" required="">
                    <?php  }  ?>
                    <input type="hidden" name="address_lat" class="form-control" value="22.728392" required="">
                    <input type="hidden" name="address_long" class="form-control" value="71.637077" required="">
                    <input type="hidden" name="customer_id" class="form-control" value="<?php  if(isset($chat_user)) { echo $chat_user['uid']; }; ?>" required="">
                    <div class="form-row">
                        <div class="col-md-6">
                            <label for="name">Billing / Invoice Name</label>
                            <input type="text" name="customer_name" class="form-control" placeholder="Enter Name" required="">
                        </div>
                        <div class="col-md-6">
                            <label for="number">Mobile Number</label>
                            <input type="number" name="mobile_number" class="form-control" placeholder="Enter Mobile Number" required="">
                        </div>
                        <div class="col-md-6">
                            <label for="name">Flat / House No.</label>
                            <input type="text" name="line1" class="form-control" placeholder="Flat / House No." required="">
                        </div>
                        <div class="col-md-6">
                            <label for="name">Colony / Street / Locality</label>
                            <input type="text" name="line2" class="form-control" placeholder="Colony / Street / Locality" required="">
                        </div>
                        <div class="col-md-6">
                            <label for="name">Landmark</label>
                            <input type="text" name="landmark" class="form-control" placeholder="Landmark" required="">
                        </div>
                        <div class="col-md-6">
                            <label for="name">Area</label>
                            <input type="text" name="area" class="form-control" placeholder="Area" required="">
                        </div>
                        <div class="col-md-6">
                            <label for="name">City</label>
                            <input type="text" name="city" class="form-control" placeholder="City" value="Mumbai" required="">
                        </div>
                        <div class="col-md-6">
                            <label for="number">Pin Code</label>
                            <input type="number" name="pincode" class="form-control" placeholder="Pin Code" required="">
                        </div>
                        <div class="col-md-6">
                            <label for="number">Gst Number</label>
                            <input type="text" name="gst_number" class="form-control" placeholder="Gst Number" >
                        </div>
                        <div class="col-md-6 select_input">
                            <label for="review">State</label>
                            <select class="form-control" size="1" name="state_id">
                                <option value="0">Select State</option>
                               <?php foreach ($state['data']['state'] as $st) { ?>
                                   <?php if($st->id == 27){ ?>
                                       <option selected value=<?php echo $st->id; ?>><?php echo $st->name; ?></option>
                                   <?php }else{?>
                                       <option value=<?php echo $st->id; ?>><?php echo $st->name; ?></option>
                                   <?php } ?>
                                   
                                <?php } ?>
                            </select>
                        </div>
                        <div class="col-md-6">
                            <label for="review">Address Type</label>
                            <ul class="chec-radio">
                                    <li class="pz">
                                        <label class="radio-inline">
                                            <input type="radio" id="hotelType" name="address_name" class="pro-chx" value="Hotel" checked>
                                            <div class="clab">HOTEL</div>
                                        </label>
                                    </li>
                                    <li class="pz">
                                        <label class="radio-inline">
                                            <input type="radio" id="retailType" name="address_name" class="pro-chx" value="Retail">
                                            <div class="clab">RETAIL</div>
                                        </label>
                                    </li>
                                    <li class="pz">
                                        <label class="radio-inline">
                                            <input type="radio" id="personalType" name="address_name" class="pro-chx" value="Personal">
                                            <div class="clab">PERSONAL</div>
                                        </label>
                                    </li>
                                </ul>
                        </div>
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-sm btn-solid" >Save Address</button>
                        </div>
                    </div>
                </form>
                <?php } ?>
            </div>
        </div>
    </div>
</section>
<!-- Section ends -->