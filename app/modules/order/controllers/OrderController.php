<?php


class OrderController extends BackendController
{
    static $inst;
    static function inst()
    {
        if(!self::$inst) { self::$inst = new self();}
        return self::$inst;
    }

    function placeOrder(){
        $input = Req::formData();
        $order = new OrdersTbl();
        $user = Session::inst()->getData('ADMIN');
        $data = cartModel::viewCustomerCartFx($user[0]->id);
        if($input->direct == "yes"){
            $pinput = (object) array("customer_id" => $user[0]->id,"product_id" => $input->pid);
           $data['product'] = ProductModel::productDetailFx($pinput);

            $input1 = (object) array( "product_id" => $input->pid,"qty" =>$input->pqty ,"customer_id" => $user[0]->id,"deliver_address_id" => $input->deliver_address_id,"total_cash" => $input->total_cash, "gateway_payment_id" => null,"total_winwin_points" => $input->total_winwin_points,"total_order" => $input->total_order, "order_items" => $data['product']['data']['product_detail'],"referral_code" => $input->referral_code, "order_notes" =>$input->order_notes,"payment_method" => $input->payment_method,"direct" =>"yes");

        }
        else
        {
            $input1 = (object) array("customer_id" => $user[0]->id,"deliver_address_id" => $input->deliver_address_id,"total_cash" => $input->total_cash,"total_order_paid" => $input->total_order_paid,"gateway_payment_id" => $input->gateway_payment_id,"total_winwin_points" => $input->total_winwin_points,"total_order" => $input->total_order, "order_items" => $data['data']['kart'],"referral_code" => $input->referral_code,"payment_method" => $input->payment_method ,"order_notes" =>$input->order_notes,"direct" =>"no");

        }

        $res = orderModel::placeOrderFx($input1);
        if($res['status'] == false){
            Response::redirect('/product_cart');
        }else{ ?>
    <script>
    localStorage.removeItem("cartproduct");
    </script>
        <?php    
        $this->loadPage('order/order_success',$res);
        }
    }

    function orderHistory()
    {
        if (!Session::inst()->getIsLogin()) {
            Response::redirect('/login');
        }
        else
        {
            $user = Session::inst()->getData('ADMIN');
            $obj = (object) array("customer_id" => $user[0]->id,"order_number" => null,"from_date" => null, "to_date" => null);
            $r = orderModel::orderHistoryFx($obj);

            $this->loadPage('order/order_history',$r);
        }


    }

    function orderSuccess()
    {
        if (!Session::inst()->getIsLogin()) {
            Response::redirect('/login');
        }
        else {

            $this->loadPage('order/order_success');
        }
    }

    function orderDetail()
    {
        if (!Session::inst()->getIsLogin()) {
            Response::redirect('/login');
        }
        else {
            if(isset($_GET['id']))
            {
                $arry = (object) array("order_id" => $_GET['id']);
                $dataarr['data'] = orderModel::orderDetailFx($arry);

                $this->loadPage('order/order_detail',$dataarr);
            }

        }
    }

	function payonline()
	{
		 $input = Req::formData();
		 $this->loadPage('order/pay',$input);
	}
	function payonline2()
	{
		 $input = Req::formData();
		 $pinput = (object) array("order_id" => $input->order_id,"payment_id" => $input->payment_id);
		 $dat=orderModel::payOnlineFx($pinput);		
         Response::redirect('/order_detail?id='.$input->order_id);
	}
	function order_agian()	
	{ 	$input2 = Req::formData();
		$user = Session::inst()->getData('ADMIN');
        $input = (object)array('customer_id' => $user[0]->id,'order_id'=>$input2->order_id);	
		$res = orderModel::orderAgianFx($input);
		Response::sendJson($res);
	}
	function verify()
	{
		 $input = Req::formData();
		 $this->loadPage('order/verify',$input);
	}
    function checkout()
    {
        if (!Session::inst()->getIsLogin()) {
            Response::redirect('/login');
        }
        else if(isset($_GET['direct'])) {
            $user = Session::inst()->getData('ADMIN');
            $input = (object)array('customer_id' => $user[0]->id);

            $data['address'] = customerModel::customerProfileModelFx( $input );
            $data['total'] =  $_GET['pqty'] * $_GET['psprice'];
            $this->loadPage('order/checkout',$data);

        }
        else {
            $user = Session::inst()->getData('ADMIN');
            $input = (object)array('customer_id' => $user[0]->id);

            $data['address'] = customerModel::customerProfileModelFx( $input );
            $data['cart'] = cartModel::viewCustomerCartFx($user[0]->id);
            foreach ($data['cart']['data']['kart'] as $total)
            {
                $totalp[] = $total->qty * $total->selling_price;
            }
            $data['total'] = array_sum($totalp);
            $data['order_items'] = cartModel::viewCustomerCartFx($user[0]->id);

            $this->loadPage('order/checkout',$data);
        }
    }
	function getdelcharge()
	{
			$DELIVERY_CHARGE1=DELIVERY_CHARGE;
			  $user = Session::inst()->getData('ADMIN');
			  $uid= $user[0]->id;
				$cart22= ProductModel::getdeliverychrg_cid($uid,$DELIVERY_CHARGE1);
				$cart['data2']=$cart22;
			$res=round($cart22[0]->total_weight,0,PHP_ROUND_HALF_UP);
			$tcharge=60;
				if($res<=1)
				{
						$tcharge=($DELIVERY_CHARGE1)+($DELIVERY_CHARGE1);
						$cart['data3']=$tcharge."--------".$res;		
				}
				else {
					$tcharge=($res*$DELIVERY_CHARGE1)+($DELIVERY_CHARGE1);
					if($tcharge%2!=0)
					{
						$tcharge=$tcharge+($DELIVERY_CHARGE1)+($DELIVERY_CHARGE1);
					}
					
					//$tcharge=(round($res,0,PHP_ROUND_HALF_UP )*50)/0.5;
					$cart['data3']=$tcharge."--------".$res;		
				}	
				if($uid == 1219)
				{
					$tcharge=0;
				}
				$tcharge=round($tcharge,0,PHP_ROUND_HALF_UP);
				 // return $tcharge;
				   Response::sendJson($tcharge);
	}

    function cancelorder() {
        $input = Req::json();
        $res = orderModel::orderCancelFx($input);
        Response::sendJson($res);

    }

    function changeOrderRemove() {
        $input = Req::json();
        $res = orderModel::removeOrderItemFX($input);
        Response::sendJson($res);
    }


    function changeOrderItem() {
        $input = Req::json();
        $res = orderModel::updateOrderItemFX($input);
        Response::sendJson($res);
    }
}