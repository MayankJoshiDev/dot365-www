<!-- breadcrumb start -->

<section class="breadcrumb-section section-b-space" style="padding-bottom: 20px;padding-top: 20px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>Check-out</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo SITE_DOMAIN . str::HOME_ROUTE ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Check-out</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->

<!-- section start -->
<section class="section-b-space" style="padding-top: 0px;padding-bottom: 0;min-height: calc(100vh - 200px);">
    <div class="container" style="background-color: #fff !important;min-height: calc(100vh - 200px);">
        <div class="checkout-page">
            <div class="checkout-form">
                <form id="placeorderform" method="post" action="/placeorder" >


                    <input type="hidden" id="total_cash" name="total_cash" >
                    <input type="hidden" name="total_order" id="total_order" value="<?php echo $total; ?>">
                    <input type="hidden" name="total_winwin_points" value="0" >
					<input type="hidden" name="gateway_payment_id" id="gateway_payment_id" value="0">
					<input type="hidden" name="total_order_paid" id="total_order_paid" value="0">
                    <?php if (isset($_GET['direct'])) { ?>
                        <input type="hidden" name="direct" value="yes">
                        <input type="hidden" name="pid" value="<?php echo $_GET['pid']; ?>">
                        <input type="hidden" name="pqty" value="<?php echo $_GET['pqty']; ?>">
                        <input type="hidden" name="psprice" value="<?php echo $_GET['psprice']; ?>">
                    <?php  } else { ?>
                        <input type="hidden" name="direct" value="no">
                    <?php } ?>
                    <input type="hidden" name="customer_id" value="<?php if (isset($chat_user)) {
                                                                        echo $chat_user['uid'];
                                                                    } ?>">
                    <div class="row">
                        <div class="col-lg-6 col-sm-12 col-xs-12">
                            <div class="checkout-title">
                                <h3>Select Delivery Address</h3>
                            </div>
                            <div class="">
                                <div id="addressHolder">
                                    <?php foreach ($address['data']['customer_address'] as $add) { ?>
                                        <div class="border-radius-0 border-bottom m-0 card">
                                            <div class="p-2 card-block">
                                                <div class="form-radio">
                                                    <div class="radio radio-inline">
                                                        <label>
                                                            <input type="radio" name="deliver_address_id" id="deliver_address_id" value="<?php echo $add->id; ?>" onclick="getdel('<?php echo $add->city; ?>')" required>
                                                            <i class="helper"></i>
                                                            <h4><?php echo $add->customer_name; ?></h4>
                                                        </label>
                                                    </div>

                                                </div>
                                                <p class="mb-2"><?php echo $add->line1; ?><br>
                                                    <?php echo $add->line2; ?> <?php echo $add->landmark; ?> <?php echo $add->city; ?> - <?php echo $add->pincode; ?> , India</p>
                                                <p class="mb-2">Mobile Number :<?php echo $add->mobile_number; ?></p>
                                                <p class="mb-2">Type :<?php echo $add->address_name; ?></p>
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <br>
                            <div class="text-right"><a href="<?php echo SITE_DOMAIN . str::NEW_ADDRESS_ROUTE ?>?getback=true" class="btn-solid btn">Add New Address</a></div>
                        </div>
                        <?php
                        if (sizeof($address['data']['customer_address']) > 0) { ?>
                            <div class="col-lg-6 col-sm-12 col-xs-12 mt-lg-5">

                                <div class="checkout-details">
                                    <div class="col-md-12">
                                        <label for="name">Enter Referral Code (Optional)</label>
                                        <input type="text" id="referral_code" name="referral_code" class="form-control" placeholder="Referral Code">
                                    </div>

                                    <div class="payment-box">
                                    <div class="col-md-12">
                                            <label for="name">Notes</label>
                                            <textarea name="order_notes" id="odnote" cols="30" rows="3"  onpaste="checkvar()" onkeyup="checkvar()"></textarea>
                                        </div>
                                        <div class="upper-box">

                                            <div class="payment-options">
                                                <ul>
												<li id="msg123"></li>
												<li>	<style>
																td{
																	padding-left:10px;
																}
																</style>
                                                                   
																	<table>
																	<tr>																
																	<td>Total</td><td>&#8377;<?php echo round($total,0,PHP_ROUND_HALF_UP );  ?>/-</td></tr>
																	<tr><td>Delivery Charge</td><td>&#8377;<span id="dcharge">0</span></td></tr>
																	<tr style="border-top:1px solid black;border-bottom:1px solid black"><td><b>Grand Total</b></td><td><b>&#8377;<span id="gtotal1">0</span></b></td></tr>
																
																	</table>
												</li>
                                                    <li id="cod">
                                                        <div class="form-radio">
                                                            <div class="radio radio-inline">
                                                                <label>
															 <input id="Cod" type="radio" name="payment_method"  value="COD" required onclick="showcod()">
                                                                    <i class="helper"></i>
                                                                    <h4>Cash On Delivery</h4>
																	
                                                               <!--     <p>Pay <b>&#8377;<?php echo $total; ?></b> On Delivery of Your Items</p>
																	-->
                                                                </label>
                                                            </div>

                                                        </div>
                                                    </li>

                                                    <li>
                                                        <div class="form-radio">
                                                            <div class="radio radio-inline">
                                                                <label>
                                                                    <input id="Online" type="radio" name="payment_method"  value="Online" required checked onclick="showonline()"> 
                                                                    <i class="helper"></i>
                                                                    <h4>Pay Online <span class="blink" style="font-size:12px;color:red"> New</span></h4> 
                                                                </label>
                                                            </div>
														

                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                       
                                        <div class="text-right" style="float:right;margin-top:-20px"><button type="button" onclick="placeOrderDirect()" class="btn-solid btn" id="placeorder" style="display:none">Place Order</button>
										<input id="paymentol" type="button" onclick="myFunction()" class="btn btn-solid"   value="Proceed for Payment"></div>
										
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>

                        <?php } ?>

                    </div>
                </form>
				
					<div id="sample code ">
					<form id="checkout-selection" action="payonline" method="POST">		
						<input type="hidden" name="item_name" value="Dot365.in">
						<input type="hidden" name="item_description" value="Payment for Your Order">
						<input type="hidden" name="item_number" value="2020/12/15">
						<input type="hidden" name="amount" value="1">
						<input type="hidden" name="cust_id" value="<?php if (isset($chat_user)) {
                                                                        echo $chat_user['uid'];
                                                                    } ?>">
						<input type="hidden" name="city" id="city" value="">
						<input type="hidden" name="address" value="ABCD Address">
						<input type="hidden" name="currency" value="INR">	
						<input type="hidden" name="cust_name" value="<?php echo $add->customer_name; ?>">								
						<input type="hidden" name="email" value="">	
						<input type="hidden" name="contact" value="<?php echo $add->mobile_number; ?>">			
						
						<input type="hidden" id="total_cash2" name="total_cash2" >
						<input type="hidden" name="total_order1" id="total_order1" value="<?php echo $total; ?>">
						<input type="hidden" name="total_winwin_points" value="0">
						<input type="hidden" name="direct" value="no">
						<input type="hidden" name="address_id" id="address_id" >
						<input type="hidden" name="notes" id="notes" >
						<input type="hidden" id="referralcode" name="referralcode" >
						<input type="hidden" name="payment_method2" id="payment_method2" >
					<!--	<input type="button" onclick="myFunction()" class="btn btn-solid" value="Payment">	-->				
					</form>	
				<!--	<button id="rzp-button1" class="btn btn-solid">Pay</button>-->
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>

var options = {
    "key": "rzp_live_2BxmADaGUn1TWW", // Enter the Key ID generated from the Dashboard
    "amount": "50000", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    "currency": "INR",
    "name": "Acme Corp",
    "description": "Test Transaction",
    "image": "https://example.com/your_logo",
    "order_id": "order_9A33XWu170gUtm", //This is a sample Order ID. Pass the `id` obtained in the response of Step 1
    "handler": function (response){
        alert(response.razorpay_payment_id);
        alert(response.razorpay_order_id);
        alert(response.razorpay_signature)
    },
    "prefill": {
        "name": "Gaurav Kumar",
        "email": "gaurav.kumar@example.com",
        "contact": "9999999999"
    },
    "notes": {
        "address": "Razorpay Corporate Office"
    },
    "theme": {
        "color": "#3399cc"
    }
};
var rzp1 = new Razorpay(options);
rzp1.on('payment.failed', function (response){
        alert(response.error.code);
        alert(response.error.description);
        alert(response.error.source);
        alert(response.error.step);
        alert(response.error.reason);
        alert(response.error.metadata.order_id);
        alert(response.error.metadata.payment_id);
});
document.getElementById('rzp-button1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
}
</script>
															</div>
            </div>
        </div>
    </div>
</section>

<script>

function placeOrderDirect() {
	document.getElementById("placeorder").style.cursor = "progress";
	document.getElementById("placeorder").disabled=true;
	//$('#exampleModal').modal('show');
	setTimeout(placeOrder2, 1000);
	
}
function placeOrder2()
{
	
	
	var gg=$('#gtotal1').html();
	if(gg<=0)
	{
		alert("Please Select Your Address Again");
		document.getElementById("placeorder").style.cursor = "pointer";
		document.getElementById("placeorder").disabled=false;
	}
	else{
		
		
		document.getElementById("gateway_payment_id").value="";
		document.getElementById("total_order").value=gg;
		//document.getElementById("address_id").value=document.getElementById("deliver_address_id").value;
		//document.getElementById("notes").value=document.getElementById("odnote").value;	
		//document.getElementById("payment_method").value=document.querySelector('input[name="payment_method"]:checked').value;
		//document.getElementById("payment_method").value="COD";
		//alert(document.getElementById("payment_method2").value);
		document.getElementById("placeorderform").submit();
	}
	//$("body").css("cursor", "default");
	
	
}
function myFunction() {
	var gg=$('#gtotal1').html();
	if(gg<=0)
	{
		alert("Please Select Your Address Again");
	}
	else{
		
		//alert("Online Payment is Under Maintainence , You can Pay Online Via Dot365 App");
		document.getElementById("address_id").value=document.getElementById("deliver_address_id").value;
		document.getElementById("notes").value=document.getElementById("odnote").value;
		document.getElementById("referralcode").value=document.getElementById("referral_code").value;
	
	 var ele = document.getElementsByName('payment_method');
              
            for(i = 0; i < ele.length; i++) {
                if(ele[i].checked)
            
						document.getElementById("payment_method2").value=ele[i].value;
            }
			
			//alert(document.getElementById("payment_method2").value);
		//document.getElementById("payment_method2").value=document.querySelector('input[name="payment_method"]:checked').value;
		
		 document.getElementById("checkout-selection").submit();
	}
 
}
function checkvar() {
    var currText = $("#odnote").val();
    if (currText.length > 1000) {
        var text = $("#odnote").text();
        $("#odnote").text(text.substr(0, 1000));
        alert("You have reached the maximum length for Note");        
    }
}

  // Store
  //localStorage.setItem("lastname", "Smith");
  // Retrieve
  //alert(localStorage.getItem("lastname"));
  //alert(localStorage.getItem("delivery_charge"));


function onload22()
{
	
	 // document.getElementById("grandtotal").value = "16";
	//	 alert("Loaded");
  //$('#dcharge').html(""+localStorage.getItem("delivery_charge"));
 // $('#gtotal1').html(""+localStorage.getItem("grand_total"));
 
}
onload22();

	function checkfree(val22)
		{
				if(val22<=500)
				{
					$('#msg123').html("<span style='color:red'>Increase your Total Above Rs 500 to get Free Delivery</span>");
						$.ajax({
							url: "/get_del_charge",
							type: "GET", //send it through get method
							
							success: function(response) {
							//	alert("hiiiiiiiiiiiiiiiiiiiiiiiiii"+response);
									$('#dcharge').html(response+"/-");
									localStorage.setItem("delivery_charge",response);									
									var tt=val22+parseInt(response);
									localStorage.setItem("grand_total", tt);
									$('#gtotal1').html(tt);
									document.getElementById("total_cash2").value=response;
									document.getElementById("total_cash").value=response;
									document.getElementById("total_order").value=tt;
								    //alert("total"+tt+" dcharge"+response);
							},
							error: function(xhr) {
								//Do Something to handle error
								toastFunction("Something Wrong Try Again!!");
							}
						});
				}
				else
				{
						$('#dcharge').html(0);
						document.getElementById("total_cash2").value="0";
						document.getElementById("total_cash").value="0";
						
						var tt=val22+0;
						document.getElementById("total_order").value=tt;
						$('#gtotal1').html(tt+"/-");
				}
		}
		function showcod()
		{
			 document.getElementById("Cod").checked = true;
			 $("#cod").css("display","block");
			 $("#placeorder").css("display","block");
			 $("#paymentol").css("display","none");
		}	
		function showonline()
		{
			 //$("#cod").css("display","none");
			 $("#placeorder").css("display","none");
			  $("#paymentol").css("display","block");
		}		
		function getdel(val1)
		{
			var gtotal=<?php echo round($total,0,PHP_ROUND_HALF_UP );  ?>;
			var ss=val1.toUpperCase();
			  $("#city").val(ss);
			if(ss=="MUMBAI")
			{
				//alert("Free delivery Available in Mumbai");
				checkfree(gtotal);
				showcod();
			}
			else if(ss=="THANE")
			{
			//	alert("Free delivery Available in Thane");
				showcod();
				checkfree(gtotal);
			}
			else if(ss=="NAVIMUMBAI" || ss=="NAVI MUMBAI")
			{
			//	alert("Free delivery Available in Navi Mumbai");
				showcod();
				checkfree(gtotal);
			}
			// else if(ss=="PUNE")
			// {
				// alert("Free delivery Available in Pune");
				// showcod();
				// checkfree(gtotal);
			// }
			else{
				$('#msg123').html("");
				  $("#cod").css("display","none");
				  $("#placeorder").css("display","none");
				  $(paymentol).css("display","block");
				   document.getElementById("Online").checked = true;
				//alert("Delivery Charge is Applied");
								$.ajax({
							url: "/get_del_charge",
							type: "GET", //send it through get method
							
							success: function(response) {
								 
									
									
									$('#dcharge').html(response+"/-");
									document.getElementById("total_cash2").value=response;
									document.getElementById("total_cash").value=response;
									localStorage.setItem("delivery_charge",response);				
									var tt=gtotal+parseInt(response);
									localStorage.setItem("grand_total", tt);
									$('#gtotal1').html(tt);
								//alert("----------"+document.getElementById("total_cash").value);
									//document.getElementById("grandtotal").value=500;	alert(response);
									
							},
							error: function(xhr) {
								//Do Something to handle error
								toastFunction("Something Wrong Try Again!!");
							}
						});
			}
		}
		
	 var gt3 = 10;

		function dd(val2)
		{
				 var dchrg2=val2;
				 var gt2=0;
			     var x=10;
				 if(dchrg2>=1 && dchrg2<=500)
				 {
					
					gt3=parseInt(x)+parseInt(val2)+100;
				 }
				 else if(dchrg2>=501 && dchrg2<=999)
				 {
					  gt3=parseInt(x)+parseInt(val2)+60;
				 }
				  else if(dchrg2>=1000)
				 {
					   gt3=parseInt(x)+parseInt(val2)+0;
				 }
				 else
				 {
					 gt3=parseInt(x)+parseInt(val2)+100;
				
				 }
				document.getElementById("total_cash").value=""+gt3; 
				
		 };  
		 
			//dd("<?php echo $total; ?>");		
			
</script>
<style>
  .blink {
        animation: blinker 0.6s linear infinite;
        color: #1c87c9;
        font-size: 30px;
        font-weight: bold;
        font-family: sans-serif;
      }
      @keyframes blinker {
        50% {
          opacity: 0;
        }
      }
      .blink-one {
        animation: blinker-one 1s linear infinite;
      }
      @keyframes blinker-one {
        0% {
          opacity: 0;
        }
      }
      .blink-two {
        animation: blinker-two 1.4s linear infinite;
      }
      @keyframes blinker-two {
        100% {
          opacity: 0;
        }
      }
</style>
<!-- section end -->