<!-- thank-you section start -->
<section class="section-b-space light-layout" style="padding-top: 20px;padding-bottom: 20px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="success-text"><i class="fa fa-check-circle" aria-hidden="true"></i>
                    <h2>thank you</h2>
                    <p>Payment is successfully processsed and your order is on the way</p>
                    <p>Transaction ID:<?php echo $data['order_detail'][0]->order_number; ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Section ends -->
<script>
    localStorage.removeItem("cartproduct");
</script>

<!-- order-detail section start -->
<section class="section-b-space" style="padding-top: 0px;padding-bottom: 0;min-height: calc(100vh - 200px)">
    <div class="container" style="min-height: calc(100vh - 200px);padding-top: 30px;padding-bottom: 30px;background-color: #fff !important;">
        <div class="row">
<!--            <div class="col-lg-3"></div>-->
            <div class="col-lg-12">
                <div class="product-order">
                    <h3 class="title pt-0">your order details</h3>
                   <?php foreach ($data['order_detail'] as $od) {
                       $total[] = $od->qty * $od->selling_price; ?>
                    <div class="row product-order-detail">
                        <div class="col-3"><img src="<?php echo $od->url; ?>" alt="" class="img-fluid "></div>
                        <div class="col-3 order_detail">
                            <div>
                                <h4>product name</h4>
                                <h5><a href="product_detail?id=<?php echo $od->product_id; ?>"><?php  echo $od->product_name;?></a></h5></div>
                        </div>
                        <div class="col-3 order_detail">
                            <div>
                                <h4>quantity</h4>
                                <h5><?php echo $od->qty; ?></h5></div>
                        </div>
                        <div class="col-3 order_detail">
                            <div>
                                <h4>price</h4>
                                <h5>&#8377;<?php  echo $od->selling_price;?></h5></div>
                        </div>
                    </div>
                  <?php } ?>
                    <div class="final-total" style="border-top: 1px solid #dddddd;display:none">
                        <h3>total <span>&#8377;<?php echo array_sum($total); ?></span></h3></div>
                    <br>
                    <a href="/home" class="float-right btn btn-solid">continue shopping</a>
                </div>
            </div>
            <div class="col-lg-3"></div>
        </div>
    </div>
</section>
<!-- Section ends -->
