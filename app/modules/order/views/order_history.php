<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space" style="padding-top: 20px;padding-bottom: 20px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>order history</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo SITE_DOMAIN. str::HOME_ROUTE ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">order history</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->



<style>
@media (max-width: 650px)
{
 .cart-section .cart-table thead th:nth-last-child(-n+4) {
	 
    }
  .cart-section tbody tr td:nth-last-child(-n+4) {
	   
     }
	  .cart-section tbody tr td {
    min-width: 100px; }
}
</style>
<!--section start-->
<section class="cart-section order-history section-b-space" style="padding-top: 0;padding-bottom: 0;min-height: calc(100vh - 200px)">
    <div class="container" style="padding-top: 50px;background-color: #fff !important;padding-bottom: 50px;min-height: calc(100vh - 200px)">
        <div class="row">
            <div class="col-sm-12">
			 
                <table class="table cart-table table-responsive-xs">
                    <thead>
                    <tr class="table-head">
                        <th scope="col">Order</th>
                        <th scope="col">Order Date</th>
                        <th scope="col">Total</th>
                        <th scope="col">status</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php if(!empty($data['order_history'])) { ?>
                        <?php foreach($data['order_history'] as $od) { ?>
                  <tr onclick="redirect(<?php echo $od->id; ?>)">

                      <td><span class="dark-data" style="color:blue;cursor: pointer"> <?php echo $od->order_number; ?></span></td>
                        <td><span class="dark-data"><?php echo $od->created_at; ?></span></td>
                        <td>
                            <h4 class="dark-data">&#8377;<?php echo $od->order_total; ?></h4></td>
                        <td>
                            <?php if($od->status == 0){ ?>
                            <span class="dark-data"><?php echo ucfirst("order Received");?></span>
                            <?php } elseif($od->status == 1) { ?>
                            <span class="dark-data"><?php echo ucfirst("Packed");?></span>
                            <?php } elseif($od->status == 2) { ?>
                                <span class="dark-data"><?php echo ucfirst("Ready To Ship");?></span>
                            <?php } elseif($od->status == 3) { ?>
                                <span class="dark-data"><?php echo ucfirst("Dispatched");?></span>
                            <?php } elseif($od->status == 4) { ?>
                            <span class="dark-data" style="color:green"><?php echo ucfirst("Delivered");?></span>
                            <?php } elseif($od->status == 5) { ?>
                            <span class="dark-data" style="color:red"><?php echo ucfirst("Cancelled");?></span>
                            <?php } ?>

                        </td>

                    </tr>

                    <?php } ?>
                    <?php } else { ?>
                        <tr>

                            <td colspan="4"><span class="dark-data">No Order Found</span></td>

                        </tr>
                    <?php } ?>
                    </tbody>

                </table>
            
			</div>
        </div>
    </div>
</section>
<!--section end-->

<script>

    function redirect(id) {
        window.location = '/order_detail?id=' + id;
    }
</script>