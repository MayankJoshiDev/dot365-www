<!-- thank-you section start -->
<?php  use Razorpay\Api\Api;?>
<section class="section-b-space light-layout" style="padding-top: 20px;padding-bottom: 20px">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="success-text">
				<input id="Main_Id1" type="hidden" value="<?php echo $data[0]->main_id; ?>"/>
				  
                    <p><b>Order ID: <?php echo $data[0]->order_number; ?></b></p>
                    <p><b>Place On: <?php echo $data[0]->created_at; ?></b></p>
					<p><b>Payment Method: <?php echo $data[0]->payment_method; ?></b></p>
                    <?php if ($data[0]->status == 5) { ?>
                        <p>
                            <h3 class="text-danger">Order Cancelled</h3>
                        </p>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Section ends -->

<!-- order-detail section start -->
<section class="section-b-space" style="padding-top: 0px;padding-bottom: 0;min-height: calc(100vh - 200px);">
    <div class="container" style="background-color: #fff !important;padding-top: 30px;padding-bottom: 30px;min-height: calc(100vh - 200px);">
        <div class="row">
            <div class="col-lg-3">
                <div class="row order-success-sec">

                    <div class="col-sm-12">
                        <h4>shipping address</h4>
                        <ul class="order-detail">
                            <li><?php echo $data[0]->line1;  ?></li>
                            <li><?php echo $data[0]->line2;  ?></li>
                            <li><?php echo $data[0]->landmark;  ?></li>
                            <li><?php echo $data[0]->city;  ?></li>
                            <li><?php echo $data[0]->state;  ?>, <?php echo $data[0]->pincode;  ?></li>
                            <li>Contact No. <?php echo $data[0]->mobile_number; ?></li>
                        </ul>
                    </div>
				
                </div>

            </div>
            <div class="col-lg-9">
                <div class="product-order">
				<div class="row">
					<div class="col-6">  <h3 class="title pt-0">your order details </h3></div>
					<div class="col-6" style="width:100%"><span style="float:right"><button id="orderagain" class="btn btn-solid" onclick="orderag()">Order Agian</button></span></div>
				</div>
                  
					
                    <?php foreach ($data as $od) {
                        $total[] = $od->qty * $od->selling_price;
                    ?>
                        <div class="row product-order-detail">
                            <div class="col-2"><img src="<?php echo $od->url; ?>" alt="" class="img-fluid "></div>
                            <div class="col-2 order_detail">
                                <div>
                                    <h4>product name</h4>
                                    <h5 style="color: #0b0b0b;"><a href="product_detail?id=<?php echo $od->product_id; ?>"> <?php echo $od->product_name; ?></a></h5>
                                </div>
                            </div>
                            <div class="col-4 order_detail">
                                <div>
                                    <h4>quantity</h4>
                                    <div class="qty-box">
                                        <div class="input-group">
                                            <span onclick="changeProductQty(<?php echo  $od->product_id; ?>)" class="input-group-prepend"><button type="button" data-id="<?php echo  $od->product_id; ?>" class="btn quantity-left-minuso" data-type="minus" data-field=""><i class="ti-angle-left"></i></button></span>
                                            <input type="hidden" id="qtyCounter_<?php echo  $od->product_id; ?>" onchange="changeProductQty(<?php echo  $od->product_id; ?>)" class="form-control input-number_<?php echo  $od->product_id; ?>" value="1">
                                            <input type="number" id="showQty_<?php echo  $od->product_id; ?>" style="background: white; width: 100px;font-weight: bold" readonly name="quantity" class="form-control " value="<?php echo $od->qty ?>">
                                            <input type="hidden" id="defaultPacaketSize_<?php echo  $od->product_id; ?>" value="<?php echo $od->pacaket ?>">
                                            <input type="text" name="unit" class="form-control" value="<?php echo $od->unit ?>">
                                            <span class="input-group-prepend" onclick="changeProductQty(<?php echo  $od->product_id; ?>)">
                                                <button type="button" class="btn quantity-right-pluso" data-type="plus" data-id="<?php echo  $od->product_id; ?>" data-field="">
                                                    <i class="ti-angle-right"></i></button>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div>
                                    <h4>quantity</h4>
                                    <h5 style="color: #0b0b0b;"><?php echo $od->qty; ?></h5>
                                </div> -->
                            </div>
                            <div class="col-2 order_detail">
                                <div>
                                    <h4>price</h4>
                                    <h5 style="color: #0b0b0b;">&#8377;<?php echo $od->selling_price; ?></h5>
                                </div>
                            </div>
                            <?php if ($od->status <= 2) { ?>
                                <div class="col-2 order_detail" style="display:none">
                                    <div>
                                        <h4>Action</h4>
                                        <h5>
                                            <button onclick="updateitem(<?php echo $od->product_id; ?>,<?php echo $od->order_id; ?>)" class="btn btn-primary btn-outline-primary">Update</button>
                                            <!--<i class="ti-close" ></i>-->
                                            <a onclick="removeitems(<?php echo $od->product_id; ?>,<?php echo $od->order_id; ?>,<?php echo $od->selling_price * $od->qty; ?>)" class="btn btn-danger btn-outline-danger"><i class="ti-close"></i></a>
                                        </h5>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    <?php } ?>
                    <hr>
                    <div class="final-total">
                        <h3>total <span>&#8377;<?php echo array_sum($total); ?></span></h3>
				
                        <input type="hidden" id="billtotal" value="<?php echo array_sum($total); ?>">
						
                    </div>
					   <div id="hide1" style="text-align:right; width:100%;">
							 <hr>
								<span >	
								<!--	Packaging Charge : <span style="font-weight:bold">&#8377;<span id='pchrg'></span> </span><br/> -->
							Delivery Charge :<span style="font-weight:bold">&#8377;<span id='dchrg'></span> </span><br/>
								
										  
										
										
											
										
								
						
					</div>
					
					   <div  id="hide2" class="final-total"><hr>
                        <h3>Grand Total <span style="font-weight:bold">&#8377;<span id="gt22"></span></span></h3>
				
                        <input type="hidden" id="billtotal" value="<?php echo array_sum($total); ?>">
						
                    </div> <hr>	<br>
					
					
					<?php if($data[0]->status=='4' || $data[0]->payment_method=="Online") {?>
					 <div  style="text-align:right; width:100%;position:relative;margin-top:-120px;margin-left:-100px">
					 <img alt="paid" src="assets/images/paid.png" />
					 </div>
					
	<?php }?>
                </div>
                <?php if ($data[0]->status != 5) { ?>
			
                    <div class="col-md-12 mt-5">
                        <div class="accordion theme-accordion" id="accordionExample">
                            <div class="card">
                                <div class="card-header" id="headingOne">
                                    <h5 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">CANCEL ORDER</button>
                                    </h5>
                                </div>
                                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample" style="">
                                    <div class="card-block" style="padding: 18px">
                                        <p class="font-weight-bold" style="margin-bottom: 10px">Why Are you cancelling this Order?</p>
                                        <form>
                                            <div class="form-radio">
                                                <div class="radio radio-inline">
                                                    <label>
                                                        <input type="radio" name="radio" value="Wrong Product Ordered" id="wrongProduct" required>
                                                        <i class="helper"></i>
                                                        <p style="margin-bottom: 0;padding-top: 4px">Wrong Product Ordered</p>
                                                    </label>
                                                </div>
                                            </div>
                                            <hr style="margin: 5px 0">
                                            <div class="form-radio">
                                                <div class="radio radio-inline">
                                                    <label>
                                                        <input type="radio" name="radio" value="Don't want this anymore" id="dontWantAnyMore" required>
                                                        <i class="helper"></i>
                                                        <p style="margin-bottom: 0;padding-top: 4px">Don't want this anymore</p>
                                                    </label>
                                                </div>
                                            </div>
                                            <hr style="margin: 5px 0">
                                            <div class="form-radio">
                                                <div class="radio radio-inline">
                                                    <label>
                                                        <input type="radio" name="radio" value="Got better Deal" id="betterDeal" required>
                                                        <i class="helper"></i>
                                                        <p style="margin-bottom: 0;padding-top: 4px">Got better Deal</p>
                                                    </label>
                                                </div>
                                            </div>
                                            <hr style="margin: 5px 0">
                                            <div class="form-radio">
                                                <div class="radio radio-inline">
                                                    <label>
                                                        <input type="radio" name="radio" value="Other" id="other" required>
                                                        <i class="helper"></i>
                                                        <p style="margin-bottom: 0;padding-top: 4px">Other</p>
                                                    </label>
                                                </div>
                                            </div>
                                            <hr style="margin: 5px 0">
                                        </form>
                                        <textarea id="inputReason" class="mt-3 form-control max-textarea" maxlength="255" rows="4" placeholder="Help us improve our services, let us know what we can do for you?"></textarea>
                                        <div class="mt-3 text-right">
                                            <button class="btn btn-solid" onclick="orderCancel(<?php echo $data[0]->order_id; ?>)">SUBMIT</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
				<script>

	 var gt3 = 10;
		function dd(val2)
		{
			var xx=parseInt(<?php echo ($data[0]->main_id); ?>);
			//alert(xx+"--"+<?php echo $data[0]->order_number; ?>);
			if(xx>2000)//if(xx>6090)
			{
				
			
			// document.getElementById("pchrg").innerHTML="10";
			//alert(xx)
				 var dchrg2=val2;
				 var gt2=0;
				var x=10;
				// alert("called= total is="+val);
			 if(dchrg2>=1 && dchrg2<=500)
				 {
					 	document.getElementById("dchrg").innerHTML="100";
					 // $('#dchrg').html("&#8377;100");
					
					gt3=parseInt(x)+parseInt(val2)+100;
					//$('#gt22').html(""+gt3);
					document.getElementById("gt22").innerHTML=""+gt3;
				 }
				 else if(dchrg2>=501 && dchrg2<=999)
				 {
					//  $('#dchrg').html("&#8377;60");
					 document.getElementById("dchrg").innerHTML="60";
					  gt3=parseInt(x)+parseInt(val2)+60;
					 // $('#gt22').html(""+gt3);
					//  $('#newval').val(gt3);
					document.getElementById("gt22").innerHTML=""+gt3;
				 }
				  else if(dchrg2>=1000)
				 {
					 // $('#dchrg').html("Free");
					document.getElementById("dchrg").innerHTML="Free";
					   gt3=parseInt(x)+parseInt(val2)+0;
					//   $('#gt22').html(""+gt3);
					document.getElementById("gt22").innerHTML=""+gt3;
				 }
				 else
				 {
					// $('#dchrg').html("&#8377;100");
					document.getElementById("dchrg").innerHTML="100";
					 gt3=parseInt(x)+parseInt(val2)+100;
					//$('#gt22').html(""+gt3);
					document.getElementById("gt22").innerHTML=""+gt3;
				 }
				 	document.getElementById("dchrg").innerHTML="<?php echo $data[0]->delivery ?>";
					gt3=parseInt(val2)+<?php echo $data[0]->delivery ?>;
					document.getElementById("gt22").innerHTML=""+gt3;
					grandtotal=gt3;
			}
			else
			{
				document.getElementById("hide1").style.display="none";
				document.getElementById("hide2").style.display="none";
			}
		 }
		
			dd("<?php echo array_sum($total); ?>");		
			
		function orderCancel() {

        var radioValue = $("input[name='radio']:checked").val();
        $.ajax({
            url: "/ordercancel",
            type: "POST",
            data: {
                order_status: "cancel",
                cancel_title: radioValue,
                cancel_reason: $("#inputReason").val(),
                order_id: id
            },
            success: function(result) {
                var parsedJson = $.parseJSON(result);
                location.reload();


            }
        });
    }
    function orderag() {

        var orderid = $("#Main_Id1").val();
		
        $.ajax({
            url: "/OrderAgain",
            type: "POST",
            data: {
                order_id: orderid
            },
            success: function(result) {
				toastFunction("Product Added to Cart");
                var parsedJson = $.parseJSON(result);
                location.reload();


            }
        });
    }

    function removeitems(pid, oid, ptotal) {
        var lgt = $(".product-order-detail").length;
        if (lgt == 1) {
            toastFunction("Only 1 Item in Order, Please Cancel the Order");
            /*alert();*/
        } else {
            if (confirm("Are you sure You Want to Delete?")) {
                var total = $("#billtotal").val();
                var newtotal = total - ptotal;
                $.ajax({
                    url: "/order_item_update",
                    type: "POST",
                    data: {
                        order_id: oid,
                        product_id: pid,
                        ntotal: newtotal,
                        action: "Delete"
                    },
                    success: function(result) {
                        var parsedJson = $.parseJSON(result);
                        if (parsedJson.status == true) {
                            toastFunction("Item Removed From Order");
                            location.reload();
                        }

                    }
                });
            }
            return false;

        }
    }
</script>

<Script>
    function changeProductQty(id) {
        var counter = $('#qtyCounter_' + id).val();
        var pacaketSize = $('#defaultPacaketSize_' + id).val();
        var newQty = counter * pacaketSize;
        $('#showQty_' + id).val(newQty);

    }


    function updateitem(id, oid) {
        if (confirm("Are you sure You Want to Update?")) {
        var pqty = $('#showQty_' + id).val();

        $.ajax({
            type: "POST",
            url: "/order_item_update",
            data: {
                product_id: id,
                qty: pqty,
                order_id: oid
            },
            success: function(response) {
                var parsedJson = $.parseJSON(response);
                if (parsedJson.status == true) {
                    toastFunction("Item Qty Updated ");
                    setTimeout(function() {
                       
                            window.location.reload(1);
                        
                    }, 2000);
                    
                }
            }
        });
    }
    return false;
    }
</Script>
				<div name="newonlinepay" style="text-align:right; width:100%;">
				<br>
					 
								
									<?php if($data[0]->payment_method=="COD" && $data[0]->status<4) {?>
											 <?php 
														 require 'config.php';
														 require 'razorpay-php/Razorpay.php';
														
									$api = new Api($keyId, $keySecret);
									$orderData = [
										'receipt'         => $data[0]->order_number,
										'amount'          =>  $data[0]->order_total * 100,
										'currency'        => 'INR',
										'payment_capture' => 1
									];
									$razorpayOrder = $api->order->create($orderData);
									$razorpayOrderId = $razorpayOrder['id'];
									$_SESSION['razorpay_order_id'] = $razorpayOrderId;
									$displayAmount = $amount = $orderData['amount'];
									if ($displayCurrency !== 'INR') {
										$url = "https://api.fixer.io/latest?symbols=$displayCurrency&base=INR";
										$exchange = json_decode(file_get_contents($url), true);
										$displayAmount = $exchange['rates'][$displayCurrency] * $amount / 100;
									}
									$data = [
										"key"               => $keyId,
										"amount"            => $amount,
										"currency"          => "INR",
										"name"              => 'Dot365',
										"description"       => 'Payment For'.$data[0]->order_number.' Website',
										"image"             => "https://admin.dot365.in/assets/img/Dot365Logo.jpeg",
										"prefill"           => [
										"name"              => '',
										"email"             => '',
										"contact"           => $data[0]->mobile_number,
										],
										"notes"             => [
										"address"           => $data[0]->city,
										"cust_odid" => $data[0]->order_number,
										],
										"theme"             => [
										"color"             => "#3b4650"
										],
										"order_id"          => $razorpayOrderId,
									];

									if ($displayCurrency !== 'INR')
									{
										$data['display_currency']  = $displayCurrency;
										$data['display_amount']    = $displayAmount;
									}

									$json = json_encode($data);


									
									?>
							<button id="rzp-button1" class="btn btn-solid">PayOnline</button>
									<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
							<form name='razorpayform' action="verify" method="POST">
								<input type="hidden" name="razorpay_payment_id" id="razorpay_payment_id">
								<input type="hidden" name="razorpay_signature"  id="razorpay_signature" >
							</form>
							<form id='payonlinesuccess' method="POST" action="/payonline2" >
								<input type="hidden" name="order_id" id="order_id" >
								<input type="hidden" name="payment_id" id="payment_id">
							</form>
							<script>
							var options = <?php echo $json?>;
							options.handler = function (response){
								document.getElementById('razorpay_payment_id').value = response.razorpay_payment_id;
								document.getElementById('razorpay_signature').value = response.razorpay_signature;
								//document.razorpayform.submit();
								alert("Payment Done Successfully ");
								if(response.razorpay_payment_id==null)
								{
									alert("Payment Failed 123 : "+response.razorpay_payment_id)
									//document.razorpayform.submit();
									
									
								}
								else
								{
									alert("Payment Done Successfully : "+response.razorpay_payment_id);
									 //document.getElementById("gateway_payment_id").value=response.razorpay_payment_id;
									 document.getElementById("order_id").value=document.getElementById("Main_Id1").value;//response.razorpay_payment_id;
									 document.getElementById("payment_id").value=response.razorpay_payment_id;
									 document.getElementById("payonlinesuccess").submit();
									// document.getElementById("checkout-selection2").submit();
									
									
								}
								
								//pay_GDLuehBynyjX01
							};
							options.theme.image_padding = false;
							options.modal = {
								ondismiss: function() {
									alert("Payment Canceld !!!");
									 // document.getElementById("gateway_payment_id").value="demo123";
									 document.getElementById("order_id").value=document.getElementById("Main_Id1").value;//response.razorpay_payment_id;
									 document.getElementById("payment_id").value="demo123";
									 document.getElementById("payonlinesuccess").submit();
									//console.log("This code runs when the popup is closed");
								},
								escape: true,
								backdropclose: false
							};
							var rzp = new Razorpay(options);
							document.getElementById('rzp-button1').onclick = function(e){
								rzp.open();
								e.preventDefault();
							}
							function mytest22()
							{
								 //document.getElementById("checkout-selection2").submit();
							}
							</script>
					
								<?php } ?>
					</div>
      
			 </div>


        </div>
    </div>
</section>
<!-- Section ends -->

	

	      
			