<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space" style="padding-top: 20px;padding-bottom: 20px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>contact</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo SITE_DOMAIN. str::HOME_ROUTE ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Contact</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->


<!--section start-->
<section class="contact-page section-b-space pb-0" style="padding-top: 0px">
    <div class="container" style="padding-top: 30px;background-color: #fff !important;">
        <div class="row section-b-space">
            <div class="col-lg-7 map">
                <form action="/contact_message" method="post" class="theme-form">
                    <div class="form-row">
                        <div class="col-md-12">
                            <label for="name">First Name</label>
                            <input name="inputName" type="text" class="form-control" id="name" placeholder="Enter Your name" required="">
                        </div>

                        <div class="col-md-12">
                            <label for="review">Phone number</label>
                            <input type="text" name="inputNumber" class="form-control"  placeholder="Enter your number" required="">
                        </div>
                        <div class="col-md-12">
                            <label for="email">Email</label>
                            <input type="text" class="form-control" name="inputEmail"  placeholder="Email" required="">
                        </div>
                        <div class="col-md-12">
                            <label for="review">Write Your Message</label>
                            <textarea name="inputMessage" class="form-control" placeholder="Write Your Message" id="exampleFormControlTextarea1" rows="6"></textarea>
                        </div>
                        <div class="col-md-12">
                            <button class="btn btn-solid" type="submit">Send Your Message</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-lg-5">
                <div class="contact-right">
                    <ul>
                        <li>
                            <div class="contact-icon"><i class="fa fa-phone" aria-hidden="true"></i>
                                <h6>Contact Us</h6></div>
                            <div class="media-body">
                                <p>+91 93211 58986</p>
                            </div>
                        </li>
                        <!--<li>
                            <div class="contact-icon"><i class="fa fa-map-marker" aria-hidden="true"></i>
                                <h6>Address</h6></div>
                            <div class="media-body">
                                <p>ABC Complex,Near xyz, New York</p>
                                <p>USA 123456</p>
                            </div>
                        </li>-->
                        <li>
                            <div class="contact-icon"><i class="fa fa-envelope-o" aria-hidden="true"></i>
                                <h6>EMAIL</h6></div>
                            <div class="media-body">
                                <p>info@dot365.in</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">

        </div>
    </div>
</section>
<!--Section ends-->