<!-- breadcrumb start -->
<section class="breadcrumb-section section-b-space" style="padding-bottom: 20px;padding-top: 20px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="page-title">
                    <h2>About us</h2>
                </div>
            </div>
            <div class="col-12">
                <nav aria-label="breadcrumb" class="theme-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?php echo SITE_DOMAIN. str::HOME_ROUTE ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">About us</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</section>
<!-- breadcrumb End -->


<!-- about section start -->
<section class="about-page section-b-space" style="padding-top: 0;padding-bottom: 0;min-height: calc(100vh - 200px)">
    <div class="container" style="padding-top: 50px;padding-bottom: 50px;background-color: #fff !important;min-height: calc(100vh - 200px)">
        <div class="row">
            <div class="col-xl-12">
               <p>Dot 365 is a one-stop destination for all things essential for your shops and restaurants. We cater to your needs in both quality and quantity. Only the best of products are sold by us and hence we have loyal customers who shop with us for all their everyday needs like bags, cups gives, bowls, cleaning essentials, etc.</p>
                <p>We understand the value of your time and hence we always strive to deliver on time for smooth running for your business. Our timely delivery is something that we are wildly known for so you can keep shopping with us worry-free.</p>
            </div>
        </div>
    </div>
</section>
<!-- about section end -->