<!-- slider section start -->
<div class="container p-0 home-banner layout-2-menu shadow-home">
    
    <div class="slide-1 home-slider">

        <?php foreach ($dashboard['banner'] as $key => $val) {  ?>
        <div>
            <div class="home text-center p-center">
                <img src="<?php echo $val->banner; ?>" class="bg-img " alt="">
                <div class="container">
                    <div class="row">
                        <div class="col">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>

    </div>
</div>
<!-- slider section end-->

<!-- category section start -->
<section class="category category-classic bg-grey" style="padding: 0px">
    <div class="container" style="padding: 50px 0;">
        <div class="slide-6 no-arrow">

            <?php foreach ($category as $key => $val) {  ?>
                <?php if(!empty($val->sub)) { ?>
            <div>
                <div class="category-wrapper">
                    <div class="img-block">


                        <a href="<?php echo SITE_DOMAIN. str::SUB_CATEGORY_ROUTE ?>?id=<?php echo $val->id; ?>&name=<?php echo $val->name; ?>">
                            <img src="<?php echo $val->category_image; ?>" alt="" class=" img-fluid">
                        </a>



                    </div>
                    <div class="category-title">
                        <a href="#"><h5><?php echo $val->name; ?></h5></a>
                    </div>
                </div>
            </div>
            <?php } else { ?>
                    <div>
                        <div class="category-wrapper">
                            <div class="img-block">


                                <a href="<?php echo SITE_DOMAIN. str::PRODUCT_LIST_ROUTE ?>?id=<?php echo $val->id; ?>&name=<?php echo $val->name; ?>">
                                    <img src="<?php echo $val->category_image; ?>" alt="" class=" img-fluid">
                                </a>



                            </div>
                            <div class="category-title">
                                <a href="#"><h5><?php echo $val->name; ?></h5></a>
                            </div>
                        </div>
                    </div>

            <?php }  }?>
        </div>
    </div>
</section>
<!-- category section end -->

<!-- tab section start -->
<section class="section-b-space tab-layout1 ratio_square" style="padding: 0px;background-color: unset">
    <div class="theme-tab">
        <div class="drop-shadow">
            <div class="left-shadow">
                <img src="../assets/images/left.png" alt="" class=" img-fluid">
            </div>
            <div class="right-shadow">
                <img src="../assets/images/right.png" alt="" class=" img-fluid">
            </div>
        </div>
        <div class="tab-content-cls">
            <div id="tab-1" class="tab-content active default" >
                <div class="container">
                    <h2 class="title pt-0" style="margin-left: 15px">Regular Products</h2>
                    <div class="row border-row1 grid-5">
                        <?php foreach ($dashboard['trending_products'] as $key => $val) { ?>
                        <div class="box-5">
						
                            <div class="product-box product-full">
								 <div class="float123"  data-toggle="modal" data-target="#addtocart" onclick="addtocartall(<?php echo $val->id; ?>,<?php  if(isset($chat_user)) { echo $chat_user['uid']; }; ?>)">
									
									<i class="fa fa-plus my-float"  ></i>
									
                           
								</div>
                                <div class="img-block">
                                    <a href="<?php echo SITE_DOMAIN. str::PRODUCT_DETAIL_ROUTE ?>?id=<?php echo $val->id; ?>">
                                        <img src="<?php echo $val->url; ?>" class=" img-fluid bg-img" alt=""></a>

                                </div>
                                <div class="product-info">
                                    <a href="<?php echo SITE_DOMAIN. str::PRODUCT_DETAIL_ROUTE ?>?id=<?php echo $val->id; ?>"><h6><?php echo  $val->name; ?></h6></a>
                                    <h5>&#8377;<?php echo $val->mrp; ?></h5>
									
									     <input type="number" id="showQty_<?php echo $val->id; ?>" style="display:none;background: white; width: 100px;font-weight: bold"
                                           readonly name="quantity"  type="hidden"
                                           value="<?php echo $val->packet_size ?>">
									<!-- New Section started -->
									<!-- <link rel="stylesheet" href="http://localhost/assets/css/color11.css">
									<link rel="stylesheet" href="http://localhost/assets/css/bootstrap.csshttp://localhost/assets/css/bootstrap.css">
									-->
							
								
							<!-- New Section Closed -->
                                </div>

                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- tab section start -->

<section class="slider-section no-border-product" style="padding: 0px">
    <div class="container" style="padding: 50px 15px;">
        <div class="title-basic">
            <h2 class="title">Best Seller</h2>
            <div class="timer-flash">
                <div class="clock"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12 p-0 ratio_asos">
                <div class="slider-6 no-arrow">
                   <?php foreach($dashboard['bestseller_product'] as $key => $val) { ?>
                    <div>
                        <div class="product-box">
						 <div class="float123"  data-toggle="modal" data-target="#addtocart" onclick="addtocartall(<?php echo $val->id; ?>,<?php  if(isset($chat_user)) { echo $chat_user['uid']; }; ?>)">
									
									<i class="fa fa-plus my-float" style="color:#fff" ></i>
									
                           
								</div>
                            <div class="img-block">
                                <a href="<?php echo SITE_DOMAIN. str::PRODUCT_DETAIL_ROUTE ?>?id=<?php echo $val->id; ?>"><img src="<?php echo $val->url; ?>" class=" img-fluid bg-img" alt=""></a>


                            </div>
                            <div class="product-info">
							
                                <a href="<?php echo SITE_DOMAIN. str::PRODUCT_DETAIL_ROUTE ?>?id=<?php echo $val->id; ?>"><h6><?php echo $val->name; ?></h6></a>
                                <h5>&#8377;<?php echo $val->mrp; ?></h5>
									     <input type="number" id="showQty_<?php echo $val->id; ?>" style="display:none;background: white; width: 100px;font-weight: bold"
                                           readonly name="quantity"  type="hidden"
                                           value="<?php echo $val->packet_size ?>">
												
							
                            </div>

                        </div>
                    </div>
                   <?php } ?>
            </div>
        </div>
    </div>
</section>
