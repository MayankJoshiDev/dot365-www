<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class HomeController extends BackendController
{
    static $inst;

    static function inst()
    {
        if (!self::$inst) {
            self::$inst = new self();
        }
        return self::$inst;
    }

    function index()
    {
        $data['dashboard'] = HomeModel::dashboardProductsFx();
        $data['category'] = CategoryModel::getCategoryListFx();


        $this->loadPage('home/home_view',$data);
    }

    function subCategoryList()
    {

        $data['productbycat'] = CategoryModel::getSubCategoryFx($_GET['id']);

        $this->loadPage('product/sub_category_list',$data);

    }
	function test9876()
	{	$orderNumber="demo123456";
	  $user = Session::inst()->getData('ADMIN');
            $input = (object)array('customer_id' => $user[0]->id);
            $r = customerModel::customerProfileModelFx($input);
           // $this->loadPage('user/profile', $r);
		
		//SMS::sendOTP("8898406415","0101");
		$contents="Thanks your order has been received. Your Order id is " . $orderNumber . ". You can expect delivery by 24 hours. You can track and manage your orders on Dot365 App. Help line number: +917045873218" . PHP_EOL . "Follow us on Instagram: https://www.instagram.com/dot365official/";
		//SMS::sendtestsms("8898406415","9090");
		 $this->loadPage('home/about_usss',$r);
	}
    function about()
    {

        $this->loadPage('home/about_us');

    }

    function contactUs()
    {


        $this->loadPage('home/contact_us');

    }
    
    function contactMessage()
    {
        require_once ROOT_DIR.'/app/thirdparty/'.'PHPMailer/src/PHPMailer.php';
        require_once ROOT_DIR.'/app/thirdparty/'.'PHPMailer/src/SMTP.php';
    
        $mail = new PHPMailer(true);
        
    
        ob_start();
        $mail->SMTPDebug = 1;                                 // Enable verbose debug output
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->Port = 465;                                    // TCP port to connect to
        $mail->Username = 'noreplay.swiftride@gmail.com';                 // SMTP username
        $mail->Password = 'noreply@swiftride2020';                           // SMTP password
//Recipients
    
        $mail->setFrom("noreply.swiftride@gmail.com", 'Dot365');
        $mail->addAddress('info@dot365.in');
        
//Content
        $message = "Name: ".$_POST['inputName'].PHP_EOL;
        $message .= "Number: ".$_POST['inputNumber'].PHP_EOL;
        $message .= "Email: ".$_POST['inputEmail'].PHP_EOL;
        $message .= "Message: ".$_POST['inputMessage'];
        
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Contact Form: Dot365.in website | '.$_POST['name'];
        $mail->Body = $message;
        $mail->AltBody = $message;
        $mail->send();
        Response::redirect('/contact_us');
    }

}