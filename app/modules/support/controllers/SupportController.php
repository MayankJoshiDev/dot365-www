<?php

/**
 * Created by PhpStorm.
 * User: OM
 * Date: 06-Feb-19
 * Time: 1:39 PM
 */
class SupportController extends BackendController
{
    static $inst;

    static function inst()
    {
        if (!self::$inst) {
            self::$inst = new self();
        }
        return self::$inst;
    }


    function supportList()
    {

        $input = Req::json();
        $order = new SupportTbl();

        $res['data']['support_chat'] = supportModel::supportListFx($input);

        $res['status'] = true;

        Response::sendJson($res);

    }


    function startNewSupport()
    {
        $input = Req::json();
        $support = new SupportTbl();
        /*params*/
        $customerId = $input->customer_id;
        $supportId = $input->support_id;
        $title = $input->support_title;
        /*params*/

        $support->setCustomerId($customerId);
        $support->setTitle($title);
        if ($supportId != 0) {
            $support->setId($supportId);
        }
        $support->setCreatedAt('NOW()');
        $support->setTicketNumber(TICKET::number());
        $supportId = $support->flush();

        $res['status'] = true;
        $res['data']['support_id'] = $supportId;

        Response::sendJson($res);

    }


    function viewSupportDetail()
    {

        $input = Req::json();
        $support = new SupportMessageTbl();

        $supportId = $input->support_id;
        $res['status'] = true;
        $res['data']['support_id'] = $supportId;
        $res['data']['support_message'] = supportModel::supportDetailFx($supportId);

        Response::sendJson($res);

    }
    
    function refreshChatDetail()
    {
        $input = Req::json();
        $support = new SupportMessageTbl();
        /*Params*/
        $customerId = $input->customer_id;
        $supportId = $input->supportid;
        $lastMessageId = $input->last_message_id;
        /*Params*/
    
        $res['status'] = true;
        $res['data']['support_id'] = $supportId;
        $res['data']['support_message'] = supportModel::getNewSupportMessages($supportId,$lastMessageId);
        Response::sendJson($res);
        
        
    }

    function sendSupportMessage()
    {

        $input = Req::json();


        $support = new SupportMessageTbl();

        $supportId = $input->support_id;
        $customerId = $input->customer_id;
        $flag = $input->flag;

        if ($flag == 'image') {

            $fileName = $customerId . '_' . time() . '.jpg';
            if (!is_dir(ROOT_DIR . '/uploads/support')) {
                mkdir(ROOT_DIR . '/uploads/support', 0777, true);
            }
            $photo = move_uploaded_file($_FILES['message']['tmp_name'], ROOT_DIR . '/uploads/support/' . $fileName);

            if ($photo) {

                $supportMsg = new SupportMessageTbl();
                $supportMsg->setCustomerId($customerId);
                $supportMsg->setFlag($flag);
                $supportMsg->setMessage($fileName);
                $supportMsg->setSupportId($supportId);
                $supportMsg->setCreatedAt('NOW()');
                $supportMgsId = $supportMsg->flush();
            } else {
                $res['status'] = false;
            }

        } else {
            $supportMessage = $input->message;
            $supportMsg = new SupportMessageTbl();
            $supportMsg->setCustomerId($customerId);
            $supportMsg->setFlag($flag);
            $supportMsg->setMessage($supportMessage);
            $supportMsg->setSupportId($supportId);
            $supportMsg->setCreatedAt('NOW()');
            $supportMgsId = $supportMsg->flush();
        }
        $res['status'] = true;
        $res['data']['support_id'] = $supportId;
        $res['data']['support_message'] = supportModel::supportDetailFx($supportId);
        Response::sendJson($res);
    }
}