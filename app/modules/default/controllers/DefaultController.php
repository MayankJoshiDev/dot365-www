<?php

class DefaultController extends BackendController
{
    static $inst;
    static function inst()
    {
        if(!self::$inst) { self::$inst = new self();}
        return self::$inst;
    }

    function index()
    {

        $data['dashboard'] = HomeModel::dashboardProductsFx();
        $data['category'] = HomeModel::getPrimaryCategoryOnlyFx();

        $this->loadPage('home/home_view',$data);


    }

    function error404()
    {

        $this->loadPage('default/default_error404');
    }
}