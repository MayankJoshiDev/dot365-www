<?php

use mysql_xdevapi\DocResult;

/**
 * Created by PhpStorm.
 * User: OM
 * Date: 06-Feb-19
 * Time: 1:39 PM
 */
class CartController extends BackendController
{
    static $inst;

    static function inst()
    {
        if (!self::$inst) {
            self::$inst = new self();
        }
        return self::$inst;
    }


    function addToCart(){

        $input = Req::json();


        $input->product = array(["product_id" => $input->product_id,"qty" => $input->qty]);

        $customer = new CustomerCartTbl();

        if($input->action == 'add'){
            $res = cartModel::customerAddCartFx($input);
        }elseif($input->action == 'edit'){
            cartModel::customerEditCartFx($input);
            $res = cartModel::viewCustomerCartFx($input);
        }elseif($input->action == 'delete'){
           cartModel::customerCartDeleteFx($input);
            $res = cartModel::viewCustomerCartFx($input);
        }


        Response::sendJson($res);
    }
    
    function viewCustomerCart()
    {
        $input = Req::json();
        $customer = new CustomerCartTbl();
        $res = cartModel::viewCustomerCartFx($input);
        Response::sendJson($res);
    }

    function addToFavourite()
    {
        $customer = new CustomerCartTbl();
        /*Params*/
        $input = Req::json();
        /*Params*/
        $res = cartModel::addFavouriteFx($input);
        Response::sendJson($res);
    }

    function getFavcount()
    {
        if (Session::inst()->getIsLogin()) {
            $user = Session::inst()->getData('ADMIN');
            $res['data'] = cartModel::customerFavcountFX($user[0]->id);
            $res['status'] = true;
            Response::sendJson($res);

        }
        else
        {
            $res['status'] = false;
            Response::sendJson($res);
        }
    }

    function getCartcount()
    {
        if (Session::inst()->getIsLogin()) {
            $user = Session::inst()->getData('ADMIN');
            $res['data'] = cartModel::customercountFX($user[0]->id);
            $res['status'] = true;
            Response::sendJson($res);

        }
        else
        {
            $res['status'] = false;
            Response::sendJson($res);
        }
    }

    function deletecartitem()
    {
        $input = Req::json();
        $res = cartModel::customerCartDeleteFx($input);
        Response::sendJson($res);
    }
}
