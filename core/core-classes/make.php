<?php

class make
{
    private $proxyContent;
    private function loadDb()
    {
       new cliDb();
    }

    function __construct()
    {
        $this->loadDb();
    }

    function datamodels()
    {

        $tblList = cliDb::query("SHOW TABLES");

        $this->proxyContent = '<?php'.PHP_EOL;
        foreach ($tblList as $k => $v) {
            foreach ($v as $k1 => $v1) {
                $this->makeModeFor($v1);
            }
        }

        $proxyFile = ROOT_DIR.'/app/data-models/proxies/data_proxy.php';
        if(!is_dir(ROOT_DIR.'/app/data-models/proxies')){
            mkdir(ROOT_DIR.'/app/data-models/proxies',755);
        }
        file_put_contents($proxyFile,$this->proxyContent,1);
        echo "done";

    }

    private function makeModeFor($tbl)
    {

        $pClassName = $tbl.'TblProxy';
        $pClassName = str_replace('_',' ',$pClassName);
        $pClassName = str_replace(' ','',ucwords($pClassName));
        $proxyClass = $this->proxyClassContents($tbl,$pClassName);

        /*  $proxyFile = APPDIR.'/application/data-models/proxies/'.$pClassName.'.php';
          file_put_contents($proxyFile,$proxyClass);*/

        $dTblClassName = $tbl.'Tbl';
        $dTblClassName = str_replace('_',' ',$dTblClassName);
        $dTblClassName = str_replace(' ','',ucwords($dTblClassName));

        $dataTblFile = ROOT_DIR.'/app/data-models/'.$dTblClassName.'.php';
        if(!file_exists($dataTblFile)){
            $dTblClass = $this->dTblClassContents($tbl,$dTblClassName);
            file_put_contents($dataTblFile,$dTblClass);
        }
    }

    private function proxyClassContents($tbl,$cName)
    {
        $str = '';
        $str .= PHP_EOL.'class '.$cName.' extends SelwynDataModels';
        $str .= ' { '.PHP_EOL;
        /*Now fetch all the row from table make private variables*/
        $fields = cliDb::query("SHOW COLUMNS FROM ".$tbl);

        $getterMethods = "";
        $setterMethods = "";
        $str .= " const TABLE_NAME = '".$tbl."' ;";
        $aiColumn = "";
        $priArray = array();
        foreach ($fields as $row)
        {
            $str .= ' const DF_'.strtoupper($row->Field)." = '".$row->Field."';".PHP_EOL;
            if($row->Extra == 'auto_increment'){
                $aiColumn = $row->Field;
            }

            $str .= ' private $'.$row->Field.';'.PHP_EOL;

            $methodName = str_replace('_',' ',$row->Field);
            $methodName = str_replace(' ','',ucwords($methodName));

            if($row->Key == 'PRI'){
                array_push($priArray,"'".$row->Field."'");
            }

            $getterMethods .= ' public function get'.$methodName.'(){'.PHP_EOL;
            $getterMethods .= ' return $this->'.$row->Field.';'.PHP_EOL;
            $getterMethods .= ' }'.PHP_EOL;

            $setterMethods .= ' public function set'.$methodName.'($'.$row->Field.'){'.PHP_EOL;
            $setterMethods .= ' $this->'.$row->Field.' = $'.$row->Field.';'.PHP_EOL;
            $setterMethods .= ' }'.PHP_EOL;
        }

        $str .= " const AI_COL = '".$aiColumn."';".PHP_EOL;
        /*Write PRI in private array*/
        $str .= ' private $PRI = array('.implode(',',$priArray).');'.PHP_EOL;
        $str .= ' function flush($primaryKeyArray = "")'.PHP_EOL.'{ if($primaryKeyArray == ""){'.PHP_EOL.'$primaryKeyArray = $this->PRI;} '.PHP_EOL.'return parent::flush($this->PRI);}';

        /*Now Make All Getter Setter Methods*/
        $str .= $getterMethods;
        $str .= $setterMethods;
        $str.= ' function getDataSet(){ return get_object_vars($this);}';


        $str .= ' }';
        $this->proxyContent .= $str;
        //return $str;
    }

    private function dTblClassContents($tbl,$cName)
    {
        $str = '<?php';
        $str .= PHP_EOL.'class '.$cName.' extends '.$cName.'Proxy ';
        $str .= PHP_EOL.'{';
        /*Now fetch all the row from table make private variables*/
        $fields = cliDb::query("SHOW COLUMNS FROM ".$tbl);
        $str .= PHP_EOL.PHP_TAB.'/* You Can Override any Method of '.$cName.'Proxy Class from this class*/';

        foreach ($fields as $row) {
            //$str .= PHP_EOL.PHP_TAB.'const DF_'.strtoupper($row->Field)." = '".$row->Field."';";
        }

        $str .= PHP_EOL.'}';
        return $str;
    }
}

class cliDb extends SelwynDatabase
{
    function __construct()
    {
        $dbName = App::inst()->getConfig('default_database');
        $inst = new SelwynDatabase();

    }
}