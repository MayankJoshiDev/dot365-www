<?php

/**
 * Created by PhpStorm.
 * User: CPU011
 * Date: 3/15/2018
 * Time: 12:16 AM
 */
class Session
{
    static $inst;
    
    static function inst()
    {
        if (!self::$inst) {
            session_name('App');
            session_start();
            self::$inst = new self();
        }
        return self::$inst;
    }
    
    function setData($key, $data)
    {
        $_SESSION[$key] = $data;
        return $this;
    }
    
    function getData($key)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        } else {
            return FALSE;
        }
    }
    
    function setIsLogIn($flag)
    {
        $_SESSION['IS_LOGIN'] = $flag;
    }
    
    function getIsLogin()
    {
        if (isset($_SESSION['IS_LOGIN'])) {
            return $_SESSION['IS_LOGIN'];
        } else {
            return FALSE;
        }
    }
    
    function setAdmin($obj)
    {
        $_SESSION['ADMIN'] = $obj;
    }
    
    function getAdmin()
    {
        if (isset($_SESSION['ADMIN'])) {
            return $_SESSION['ADMIN'];
        } else {
            return FALSE;
        }
    }
    
    function logout()
    {
        session_destroy();
    }
    
    function setMeta($name, $value)
    {
        $_SESSION['meta'][$name] = $value;
    }
    
    function getMeta($name)
    {
        if(isset($_SESSION['meta'][$name])){
            return $_SESSION['meta'][$name];
        }else{
            return false;
        }
    }
    
}