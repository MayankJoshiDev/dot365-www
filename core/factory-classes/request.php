<?php

class Req
{
    static $inst;
    static function inst()
    {
        if(self::$inst){
            return self::$inst;
        }else{
            self::$inst = new self();
            return self::$inst;
        }
    }

    static function header()
    {

    }

    static function formData()
    {
        return json_decode(json_encode($_POST));
    }

    static function json()
    {
       if($_POST){
           return json_decode(json_encode($_POST));
       }else{
           $input =  file_get_contents('php://input');
           return json_decode($input);
       }

    }
}

class ReqValidator
{
    static $inst;
    static function inst()
    {
        if(self::$inst){
            return self::$inst;
        }else{
            self::$inst = new self();
            return self::$inst;
        }
    }

    function abc()
    {
        $login = new stdClass();
        $login->username->required = true;
        $login->username->datatype = "string";
        $login->username->min_len = 3;
        $login->username->max_len = 255;
    }
}

?>