<?php
/**
 * Created by PhpStorm.
 * User: CPU011
 * Date: 4/23/2018
 * Time: 11:23 PM
 */
class AppException
{
    function __construct()
    {

        return $this;
    }

    private $code;
    private $errorMsg;
    private $severity;

    private $doLog = true;
    private $willDie = false;

    function code($code)
    {
        $this->code = $code;
        return $this;
    }

    function errorMsg($messageStr)
    {
        $this->errorMsg = $messageStr;
        return $this;
    }

    function severity($severity)
    {
        $this->severity = $severity;
        return $this;
    }

    function writeLog($bool)
    {
        $this->doLog = $bool;
        return $this;
    }

    function killApp()
    {
        $this->willDie = TRUE;
        echo "<pre>";
        print_r($this);
        echo "</pre>";
        exit();
        return $this;
    }



}
