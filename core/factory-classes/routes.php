<?php
/**
 * Created by PhpStorm.
 * User: CPU011
 * Date: 3/15/2018
 * Time: 12:16 AM
 */

class Route
{

    static function get($uri, $callback)
    {
        $routeMatchedFlag = App::inst()->getFlag(fstr::FLAG_ROUTE_MATCHED);
        if (!isset($_SERVER['REQUEST_METHOD']) || $_SERVER['REQUEST_METHOD'] !== "GET" || $routeMatchedFlag) {
            return false;
        }
        if (isset($_SERVER['REQUEST_URI'])) {

            $pathInfo = str_replace(str_replace($_SERVER['HTTP_HOST'], '', SITE_DOMAIN), '', $_SERVER['REQUEST_URI']);
            $pathArray = explode('?',$pathInfo);
            $pathInfo = $pathArray[0];

            if ($uri == $pathInfo) {

                App::inst()->setFlag(fstr::FLAG_ROUTE_MATCHED, true);
                $callback();
            } else {

            }
        } else {

            $defaultController = App::inst()->getConfig('default_controller');
            $str = ucfirst($defaultController) . 'Controller';
            if (!class_exists($str)) {
                throw new Exception('Default Controller Does not exist', 1003);
            }
            $class = new $str();
            if (!is_a($class, 'SelwynController')) {
                throw new Exception('Default Controller Is not Child of SelwynController Class', 1003);
            }
            if (method_exists($class, 'index')) {
                App::inst()->setFlag(fstr::FLAG_ROUTE_MATCHED, true);
                $class->index();
            } else {
                throw new Exception('Index Method Not Found on Default Class', 1003);
            }
        }


    }


    static function post($uri, $callback)
    {


        $routeMatchedFlag = App::inst()->getFlag(fstr::FLAG_ROUTE_MATCHED);
        if (!isset($_SERVER['REQUEST_METHOD']) || $_SERVER['REQUEST_METHOD'] !== "POST" || $routeMatchedFlag) {
            return false;
        }




        $pathInfo = str_replace(str_replace($_SERVER['HTTP_HOST'], '', SITE_DOMAIN), '', $_SERVER['REQUEST_URI']);

        if ($uri == $pathInfo) {
            App::inst()->setFlag(fstr::FLAG_ROUTE_MATCHED, true);
            /*First Decode the Header*/
            $header = $_SERVER['HTTP_AUTHORIZATION'];

            $token = str_replace('bearer  ', '', $header);
            $jwt = new \Ahc\Jwt\JWT(APP_SECRET, 'HS256', '72000', 10);
            try {
                $tokenValues = $jwt->decode($token);
                $_SERVER['TOKEN_VALUES'] = $tokenValues;
            } catch (Exception $e) {
                $_SERVER['TOKEN_VALUES'] = false;
                /*$arr['status'] = 301;
                $arr['msg'] = "Session Expired";
                Response::sendJson($arr);*/
            }
            $callback();
        }

    }

    static function put()
    {

    }

    static function patch()
    {

    }

    static function delete()
    {

    }
}