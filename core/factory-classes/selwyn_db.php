<?php

class SelwynDatabase
{
    
    static $con;
    static $inst;
    
    
    static function getInstance()
    {
        if (!self::$inst) {
            self::$inst = new self();
        }
        
        return self::$inst;
    }
    
    function __construct()
    {
        
        self::$con = App::$dbCon;
    }
    
    static function query($sql, $fetch = 'class', $ParamsArray = array())
    {
        try {
            $con = self::$con;
            //$sql = str_replace("'","\\'",$sql);
            if($con == null){
                $i = new self();
                $con = self::$con;
            }
            $stmt = $con->prepare($sql);
            App::$lastSQL = $sql;
            foreach ($ParamsArray as $key => $val) {
                //$con->bindParam($key,$val);
                $stmt->bindParam($key, $val);
            }
            $stmt->execute();
            
            if ($fetch == 'class') {
                
                return $stmt->fetchAll(PDO::FETCH_CLASS);
            } else if ($fetch == 'update') {
                return $stmt->rowCount();
            } else if ($fetch == 'insert') {
                return $con->lastInsertId();
            } else if ($fetch == 'array') {
                return $stmt->fetchAll(PDO::FETCH_ASSOC);
            }
        } catch (Exception $e) {
            echo "<pre>";
            print_r($e);
            echo "</pre>";
            exit();
        }
        
    }
    
    static function getSqlFunction()
    {
        $sqlFunctions = array('now()', 'NOW()','CURRENT_DATE()');
        return $sqlFunctions;
    }
    
    
    function insert($array, $table)
    {
        $fields = "";
        $data = "";
        
        foreach ($array as $key => $value) {
            $value = str_replace("'", "''", $value);
            $fields .= "`" . $key . "`, ";
            if (in_array($value, self::getSqlFunction())) {
                $data .= " " . $value . ", ";
            } else {
                $data .= "'" . $value . "', ";
            }
        }
        
        //$data = str_replace("'","''",$data);
        $fields = rtrim($fields, ", ");
        $data = rtrim($data, ", ");
        $sql = "INSERT INTO " . $table . " ( " . $fields . " ) VALUES ( " . $data . " )";
        return $this->query($sql, 'insert', array());
    }
    
}

class SelwynDataModels extends SelwynDatabase
{
  
    
    function flush($primaryArray)
    {
        
        $ds = $this->getDataSet();
        $pk = $ds['PRI'][0];
        unset($ds['PRI']);
        $cls = get_called_class();
        
        if (is_null($ds[$pk]) || $ds[$pk] == 0 || $ds[$pk] == '0' || $ds[$pk] == "") {
            
            /*Insert*/
            unset($ds[$pk]);
            foreach ($ds as $key => $val) {
                if (is_null($val)) {
                    unset($ds[$key]);
                }
            }
            return $this->insert($ds, $cls::TABLE_NAME);
            
        } else {
            
            /*update*/
            $sql = "UPDATE `" . $cls::TABLE_NAME . "` SET ";
            $primaryKey = $ds[$pk];
            unset($ds[$pk]);
            foreach ($ds as $key => $val) {
                if($key == 'sqlFunctions'){
                    continue;
                }
                $sqlFunctions = self::getSqlFunction();
                if (gettype($val) == 'string') {
                    if (in_array($val, $sqlFunctions)) {
                        $sql .= " `" . $key . "` = " . $val . ",";
                    } else {
                        $val = str_replace("'", "''", $val);
                        $sql .= " `" . $key . "` = '" . $val . "',";
                    }
                } else if (gettype($val) == 'NULL') {
                    $sql .= " `" . $key . "` = null,";
                } else if (gettype($val) == 'integer') {
                    $sql .= " `" . $key . "` = " . $val . ", ";
                } else if (gettype($val) == 'double') {
                    $sql .= " `" . $key . "` = " . $val . ", ";
                } else {
                    var_dump($val);
                    var_dump(gettype($val));
                    exit;
                }
            }
            
            /*Remove Last comma*/
            
            $sql = substr($sql, 0, strlen($sql) - 1);
            
            if (substr($sql, strlen($sql) - 1, 1) == ',') {
                $sql = substr($sql, 0, strlen($sql) - 1);
            }
            $sql .= " Where " . $pk . " = '" . $primaryKey . "'";
            
            if (self::query($sql, 'update') !== FALSE) {
                return $primaryKey;
            } else {
                return FALSE;
            }
        }
    }
    
    static function find($params)
    {
        /*
         * Params has to be array
         * */
        $cls = get_called_class();
        $tblName = $cls::TABLE_NAME;
        $sql = "SELECT *  FROM " . $tblName . " WHERE 1=1 ";
        foreach ($params as $col => $val) {
            $sql .= " AND `" . $col . "` = '" . $val . "'";
        }
        $result = $cls::query($sql);
        if (!empty($result)) {
            
            $dataset = array();
            foreach ($result as $key => $val) {
                $obj = new $cls();
                foreach ($val as $k1 => $v1) {
                    $methodName = str_replace('_', ' ', $k1);
                    $methodName = "set" . str_replace(' ', '', ucwords($methodName));
                    $obj->{$methodName}($v1);
                }
                $dataset[$key] = $obj;
                
            }
            return $dataset;
        } else {
            return FALSE;
        }
    }
    
    static function findDs($params, $sort = array())
    {
        /*
         * Params has to be array
         * */
        $cls = get_called_class();
        $tblName = $cls::TABLE_NAME;
        $sql = "SELECT *  FROM " . $tblName . " WHERE 1=1 ";
        foreach ($params as $col => $val) {
            $sql .= " AND `" . $col . "` = '" . $val . "'";
        }
        
        if (count($sort) > 0) {
            $sql .= " ORDER BY ";
            $i = 0;
            foreach ($sort as $col => $val) {
                if ($i != 0) {
                    $sql .= ",";
                } else {
                    $i++;
                }
                $sql .= " `" . $col . "` " . $val;
            }
        }
        
        $result = $cls::query($sql);
        
        return $result;
        if (!empty($result)) {
            
            $dataset = array();
            foreach ($result as $key => $val) {
                $obj = new $cls();
                foreach ($val as $k1 => $v1) {
                    $methodName = str_replace('_', ' ', $k1);
                    $methodName = "set" . str_replace(' ', '', ucwords($methodName));
                    $obj->{$methodName}($v1);
                }
                $dataset[$key] = $obj;
                
            }
            return $dataset;
        } else {
            return FALSE;
        }
    }
    
    static function findOneBy($params)
    {
        /*
         * Params has to be array
         * */
        
        $t = self::getSqlFunction();
        $cls = get_called_class();
        $tblName = $cls::TABLE_NAME;
        $sql = "SELECT *  FROM " . $tblName . " WHERE 1=1 ";
        foreach ($params as $col => $val) {
            if (in_array($val, $t)) {
                $sql .= " AND `$col`=" . $val ;
            } else {
                $sql .= " AND `$col` = '" . $val."' ";
            }
            //$sql .= " AND `" . $col . "` = '" . $val . "'";
        }
        
        $result = $cls::query($sql);
        if (!empty($result)) {
            
            $dataset = array();
            foreach ($result as $key => $val) {
                $obj = new $cls();
                foreach ($val as $k1 => $v1) {
                    $methodName = str_replace('_', ' ', $k1);
                    $methodName = "set" . str_replace(' ', '', ucwords($methodName));
                    $obj->{$methodName}($v1);
                }
                $dataset[$key] = $obj;
                
            }
            
            return $dataset[0];
        } else {
            return FALSE;
        }
    }
    
    static function loadDsBy($id)
    {
        /*
        * Params has to be array
        * */
        $cls = get_called_class();
        $tblName = $cls::TABLE_NAME;
        $sql = "SELECT *  FROM " . $tblName . " WHERE id=" . $id;
        
        $result = $cls::query($sql);
        if (!empty($result)) {
            return $result[0];
        } else {
            return FALSE;
        }
    }
    
    static function load($id)
    {
        /*
        * Params has to be array
        * */
        $cls = get_called_class();
        $tblName = $cls::TABLE_NAME;
        $sql = "SELECT *  FROM " . $tblName . " WHERE id=" . $id;
        
        $result = $cls::query($sql);
        if (!empty($result)) {
            
            $dataset = array();
            foreach ($result as $key => $val) {
                $obj = new $cls();
                foreach ($val as $k1 => $v1) {
                    $methodName = str_replace('_', ' ', $k1);
                    $methodName = "set" . str_replace(' ', '', ucwords($methodName));
                    $obj->{$methodName}($v1);
                }
                $dataset[$key] = $obj;
                
            }
            
            return $dataset[0];
        } else {
            return FALSE;
        }
    }
}