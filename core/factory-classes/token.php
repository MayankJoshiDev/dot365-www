<?php

class Token
{
    /*This is Selwyn default Tokenization Class*/
    private static $inst;

    private static function inst()
    {
        if(!self::$inst) { self::$inst = new self(); }
        return self::$inst;
    }
    static function encode($var)
    {
        if(!is_array($var)) {
            $payLoad = array($var);
        }else{
            $payLoad = $var;
        }

        $payLoad['SelwynTokenTimeStamp'] = time();
        return Firebase\JWT\JWT::encode(json_encode($payLoad),APP_SECRET);
    }

    static function decode($token)
    {
        $decoded = Firebase\JWT\JWT::decode($token,APP_SECRET,array('HS256'));
        if(!$decoded){
            return false;
        }

        $currentTime = time();
        $decodedObj = json_decode($decoded);
        if(($currentTime - $decodedObj->SelwynTokenTimeStamp) > TOKEN_EXPIRE)
        {
            return false;
        }else{
            $array = (array) $decodedObj;
            unset($array['SelwynTokenTimeStamp']);
            return $array;
        }
    }
}