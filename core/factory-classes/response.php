<?php

class Response
{
    private $header;
    private $footer;
    private $objectBuffer = array();
    private $headerBuffer;
    private $footerBuffer;
    private $dataBuffer = array();
    private $jsBeforeBody = array();
    private $jsAfterBody = array();
    private $css = array();
    private $obLevel = 0;
    
    private $metaDesc = "";
    private $metaTittle = "";
    private $metaKeywords = "";
    private $author = "";
    
    private $debugBuffer = "";
    
    
    static $inst;
    private $theme = 'default';
    
    static function inst()
    {
        if (self::$inst) {
            return self::$inst;
        } else {
            self::$inst = new self();
            return self::$inst;
        }
        
    }
    
    static function setTheme($theme)
    {
        self::inst()->theme = $theme;
        define('THEME_PATH', ROOT_DIR . '/app/theme/' . $theme);
        define('THEME_URL', SITE_DOMAIN . "/assets");
    }
    
    static function setHeader($file)
    {
        self::inst()->header = $file;
    }
    
    static function setFooter($file)
    {
        self::inst()->footer = $file;
    }
    
    static function bindData($dataArray = NULL)
    {
        if (is_array($dataArray)) {
            self::inst()->dataBuffer = array_merge(self::inst()->dataBuffer, $dataArray);
        
        }
    }
    
    static function bindCss($file)
    {
    
    }
    
    static function jsBeforeBody()
    {
    
    }
    
    static function jsAfterBody()
    {
    
    }
    
    static function loadHeader()
    {
        ob_start();
        $fileName = self::inst()->header . '.php';
        $file = THEME_PATH . '/' . $fileName;
        
        if (file_exists($file)) {
            if (is_array(self::inst()->dataBuffer)) {
                foreach (self::inst()->dataBuffer as $key => $value) {
                    $$key = $value;
                }
            }
            include $file;
        } else {
            $e = new AppException();
            
            $e->errorMsg('Header File Not Found '. $file)
                ->code(1003)
                ->killApp();
        }
        self::inst()->headerBuffer .= ob_get_contents();
        ob_clean();
        
    }
    
    private function loadPreBody()
    {
    
    }
    
    private function loadPostBody()
    {
    
    }
    
    private function postFooter()
    {
    
    }
    
    static function loadFooter()
    {
        
        ob_start();
        $fileName = self::inst()->footer . '.php';
        $file = THEME_PATH . '/' . $fileName;
        
        if (file_exists($file)) {
            if (is_array(self::inst()->dataBuffer)) {
                foreach (self::inst()->dataBuffer as $key => $value) {
                    $$key = $value;
                }
            }
            include $file;
        } else {
            $e = new AppException();
            $e->code(1003)
                ->errorMsg('Footer File Not Found');
        }
        self::inst()->footerBuffer .= ob_get_contents();
        ob_clean();
    }
    
    static function redirect($path)
    {
        ob_clean();
        header('Location: '.$path);
        exit;
        //http_redirect($path);
    }
    
    static function loadHTML($view, $dataArray = NULL)
    {
        $viewArray = explode("/", $view);
        if (count($viewArray) !== 2) {
            /*TODO Exception here*/
            //exit('line 130 LoadHtml');
        }
        while (ob_get_level()) {
            ob_get_clean();
        }
        
        ob_start();
        $moduleName = strtolower($viewArray[0]);
        $fileName = strtolower($viewArray[1]);
        $file = ROOT_DIR . '/app/modules/' . $moduleName . "/views/" . $fileName . '.php';
        if (file_exists($file)) {
            if (is_array($dataArray)) {
                self::inst()->dataBuffer = array_merge(self::inst()->dataBuffer, $dataArray);
                
            }
            if (is_array(self::inst()->dataBuffer)) {
                foreach (self::inst()->dataBuffer as $key => $value) {
                    $$key = $value;
                }
            }
            
            include $file;
        } else {
            $exception = new AppException();
            $exception->code(701)
                ->severity('LOW')
                ->errorMsg("Can't Load the requested file \"" .$file."\"")
                ->killApp();
            
        }
        
        while (ob_get_level()) {
            self::inst()->objectBuffer[self::inst()->obLevel] = ob_get_contents();
            self::inst()->obLevel++;
            ob_get_clean();
        }
        
    }
    
    
    static function injectHTML($view, $dataArray = NULL)
    {
        $viewArray = explode("/", $view);
        if (count($viewArray) !== 2) {
            /*TODO Exception here*/
            exit('line 178 injectHtml');
        }
        
        $moduleName = strtolower($viewArray[0]);
        $fileName = strtolower($viewArray[0]);
        $file = ROOT_DIR . '/app/modules/' . $moduleName . "/views/" . $fileName . '.phtml';
        if (file_exists($file)) {
            if (is_array($dataArray)) {
                self::inst()->dataBuffer = array_merge($dataArray, self::inst()->dataBuffer);
                
            }
            if (is_array(self::inst()->dataBuffer)) {
                foreach (self::inst()->dataBuffer as $key => $value) {
                    $$key = $value;
                }
            }
            
            include $file;
        } else {
            throw new Exception("Can't Load the requested file - " . $fileName, 701);
        }
        self::inst()->objectBuffer[self::inst()->obLevel] = ob_get_contents();
    }
    
    static function sendHtml()
    {
        ob_get_clean();
        echo self::inst()->headerBuffer;
        foreach (self::inst()->objectBuffer as $key => $val) {
            echo $val;
        }
        echo self::inst()->footerBuffer;
        echo self::inst()->debugBuffer;
        
    }
    
    function debugBuffer($str)
    {
        
        $this->debugBuffer .= $str;
    }
    
    static function sendJson($arr)
    {
        echo json_encode($arr);
        exit;
    }
    
    static function metaDesc($desc)
    {
        self::inst()->metaDesc = $desc;
    }
    
    static function metaKeywords($stringOfKeywords)
    {
        self::inst()->metaKeywords = $stringOfKeywords;
    }
    
    static function metaTittle($tittle)
    {
        self::inst()->metaTittle = $tittle;
    }
    
}