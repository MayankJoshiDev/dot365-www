<?php
/**
 * Created by PhpStorm.
 * User: CPU011
 * Date: 4/24/2018
 * Time: 12:22 AM
 */
class error_codes
{
    const SUCCESS = 1000;
    const ERR_AUTHORIZATION = 1001;
    const ERR_DATA_NOT_AVAILABLE = 1002;
    const ERR_FILE_NOT_FOUND = 1003;
}