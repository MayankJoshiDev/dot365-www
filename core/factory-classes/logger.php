<?php
/**
 * Created by PhpStorm.
 * User: CPU011
 * Date: 3/14/2018
 * Time: 12:43 AM
 */

class log
{
    static function info($text){
        $dt = date('d-m-Y');
        $path = ROOT_DIR.'/logs/';
        if(!is_dir($path)){
            mkdir($path,777);
        }
        $file = $path.$dt.'.log';
        $f = fopen($file,'a+');
        $str  = date('H:i:s').'-'.$text.PHP_EOL;
        fwrite($f,$str);
        fclose($f);
    }
}