<?php

define('PHP_TAB',"\t");
final class App
{
    private $ENV = "";
    private static $inst;
    private $isRunning = false;
    private $appFlag = array();
    static $lastSQL = '';

    private $config = array();
    public static $dbCon;
    static function inst()
    {
        if(self::$inst){
            return self::$inst;
        }else{
            self::$inst = new self();
            return self::$inst;
        }
    }

    function __construct()
    {
        //$this->run();
        error_reporting(E_ALL);
    }

    public function getEnvironment()
    {
        return $this->ENV;
    }

    function run()
    {
        if($this->isRunning){return true;}else{$this->isRunning = true;}
        $this->_loadPreCore();
        $this->_loadFactoryClasses();
        $this->_loadCoreClasses();

        $this->_loadConfig();
        $this->_loadDatabase();
        $this->_loadDataModels();
        $this->_loadPostCore();


        $this->_loadHelpers();
        $this->_loadBaseControllers();
        $this->_loadModels();
        $this->_loadModules();

        if(isset($_SERVER['HTTP_DEVICE'])){
            $device = $_SERVER['HTTP_DEVICE'];
        }else{
            $device = "";
        }

        if(isset($_SERVER['HTTP_APPVERSION'])){
            $appVersion = $_SERVER['HTTP_APPVERSION'];
        }else{
            $appVersion = "";
        }
        
        if(isset($_SERVER['REMOTE_ADDR'])){
            if($_SERVER['REMOTE_ADDR'] != '182.70.123.238' && $_SERVER['REMOTE_ADDR'] != '217.146.95.83' && $_SERVER['REQUEST_URI'] != '/abtest'){
                log::info($device.'['.$appVersion.']['.$_SERVER['REMOTE_ADDR'].'] '.$_SERVER['REQUEST_URI'].' - '.json_encode(Req::json()));
            }
        }
        
        $this->_loadRoutes();
        $this->_postRoutes();

    }

    function setFlag($flag,$value)
    {
        $this->appFlag[$flag] = $value;
    }

    function getFlag($key)
    {
        if (isset($this->appFlag[$key]))
        {
            return $this->appFlag[$key];
        }else{
            return null;
        }
    }

    function getConfig($key)
    {
        if(key_exists($key,$this->config)){
            return $this->config[$key];
        }else{
            return null;
        }
    }

    private function _loadConfig()
    {
        /*First Load Global Config, then Environment*/
        $configBuffer  = include_once ROOT_DIR.'/config/global_config.php';
        $envConfig = include_once  ROOT_DIR.'/config/'.$configBuffer['environment'].'_config.php';
        define('ENVIRONMENT',$configBuffer['environment']);
        $this->config =  array_merge($configBuffer,$envConfig);

    }

    private function _loadDatabase()
    {
        //require_once ROOT_DIR.'/config/database.php';
        $dbhost = DB_HOST;
        $dbuser = DB_USERNAME;
        $dbpass = DB_PASSWORD;
        $dbname = DB_NAME;

        $dbConnection = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
        $dbConnection->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        self::$dbCon = $dbConnection;
    }



    private function _loadPreCore()
    {
        date_default_timezone_set("Asia/Calcutta");
    }

    private function _loadDataModels()
    {

        $routeDir = ROOT_DIR.'/app/data-models';
        if (!is_dir($routeDir )){
            throw new \Exception('Data Module Directory not found', 1003);
        }
        $fileList = scandir($routeDir );
        unset($fileList[0]);
        unset($fileList[1]);
        require $routeDir  . '/proxies/data_proxy.php';
        foreach ($fileList as $item) {
            if($item != 'proxies'){
                require $routeDir  . '/' . $item;
            }
        }


        unset($routeDir);
    }
    private function _loadPostCore()
    {
        /*Load App/Lib*/
        $libDir = ROOT_DIR.'/app/global-classes';
        if (!is_dir($libDir )){
            throw new \Exception('Application Global Classes Directory not found', 1003);
        }
        $fileList = scandir($libDir );
        unset($fileList[0]);
        unset($fileList[1]);

        foreach ($fileList as $item) {
            if(!self::inst()->shouldLoadFile($item)){continue;}
            require $libDir  . '/' . $item;
        }
        unset($libDir);

        $libDir = ROOT_DIR.'/app/lib';
        if (!is_dir($libDir )){
            throw new \Exception('Application Lib Directory not found', 1003);
        }
        $fileList = scandir($libDir );
        unset($fileList[0]);
        unset($fileList[1]);

        foreach ($fileList as $item) {
            if(!self::inst()->shouldLoadFile($item)){continue;}
            require $libDir  . '/' . $item;
        }
        unset($libDir);

        require_once ROOT_DIR.'/app/thirdparty/jwt/ValidatesJWT.php';
        require_once ROOT_DIR.'/app/thirdparty/jwt/JWTException.php';
        require_once ROOT_DIR.'/app/thirdparty/jwt/functions.php';
        require_once ROOT_DIR.'/app/thirdparty/jwt/JWT.php';
        require_once ROOT_DIR.'/app/thirdparty/jwt/JWT.php';


    }
    private function _loadFactoryClasses()
    {

        $baseClassDir = ROOT_DIR.'/core/factory-classes';
        if (!is_dir($baseClassDir )){
            throw new \Exception('Factory Classes Directory not found', 1003);
        }
        $fileList = scandir($baseClassDir );
        unset($fileList[0]);
        unset($fileList[1]);

        foreach ($fileList as $item) {
            require $baseClassDir  . '/' . $item;
        }
        unset($baseClassDir);

        /*Loading Factory Helpers*/
        $baseHelperDir = ROOT_DIR.'/core/factory-helpers';

        if (!is_dir($baseHelperDir )){
            mkdir($baseHelperDir,0544);
            //throw new \Exception('Factory Helpers Directory not found', 1003);
        }
        $fileList = scandir($baseHelperDir );
        unset($fileList[0]);
        unset($fileList[1]);

        foreach ($fileList as $item) {
            require $baseHelperDir  . '/' . $item;
        }
        unset($baseHelperDir);
    }
    private function _loadCoreClasses()
    {

        $baseClassDir = ROOT_DIR.'/core/core-classes';
        if (!is_dir($baseClassDir )){
            throw new \Exception('Factory Classes Directory not found', 1003);
        }
        $fileList = scandir($baseClassDir );
        unset($fileList[0]);
        unset($fileList[1]);

        foreach ($fileList as $item) {
            require $baseClassDir  . '/' . $item;
        }
        unset($baseClassDir);

        /*Loading Factory Helpers*/
        $baseHelperDir = ROOT_DIR.'/core/factory-helpers';
        if (!is_dir($baseHelperDir )){
            throw new \Exception('Core Classes Directory not found', 1003);
        }
        $fileList = scandir($baseHelperDir );
        unset($fileList[0]);
        unset($fileList[1]);

        foreach ($fileList as $item) {
            require $baseHelperDir  . '/' . $item;
        }
        unset($baseHelperDir);
    }

    private function _loadModels()
    {
        $baseHelperDir = ROOT_DIR.'/app/models';
        if (!is_dir($baseHelperDir )){
            throw new \Exception('Models Directory not found', 1003);
        }
        $fileList = scandir($baseHelperDir );
        unset($fileList[0]);
        unset($fileList[1]);

        foreach ($fileList as $item) {
            require $baseHelperDir  . '/' . $item;
        }
        unset($baseHelperDir);
    }
    private function _loadModules()
    {
        $moduleDir = ROOT_DIR.'/app/modules';
        if (!is_dir($moduleDir )){
            throw new \Exception('Application Modules Directory not found', 1003);
        }
        $fileList = scandir($moduleDir );
        unset($fileList[0]);
        unset($fileList[1]);

        foreach ($fileList as $item) {
            $item = $moduleDir  . '/' . $item.'/controllers/'.ucfirst($item).'Controller.php';
            require $item;

        }
        unset($moduleDir);
    }

    private  function _loadBaseControllers()
    {
        $baseHelperDir = ROOT_DIR.'/app/base-controllers';
        if (!is_dir($baseHelperDir )){
            throw new \Exception('Base Controller Directory not found', 1003);
        }
        $fileList = scandir($baseHelperDir );
        unset($fileList[0]);
        unset($fileList[1]);

        foreach ($fileList as $item) {
            require $baseHelperDir  . '/' . $item;
        }
        unset($baseHelperDir);
    }

    private function _loadHelpers()
    {
        $helperDir = ROOT_DIR.'/app/helpers';
        if (!is_dir($helperDir )){
            throw new \Exception('Application Helper Directory not found', 1003);
        }
        $fileList = scandir($helperDir );
        unset($fileList[0]);
        unset($fileList[1]);

        foreach ($fileList as $item) {
            require $helperDir  . '/' . $item;
        }
        unset($helperDir);
    }

    private function _loadRoutes()
    {
        $this->appFlag[fstr::FLAG_ROUTE_MATCHED] = false;
        $routeDir = ROOT_DIR.'/app/routes';
        if (!is_dir($routeDir )){
            throw new \Exception('Application Routes Directory not found', 1003);
        }
        $fileList = scandir($routeDir );
        unset($fileList[0]);
        unset($fileList[1]);

        foreach ($fileList as $item) {
            require $routeDir  . '/' . $item;
        }
        unset($routeDir);
    }

    private function _postRoutes()
    {
        /*Check if App Load Flag is not true*/
        if(!$this->appFlag[fstr::FLAG_ROUTE_MATCHED])
        {
            /*So now 404*/
            //DefaultController::inst()->error404();
            /*http_response_code(404);
            exit("<h1>404 Not Found</h1>");*/
        }
    }

    private function _walkByDefinedRoutes()
    {

    }


    private function shouldLoadFile($item)
    {

        if($item == ".keep"){
            return false;
        }else{
            return true;
        }

    }

    public function loadHTML($page,$data)
    {
        foreach ($data as $k => $v)
        {
            $$k = $v;
        }

        include (ROOT_DIR.'/html/'.$page);
    }

}
?>