<?php
if(!defined('ROOT_DIR')){exit('No Direct Access Allowed');}

$config['app_name'] = "lifePHP";
$config['selwyn_app_id'] = "obtain_your_app_id";
$config['app_key'] = "secret_app_key";


/*Env Setting*/
if(isset($_SERVER['HTTP_HOST']) && strstr($_SERVER['HTTP_HOST'],'localhost')){
    $env = 'local';
}elseif(isset($_SERVER['HTTP_HOST']) && strstr($_SERVER['HTTP_HOST'],'stage')){
    $env = 'dev';

}elseif(isset($_SERVER['HTTP_HOST']) && strstr($_SERVER['HTTP_HOST'],'dot365.in')){
    $env = 'production';
}else{
    $env = 'local';
}



/*Env Setting*/
$config['environment'] = $env;
$config['default_controller'] = "home";
return $config;